/label ~"workflow::discussion"
/assign me

## Summary

<!-- enter brief summary text here, 2-3 sentences -->

### Detailed Description

<!-- At least one comprehensible use case -->

#### Steps to reproduce the issue

<!-- how did you get here? -->

1. ...
2. ...
3. ...

#### Result

<!-- what happened? -->

#### Expected Result

<!-- in your opinion, what is supposed to happen instead? -->

#### Additional Information

<!-- Configuration, data, software versions etc. which are involved. If possible, provide the git commit ID you are running off. -->

## Resolution

<!-- OPTIONAL section in case you have concrete ideas for a fix. If unused, delete this entire section -->

### Proposal

To be discussed.

<!-- Alternatively, you can add a full text or bullet-pointed proposal here for discussion -->

### Specification

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and "OPTIONAL" in this issue are to be interpreted *in the spirit of* [RFC 2119](https://datatracker.ietf.org/doc/html/rfc2119), even though we're not technically doing protocol design.

<!-- Bullet-point list of MUST, SHOULD, MAY, SHOULD NOT and MUST NOT. will be added as a result of the proposal discussion process. -->

