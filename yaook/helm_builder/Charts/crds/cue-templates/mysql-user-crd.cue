// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#operatorcrd
{
	#group:    "infra.yaook.cloud"
	#kind:     "MySQLUser"
	#plural:   "mysqlusers"
	#singular: "mysqluser"
	#schema: properties: spec: {
		required: [
			"user",
			"serviceRef",
			"passwordSecretKeyRef",
		]
		properties: {
			user: type: "string"
			serviceRef:           crd.#ref
			passwordSecretKeyRef: crd.#secretkeyref
			databasePrivileges: {
				type: "array"
				default: ["ALL PRIVILEGES"]
				items: {
					type:    "string"
					pattern: "[A-Z ]+"
				}
			}
			globalPrivileges: {
				type: "array"
				default: []
				items: {
					type:    "string"
					pattern: "[A-Z ]+"
				}
			}
		}
	}
}
