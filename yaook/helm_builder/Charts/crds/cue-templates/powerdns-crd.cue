// Copyright (c) 2024 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#cacertificatecrd
crd.#databaseNoDriver
crd.#releaseawarecrd
crd.#operatorcrd
crd.#issuer
{
	#group:    "infra.yaook.cloud"
	#kind:     "PowerDNSService"
	#plural:   "powerdnsservices"
	#singular: "powerdnsservice"
	#shortnames: ["pdnss", "pdns"]
	#releases: ["4.9"]
	#schema: properties: spec: {
		crd.replicated
		crd.topologySpreadConstraints
		required: [
			"database",
			"targetRelease",
			"subnetCidr",
			"apiKeySecret",
			"issuerRef",
		]
		properties: {
			serviceMonitor: crd.#servicemonitor
			powerdnsConfig: {
				type:                                   "object"
				description:                            "Key value pairs to overwrite pdns.conf settings"
				"x-kubernetes-preserve-unknown-fields": true
			}
			apiKeySecret: {
				type:        "object"
				description: "A single secret injection configuration. This will set the secret value as the API key inside the PowerDNS configuration."
				required: ["secretName", "key"]
				properties: {
					secretName: {
						type:        "string"
						description: "Name of the Kubernetes Secret to read"
					}
					key: {
						type:        "string"
						description: "Key of the Kubernetes Secret to read"
					}
				}
			}

			loadBalancerIP: {
				type:        "string"
				description: "IP address of the load balancer for the PowerDNS webserver"
			}

			subnetCidr: {
				type:        "string"
				description: "IP range of the k8s pod network. Used to only allow requests from within the k8s network."
			}

			resources: {
				type: "object"
				properties: {
					"powerdns":       crd.#containerresources
					"ssl-terminator": crd.#containerresources
					"service-reload": crd.#containerresources
				}
			}
		}
	}
	#schema: properties: status: properties: replicas: type: "integer"
}
