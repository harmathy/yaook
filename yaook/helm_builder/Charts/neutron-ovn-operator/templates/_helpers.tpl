{{- define "yaook.operator.name" -}}
neutron_ovn
{{- end -}}

{{- define "yaook.operator.extraEnv" -}}
- name: YAOOK_NEUTRON_OVN_AGENT_OP_JOB_IMAGE
  value: {{ include "yaook.operator-lib.image" . }}
{{- end -}}
