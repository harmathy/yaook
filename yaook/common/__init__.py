#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import base64
import logging
import secrets
import string
import typing
import environ


def genpassword(length: int = 32) -> typing.Tuple[str, str]:
    alphabet = string.ascii_letters + string.digits
    password = "".join(secrets.choice(alphabet) for i in range(length))
    return base64.b64encode(password.encode("ascii")).decode("ascii"), password


def configure_logging(
    verbosity: int,
    debug_libraries: bool,
) -> None:
    """
    Apply basic configuration to the logging library.

    :param verbosity: The verbosity level (0-3) to use.
    :param debug_libraries: If true, also non-Yaook loggers will have their
        verbosity raised.

    The verbosity number is translated into a logging verbosity in the
    following manner:

    - `0` => :data:`logging.ERROR`
    - `1` => :data:`logging.INFO`
    - `2` => :data:`logging.WARNING`
    - anything else => :data:`logging.DEBUG`

    If `debug_libraries` is true, all loggers will receive the `verbosity` as
    configured. If `debug_libraries` is false, the verbosity of non-Yaook
    loggers is capped at :data:`logging.WARNING`.

    In addition to configuring verbosities, this function configures the
    output format.
    """
    verbosity_map = {
        0: logging.ERROR,
        1: logging.WARNING,
        2: logging.INFO,
    }

    if debug_libraries:
        global_verbosity = verbosity
    else:
        # if not debugging, take the lower verbosity level
        global_verbosity = min(1, verbosity)

    global_level = verbosity_map.get(global_verbosity, logging.DEBUG)
    yaook_level = verbosity_map.get(verbosity, logging.DEBUG)

    logging.basicConfig(
        level=global_level,
        format="%(asctime)-15s %(levelname)7s  %(name)s  %(message)s",
    )
    logging.getLogger("yaook").setLevel(yaook_level)


@environ.config(prefix="YAOOK_OP")
class OpRunConfig:
    namespace = environ.var(default="yaook")
    multiple_namespaces = environ.bool_var(default=False)
    runner_count = environ.var(default=3)
    periodic_reconcile_delay = environ.var(default=3600)
    metrics_port = environ.var(default=8000)


@environ.config(prefix="YAOOK_OP")
class OpCommonConfig:
    verbosity = environ.var(0, converter=int)
    tracing_enabled = environ.var(False, converter=bool)
    jaeger_hostname = environ.var("localhost")
    jaeger_port = environ.var(6831, converter=int)
