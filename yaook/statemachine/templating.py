#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
:mod:`~yaook.statemachine.templating` – Template utilities
##########################################################

.. autofunction:: to_config_map_volumes

.. autofunction:: to_secret_volumes

.. autofunction:: flat_merge_dict

.. autofunction:: parse_chmod

.. autofunction:: container_resources

"""
import functools
import json
import operator
import re
import typing

import jinja2.ext
from jinja2.lexer import Token

import kubernetes_asyncio.client as kclient


class SafeWrapper(str):
    """A string marked for unescaped insertion."""
    # partially based on
    # <https://github.com/mbello/jinja2-ext-custom-autoescaping/>
    # which is lincensed as CC0


def to_json(data: typing.Any) -> str:
    """
    Serialize the argument to a single line of json.

    This allows safe insertion of the argument in rendered YAML.
    If the argument is an instance of
    """
    # partially based on
    # <https://github.com/mbello/jinja2-ext-custom-autoescaping/>
    # which is lincensed as CC0
    if isinstance(data, jinja2.Undefined):
        data._fail_with_undefined_error()
    if isinstance(data, SafeWrapper):
        return str(data)
    return json.dumps(data)


class YAMLAutoescapeExtension(jinja2.ext.Extension):
    """
    Insert a `| to_json` filter at the end of every variable substitution.

    This will ensure that all injected values are converted to YAML.

    The extension also supplies the `unsafe` filter, which will cause the
    result to be inserted unescaped in the output.
    """
    # partially based on
    # <https://github.com/mbello/jinja2-ext-custom-autoescaping/>
    # which is lincensed as CC0

    def __init__(self, env: jinja2.Environment):
        super().__init__(env)

        # register the required filters
        env.filters['to_json'] = to_json
        env.filters['unsafe'] = lambda x: SafeWrapper(x)

    def filter_stream(
            self,
            stream: typing.Iterable[Token],
            ) -> typing.Iterable[Token]:
        for token in stream:
            if token.type == 'variable_end':  # nosemgrep this is no secret :)
                yield Token(token.lineno, 'rparen', ')')
                yield Token(token.lineno, 'pipe', '|')
                yield Token(token.lineno, 'name', 'to_json')
            yield token
            if token.type == 'variable_begin':
                yield Token(token.lineno, 'lparen', '(')


def to_volume_templates(
        join_spec: typing.Callable[
            [kclient.V1ObjectReference, typing.Mapping],
            typing.Mapping[str, typing.Any]],
        instances: typing.Mapping[typing.Optional[str],
                                  kclient.V1ObjectReference],
        extra_spec: typing.Mapping) -> typing.Mapping:
    return {
        node_name: {
            "template": join_spec(
                object_reference,
                extra_spec,
            ),
        }
        for node_name, object_reference in instances.items()
    }


def _volume_template_generator(
        join_spec: typing.Callable[
            [kclient.V1ObjectReference, typing.Mapping],
            typing.Mapping[str, typing.Any]],
        ) -> typing.Callable[
            [
                typing.Mapping[typing.Optional[str],
                               kclient.V1ObjectReference],
                typing.Mapping,
            ],
            typing.Mapping]:

    @functools.wraps(join_spec)
    def generator(
            instances: typing.Mapping[
                typing.Optional[str],
                kclient.V1ObjectReference],
            extra_spec: typing.Mapping) -> typing.Mapping:
        return to_volume_templates(join_spec, instances, extra_spec)
    return generator


def _join_spec(
        base_spec: typing.Mapping,
        extra_spec: typing.Mapping) -> typing.Mapping:
    result = dict(extra_spec)
    result.update(base_spec)
    return result


@_volume_template_generator
def to_config_map_volumes(
        ref: kclient.V1ObjectReference,
        extra_spec: typing.Mapping) -> typing.Mapping:
    """
    Convert an instance mapping to a `nodeMap` for a `configMap` volume
    template for use with a `ConfiguredDaemonSet`.
    """
    return {
        "configMap": _join_spec(
            {
                "name": ref.name,
            },
            extra_spec,
        )
    }


@_volume_template_generator
def to_secret_volumes(
        ref: kclient.V1ObjectReference,
        extra_spec: typing.Mapping) -> typing.Mapping:
    """
    Convert an instance mapping to a `nodeMap` for a `secret` volume template
    for use with a `ConfiguredDaemonSet`.
    """
    return {
        "secret": _join_spec(
            {
                "secretName": ref.name,
            },
            extra_spec,
        )
    }


def flat_merge_dict(
        d1: typing.Mapping,
        d2: typing.Mapping,
        ) -> typing.MutableMapping:
    """
    Merge `d1` and `d2` together and return the result.

    This always returns a (flat) copy of the dictionaries and does not mutate
    any of the input dictionaries.

    If the keys of `d1` and `d2` conflict, `d2` wins.
    """
    result = dict(d1)
    result.update(d2)
    return result


def parse_chmod(s: str) -> int:
    """
    Convert the string representation of mode/permission bits into an integer.

    :param s: String representation of the permission bits.
    :return: The integer value represented.

    This function supports two input formats:

    - Octal number with leading ``0o`` (e.g. `0o755`)
    - A subset of the syntax accepted by chmod. The following restrictions
      apply:

      - No support for sticky/setgid/setuid bits
      - No support for ``+``/``-``
      - Setting the bits of the same subject multiple times in the same string
        is prohibited (i.e. ``u=r,u=w`` is illegal)
      - Leaving the subject blank (chmod does fun things based on umask in that
        case)

      Valid examples:

      - ``a=r`` (equal to ``0o444``)
      - ``u=rw,go=r`` (equal to ``0o644``)

      Invalid examples:

      - ``a=r,u=rw`` (duplicate assignment for ``u``)
      - ``a=r,u+w`` (use of ``+``)
      - ``=r`` (blank subject)
      - ``u=rws`` (setuid bit)
    """
    if s.startswith("0o"):
        return int(s[2:], 8)
    try:  # nosemgrep this is intended as success is failure in this case
        int(s)
    except ValueError:
        pass
    else:
        raise ValueError("octal number must start with 0o")

    SHIFT_MAP = {
        "u": 6,
        "g": 3,
        "o": 0,
    }

    BIT_MAP = {
        "r": 0o4,
        "w": 0o2,
        "x": 0o1,
    }

    SUBJECT_MAP = {
        "u": "user",
        "g": "group",
        "o": "others",
    }

    result_map = {}

    terms = s.split(",")
    for term in terms:
        if "+" in term or "-" in term:
            raise ValueError(
                f"plus and minus are not supported in term {term!r}"
            )
        lhs, op, rhs = term.partition("=")
        if not op:
            raise ValueError(
                f"missing equal sign in chmod term {term!r}"
            )

        if not lhs:
            raise ValueError(
                f"missing subject in term {term!r}"
            )

        if "a" in lhs:
            lhs = "".join(set(lhs + "ugo") - {"a"})

        for subject in lhs:
            if subject in result_map:
                raise ValueError(
                    f"bits for {SUBJECT_MAP[subject]} specified multiple times"
                )
            if subject not in SHIFT_MAP:
                raise ValueError(
                    f"unsupported subject {subject!r} in term {term!r}"
                )
            try:
                result_map[subject] = functools.reduce(
                    operator.or_,
                    (BIT_MAP[c] for c in rhs),
                    0,
                )
            except KeyError as exc:
                raise ValueError(
                    f"unsupported permission bit {exc!s} in term {term!r}"
                )

    result = functools.reduce(
        operator.or_,
        (bits << SHIFT_MAP[c] for c, bits in result_map.items()),
        0,
    )

    return result


def normalize_cpu(spec: str) -> str:
    """
    Normalize the value in the same way the API server does.

    :param spec: A cpu resource specification.
    :return: A cpu resource specification representing the same value as the
      input.

    The CRD has already validated the value and disallowed floats. So we only
    need to normalize on millicpu by the following rules:

      - millicpu values divisible by 1000 are normalized to full cpus
      - all other values are left untouched

    Examples:

    - ``100m`` -> ``100m``
    - ``1000m`` -> ``1``
    - ``1234m`` -> ``1234m``
    - ``2`` -> ``2``
    """
    match = re.match(r"^([1-9][0-9]*)(m?)$", spec)
    if match is None:
        # This should not happen
        raise ValueError(f"Invalid CPU quantity format: {spec}")

    int_val = int(match.group(1))
    if match.group(2) == 'm' and int_val % 1000 == 0:
        return str(int(int_val/1000))

    return spec


def normalize_memory(spec: str) -> str:
    """
    Normalize the value in the same way the API server does.

    :param spec: A memory resource specification.
    :return: A memory resource specification representing the same value as the
      input.

    The CRD has already validated the value and disallowed floats values.
    So we normalize by the following rules:

      - values without a unit or one of (k|M|G|T|P|E) and divisible by powers
        of 1000 are normalized to the respective unit,
      - values with one of (Ki|Mi|Gi|Ti|Pi|Ei) and divisible by powers
        of 1024 are normalized to the respective unit,
      - all other values are left untouched.

    Examples:

    - ``1234`` -> ``1234``
    - ``100Ki`` -> ``100Ki``
    - ``1000k`` -> ``1M``
    - ``1024Ki`` -> ``1Mi``
    - ``1234k`` -> ``1234k``
    - ``2000000`` -> ``2M``
    - ``1048576Ki`` -> ``1Gi``
    """
    match = re.match(r"^([1-9][0-9]*)([KMGTPE]i)$", spec)
    if match is not None:
        units = [None, "Ki", "Mi", "Gi", "Ti", "Pi", "Ei"]
        base = 1024
        max_val = "7Ei"
    else:
        match = re.match(r"^([1-9][0-9]*)([kMGTPE]?)$", spec)
        if match is not None:
            units = ["", "k", "M", "G", "T", "P", "E"]
            base = 1000
            max_val = "9E"
        else:
            # This should not happen
            raise ValueError(f"Invalid memory quantity format: {spec}")

    int_val = int(match.group(1))
    # The list represents the exponents of the base.
    exponent = units.index(match.group(2))
    for unit in units[exponent:-1]:
        if int_val % base != 0:
            break
        exponent += 1
        int_val = int(int_val/base)
    else:
        unit = units[-1]

    # Handle overflow
    if int_val * base**exponent >= 2**63:
        return max_val

    return f"{int_val}{unit}"


def container_resources(crd_spec: dict, container_path: str) -> dict:
    """
    Find and normalize the resource constraints for the given container in
    the crd spec.

    :param crd_spec: The custom resource data itself, available in the
        template.
    :param container_path: The path of the container for which to extract
        resource constraints. This includes the name of the to-level
        CRD attribute it belongs to, e.g. "api.ssl-terminator".
    :return: The resource constraints or an empty dict if none.

    This function does some simple defaulting. It is used with Deployment CRDs
    which have an optional "resources" field. If the field is missing
    completely or the container_name is not found, an empty dictionary is
    returned.
    """
    path = container_path.split(".")
    container_name = path[-1]
    if path[0] == "job":
        resources = crd_spec.get('jobResources', {})
    else:
        # Walk down the crd_spec along the path
        spec_level = crd_spec
        for level in path[:-1]:
            spec_level = spec_level.get(level, {})
        resources = spec_level.get('resources', {})
    spec = resources.get(container_name)
    if not spec:
        return {}

    for name in ('requests', 'limits'):
        res = spec.get(name)
        if res:
            cpu = res.get('cpu')
            if cpu:
                spec[name]['cpu'] = normalize_cpu(cpu)
            mem = res.get('memory')
            if mem:
                spec[name]['memory'] = normalize_memory(mem)

    return spec


def container_resources_dict(
        crd_spec: dict,
        base_path: str,
        container_names: typing.List[str]) -> dict:
    return_val = {}
    for container in container_names:
        res = container_resources(crd_spec, f"{base_path}.{container}")
        if res:
            return_val[container] = res
    return return_val


def db_proxy_resources(
        crd_spec: dict, base_path: str = "database.proxy") -> dict:
    return container_resources_dict(
        crd_spec, base_path,
        [
            "create-ca-bundle",
            "haproxy",
            "service-reload",
        ])


def db_resources(
        crd_spec: dict, base_path: str = "database") -> dict:
    return container_resources_dict(
        crd_spec, base_path,
        [
            "mariadb-galera",
            "backup-creator",
            "backup-shifter",
            "mysqld-exporter",
        ])


def amqp_resources(
        crd_spec: dict, base_path: str = "messageQueue") -> dict:
    return container_resources_dict(crd_spec, base_path, ["rabbitmq"])


def memcached_resources(
        crd_spec: dict, base_path: str = "memcached") -> dict:
    return container_resources_dict(crd_spec, base_path, ["memcached"])


def ovsdb_resources(
        crd_spec: dict, base_path: str = "ovsdb") -> dict:
    return container_resources_dict(
        crd_spec, base_path,
        [
            "setup-ovsdb",
            "ovsdb",
        ])
