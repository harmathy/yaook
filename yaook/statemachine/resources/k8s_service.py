#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
Kubernetes Service resources
############################

.. currentmodule:: yaook.statemachine.resources

.. autoclass:: Service

.. autoclass:: Ingress

.. autoclass:: TemplatedService

.. autoclass:: TemplatedIngress
"""
import typing

import kubernetes_asyncio.client as kclient
from opentelemetry import trace

from .. import api_utils, context, exceptions, interfaces, watcher
from .k8s import BodyTemplateMixin, SingleObject
from yaook.statemachine.tracing import start_as_current_span_async


tracer = trace.get_tracer(__name__)


class Service(SingleObject[kclient.V1Service]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[
                kclient.V1Service]:
        return interfaces.service_interface(api_client)

    def _needs_update(self,
                      current: kclient.V1Service,
                      new: typing.Mapping) -> bool:

        cyaml = api_utils.k8s_obj_to_yaml_data(current)["spec"]
        if "type" in cyaml and cyaml["type"] != new.get("spec", {}).get(
            "type", "ClusterIP"
        ):
            return True
        return (super()._needs_update(current, new) or
                api_utils.deep_has_changes(cyaml.get("ports"),
                                           new.get("spec", {}).get("ports")) or
                api_utils.deep_has_changes(cyaml.get("selector"),
                                           new.get("spec", {}).get("selector"))
                )


class Ingress(SingleObject[kclient.NetworkingV1Api]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[
                kclient.NetworkingV1Api]:
        return interfaces.ingress_interface(api_client)

    def _needs_update(self,
                      current: kclient.
                      NetworkingV1Api,
                      new: typing.Mapping) -> bool:
        cyaml = api_utils.k8s_obj_to_yaml_data(current)
        return (super()._needs_update(current, new) or
                api_utils.deep_has_changes(cyaml["spec"], new["spec"]))

    @start_as_current_span_async(tracer, "is_ready")
    async def is_ready(self, ctx: context.Context) -> bool:
        """
        Return true if and only if the Job exists and has had at least one
        successful Pod.
        """
        try:
            instance = await self._get_current(ctx)
        except exceptions.ResourceNotPresent:
            return False

        status = instance.status
        if status is None or status == {}:
            return False

        return await super().is_ready(ctx)

    def get_listeners(self) -> typing.List[context.Listener]:
        return super().get_listeners() + [
            context.KubernetesListener[kclient.V1Ingress](
                'networking.k8s.io', 'v1', 'ingresses', self._handle_ingress_event,
                component=self.component,
            ),
        ]

    def _handle_ingress_event(
            self,
            ctx: context.Context,
            event: watcher.StatefulWatchEvent[
                kclient.V1Ingress],
            ) -> bool:
        if event.type_ == watcher.EventType.DELETED:
            return True

        ingress = event.object_
        if not ingress.status:
            return False

        # A status is set, but old_object is not there or had no status -> reconcile
        if event.old_object is None or not event.old_object.status:
            return True
        status = ingress.status
        oldstatus = event.old_object.status
        return (oldstatus.load_balancer != status.load_balancer)


class TemplatedService(BodyTemplateMixin, Service):
    """
    Manage a jinja2-templated Service.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.
    """


class TemplatedIngress(BodyTemplateMixin, Ingress):
    """
    Manage a jinja2-templated Deployment.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.

        :class:`~.IngressState`:
            for arguments specific to Ingresses.
    """
