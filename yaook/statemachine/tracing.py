#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# This code is adapted from the suggestions in
# https://github.com/open-telemetry/opentelemetry-python/issues/62


import functools
import typing

from opentelemetry.trace import Tracer, SpanKind, _Links
from opentelemetry.context import Context
from opentelemetry.util import types

P = typing.ParamSpec("P")
R = typing.TypeVar("R")
F = typing.Callable[P, R]


def start_as_current_span_async(
    tracer: Tracer,
    name: str,
    context: typing.Optional[Context] = None,
    kind: SpanKind = SpanKind.INTERNAL,
    attributes: types.Attributes = None,
    links: _Links = None,
    start_time: typing.Optional[int] = None,
    record_exception: bool = True,
    set_status_on_exception: bool = True,
    end_on_exit: bool = True,
) -> typing.Callable[[F], F]:

    def decorator(function: F) -> F:
        @functools.wraps(function)
        async def wrapper(*args, **kwargs):
            with tracer.start_as_current_span(
                name=name,
                context=context,
                kind=kind,
                attributes=attributes,
                links=links,
                start_time=start_time,
                record_exception=record_exception,
                set_status_on_exception=set_status_on_exception,
                end_on_exit=end_on_exit,
            ):
                return await function(*args, **kwargs)

        return wrapper

    return decorator
