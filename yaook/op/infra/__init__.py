#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os

if "INFRA_OPERATOR_LOCAL_TESTING_DISABLE_DB_CR" not in os.environ:
    from .db_cr import MySQLService, MySQLUser  # noqa:F401

if "INFRA_OPERATOR_LOCAL_TESTING_DISABLE_AMQP_CR" not in os.environ:
    from .amqp_cr import AMQPServer  # noqa:F401

if "INFRA_OPERATOR_LOCAL_TESTING_DISABLE_MEMCACHED_CR" not in os.environ:
    from .memcached_cr import MemcachedService  # noqa:F401

if "INFRA_OPERATOR_LOCAL_TESTING_DISABLE_OVSDB_CR" not in os.environ:
    from .ovsdb_cr import OVSDBService  # noqa:F401

if "INFRA_OPERATOR_LOCAL_TESTING_DISABLE_PDNS_CR" not in os.environ:
    from .powerdns_cr import PowerDNSService  # noqa:F401
