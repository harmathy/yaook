##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: batch/v1
kind: Job
metadata:
  generateName: "powerdns-db-sync-"
spec:
  backoffLimit: 12
  template:
    spec:
        automountServiceAccountToken: false
        enableServiceLinks: false
        initContainers:
        - name: copy-sql-schema
          image: {{ versioned_dependencies['pdns_docker_image'] }}
          imagePullPolicy: IfNotPresent
          command: ["bash", "-c", "cp /usr/local/share/doc/pdns/*.mysql.sql /schema"]
          volumeMounts:
          - name: sql-schema
            mountPath: /schema
        containers:
        - name: powerdns-setup
          image: {{ versioned_dependencies['mariadb_docker_image'] }}
          imagePullPolicy: IfNotPresent
          command:
          - bash
          - "-xec"
          - |
            set -euo pipefail
            target_release='{{ crd_spec.targetRelease }}'
            exists=$(mariadb -se \
              "SELECT count(*)
                FROM information_schema.tables
                WHERE table_schema = 'powerdns' 
                AND table_name = 'migrations';"
            )

            if [[ $exists -eq 0 ]]; then
                mariadb < /schema/schema.mysql.sql
                mariadb -se "CREATE TABLE migrations (id MEDIUMINT NOT NULL AUTO_INCREMENT, version varchar(10) NOT NULL UNIQUE, PRIMARY KEY (id));
                            INSERT INTO migrations (version) VALUES ('$target_release');"
            else
                current_db_version=$(mariadb -se "select version from migrations order by id desc limit 1")
                migration_file="/schema/$current_db_version.0_to_$target_release.0_schema.mysql.sql"

                if [ ! -f $migration_file ]; then
                    echo "[INFO] No migration file $migration_file for version update found"
                else
                    mariadb < $migration_file
                fi
            fi
          volumeMounts:
          - name: sql-schema
            mountPath: /schema
          - name: sql-client-config
            mountPath: /opt/bitnami/mariadb/conf
        volumes:
          - name: sql-schema
            emptyDir: {}
          - name: sql-client-config
            secret:
              secretName: {{ dependencies['db_client_config'].resource_name() }}
              items:
                - key: mariadb_client.conf
                  path: my.cnf
{% if crd_spec.imagePullSecrets | default(False) %}
        imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
        restartPolicy: Never
