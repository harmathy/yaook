##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
{% set service_account_name = dependencies['service_account'].resource_name() %}
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ "%s-mq" | format(labels["state.yaook.cloud/parent-name"]) }}
spec:
  replicas: {{ crd_spec.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  podManagementPolicy: OrderedReady
  serviceName: {{ dependencies['ready_service'].resource_name() }}
  template:
    metadata:
      annotations:
        operator.yaook.cloud/replication-ca-cert-not-before: {{ vars.replication_ca_cert_not_before }}
      labels: {{ pod_labels }}
    spec:
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: {{ crd_spec.scheduleRuleWhenUnsatisfiable }}
          labelSelector:
            matchLabels: {{ pod_labels }}
      securityContext:
        # we might want to choose this one randomly per cluster in a future
        # version
        fsGroup: 2500011
        runAsUser: 2500011
      serviceAccountName: {{ service_account_name }}
      terminationGracePeriodSeconds: 3600
      containers:
        - name: rabbitmq
          image: {{ crd_spec.imageRef }}
          imagePullPolicy: IfNotPresent
          command:
            - /bin/bash
            - -c
            - |
              set -xeuo pipefail
              if [[ "$MY_POD_IP" == *:* ]]; then
                proto=inet6_tls
                cat > $ERL_INETRC <<EOF
              {inet6, true}.
              EOF
              else
                proto=inet_tls
                echo > $ERL_INETRC
              fi
              erl_ssl_path="$(erl -noinput -eval 'io:format("~s~n", [filename:dirname(code:which(inet_tls_dist))])' -s init stop)"
              # The Bitnami scripts override the RABBITMQ_CONF_ENV_FILE (both
              # variable and file), so we cannot use it during initial startup
              # to configure either the server or the rabbimq CLI. It is still
              # useful for liveness probes and kubectl exec, hence we write the
              # client variables in there.
              export RABBITMQ_SERVER_ADDITIONAL_ERL_ARGS="\
                -pa $erl_ssl_path /var/lib/rabbitmq \
                -proto_dist $proto \
                -ssl_dist_optfile /etc/rabbitmq/inter_node_tls.config \
                -mnesia no_table_loaders 30"
              export RABBITMQ_CTL_ERL_ARGS="$RABBITMQ_SERVER_ADDITIONAL_ERL_ARGS"
              cat > $RABBITMQ_CONF_ENV_FILE <<EOF
              RABBITMQ_CTL_ERL_ARGS='\
                -pa $erl_ssl_path /var/lib/rabbitmq \
                -proto_dist $proto \
                -ssl_dist_optfile /etc/rabbitmq/inter_node_tls.config \
                -mnesia no_table_loaders 30'
              NODENAME='rabbit@$(hostname -f)'
              EOF
              # Our custom stop script
              mkdir -p /var/lib/rabbitmq
              cp /etc/rabbitmq/amqp_drain.erl /var/lib/rabbitmq/amqp_drain.erl
              erlc -o /var/lib/rabbitmq/ /var/lib/rabbitmq/amqp_drain.erl
              cat > /var/lib/rabbitmq/.erlang.cookie <<<"$RABBITMQ_ERL_COOKIE"
              chmod 400 /var/lib/rabbitmq/.erlang.cookie
              # the original image enables those plugins, but we shadow that when
              # mounting over /etc/rabbitmq
              cat > ${RABBITMQ_ENABLED_PLUGINS_FILE} <<EOF
              [${RABBITMQ_PLUGINS}].
              EOF
              exec docker-entrypoint.sh rabbitmq-server
          env:
            - name: BITNAMI_DEBUG
              value: "true"
            - name: MY_POD_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP
            - name: MY_POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: MY_POD_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
            - name: K8S_SERVICE_NAME
              value: {{ dependencies['ready_service'].resource_name() }}
            - name: K8S_ADDRESS_TYPE
              value: hostname
            - name: YAOOK_CLUSTER_DOMAIN
              value: {{ cluster_domain }}
            - name: RABBITMQ_SCHEDULER_BIND_TYPE
              value: "u"
            - name: RABBITMQ_FORCE_BOOT
              value: "no"
            - name: RABBITMQ_NODE_NAME
              value: "rabbit@$(MY_POD_NAME).$(K8S_SERVICE_NAME).$(MY_POD_NAMESPACE).svc.$(YAOOK_CLUSTER_DOMAIN)"
            - name: K8S_HOSTNAME_SUFFIX
              value: ".$(K8S_SERVICE_NAME).$(MY_POD_NAMESPACE).svc.$(YAOOK_CLUSTER_DOMAIN)"
            - name: RABBITMQ_LDAP_ENABLE
              value: "no"
            - name: RABBITMQ_LOGS
              value: "-"
            - name: RABBITMQ_ULIMIT_NOFILES
              value: "65535"
            - name: RABBITMQ_USE_LONGNAME
              value: "true"
            - name: RABBITMQ_ERL_COOKIE
              valueFrom:
                secretKeyRef:
                  name: {{ dependencies['cluster_credentials'].resource_name() }}
                  key: rabbitmq-erlang-cookie
            - name: RABBITMQ_DEFAULT_USER
              value: {{ params.rabbitmq_root_username }}
            - name: RABBITMQ_DEFAULT_PASS
              valueFrom:
                secretKeyRef:
                  name: {{ dependencies['cluster_credentials'].resource_name() }}
                  key: rabbitmq-password
            - name: RABBITMQ_SECURE_PASSWORD
              value: "yes"
            - name: RABBITMQ_PLUGINS
              value: {{ crd_spec.enabledPlugins | default('rabbitmq_management,rabbitmq_prometheus') | string }}
            - name: RABBITMQ_CONF_ENV_FILE
              value: /etc/rabbitmq/runtime-conf/rabbitmq-env.conf
            - name: RABBITMQ_ENABLED_PLUGINS_FILE
              value: /var/lib/rabbitmq/enabled_plugins
            - name: SCHEDULER_BUSY_WAIT_THRESHOLD
              value: medium
            - name: RABBITMQ_DISTRIBUTION_BUFFER_SIZE
              value: "265000"
            - name: ERL_INETRC
              value: "/etc/rabbitmq/runtime-conf/inetrc.conf"
          ports:
            - name: amqps
              containerPort: 5671
              protocol: TCP
            - name: stats
              containerPort: 15672
              protocol: TCP
            - name: managements
              containerPort: 15671
              protocol: TCP
            - name: dist
              containerPort: 25672
              protocol: TCP
            - name: epmd
              containerPort: 4369
              protocol: TCP
            - name: metrics
              containerPort: 9419
              protocol: TCP
          lifecycle:
            preStop:
              exec:
                command:
                  - /bin/bash
                  - -ec
                  - |
                    echo "initiating shutdown via preStop"
                    echo "draining node"
                    timeout 600 rabbitmq-upgrade drain || echo "node draining timed out, proceeding anyway"
                    echo "transfering master of mirrored queues"
                    timeout 1800 rabbitmqctl eval 'amqp_drain:failover_mirrored_queues(10).' || echo "migrating mirrored queues timed out, proceeding anyway"
                    echo "stopping rabbitmq app"
                    timeout 600 rabbitmqctl stop_app || echo "graceful shutdown timed out, proceeding anyway"
                    echo "stop_app is done. Now stopping whole erlang"
                    rabbitmqctl stop
          livenessProbe:
            exec:
              command:
                - /bin/bash
                - -ec
                - rabbitmq-diagnostics -q ping
            failureThreshold: 6
            initialDelaySeconds: 120
            periodSeconds: 30
            successThreshold: 1
            timeoutSeconds: 20
          readinessProbe:
            exec:
              command:
                - /bin/bash
                - -ec
                - rabbitmq-diagnostics -q check_running && rabbitmq-diagnostics -q check_local_alarms
            failureThreshold: 3
            initialDelaySeconds: 10
            periodSeconds: 30
            successThreshold: 1
            timeoutSeconds: 20
          volumeMounts:
            - name: runtime-config
              mountPath: /etc/rabbitmq/runtime-conf
            - name: configuration
              mountPath: /etc/rabbitmq/
            - name: data
              mountPath: /var/lib/rabbitmq/mnesia/
            - name: certificates
              mountPath: /etc/rabbitmq/certs
          resources: {{ crd_spec | resources('rabbitmq') }}
      volumes:
        - name: runtime-config
          emptyDir:
            medium: Memory
        - name: configuration
          projected:
            sources:
              - configMap:
                  name: {{ dependencies['amqp_config'].resource_name() }}
                  items:
                    - key: rabbitmq.conf
                      path: rabbitmq.conf
              - configMap:
                  name: {{ dependencies['amqp_inter_node_tls_config'].resource_name() }}
                  items:
                    - key: inter_node_tls.config
                      path: inter_node_tls.config
              - configMap:
                  name: {{ dependencies['amqp_drain_script'].resource_name() }}
                  items:
                  - key: amqp_drain.erl
                    path: amqp_drain.erl
        - name: certificates
          projected:
            sources:
              - secret:
                  name: {{ dependencies['ready_frontend_certificate_secret'].resource_name() }}
                  items:
                    - key: tls.key
                      path: frontend/tls.key
                      mode: {{ 'u=r' | chmod }}
                    - key: tls.crt
                      path: frontend/tls.crt
                      mode: {{ 'u=r' | chmod }}
                    - key: ca.crt
                      path: frontend/ca.crt
                      mode: {{ 'u=r' | chmod }}
              - secret:
                  name: {{ dependencies['ready_replication_certificate_secret'].resource_name() }}
                  items:
                    - key: tls.key
                      path: replication/tls.key
                      mode: {{ 'u=r' | chmod }}
                    - key: tls.crt
                      path: replication/tls.crt
                      mode: {{ 'u=r' | chmod }}
                    - key: ca.crt
                      path: replication/ca.crt
                      mode: {{ 'u=r' | chmod }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
  volumeClaimTemplates:
    - metadata:
        name: data
        labels: {{ pod_labels }}
      spec:
        accessModes:
        - ReadWriteOnce
        volumeMode: Filesystem
        resources:
          requests:
            storage: {{ crd_spec.storageSize }}
{% if crd_spec.storageClassName | default(False) %}
        storageClassName: {{ crd_spec.storageClassName }}
{% endif %}
