// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ceilometer_compute

import (
	"strings"
	"yaook.cloud/ceilometer_template"
)

ceilometer_compute_conf_spec: ceilometer_template.ceilometer_template_conf_spec
ceilometer_compute_conf_spec: {
	service_credentials: {
		password: string
	}
	DEFAULT: {
		#transport_url_hosts:    *["rabbitmq-gnocchi"] | [...string]
		#transport_url_port:     *5672 | int
		#transport_url_username: *"admin" | string
		#transport_url_password: *"" | string
		#transport_url_vhost:    *"" | string
		#transport_url_parts: [ for Host in #transport_url_hosts {"\( #transport_url_username ):\( #transport_url_password )@\( Host ):\( #transport_url_port )"}]
		transport_url: "rabbit://\( strings.Join(#transport_url_parts, ",") )/\( #transport_url_vhost )"
	}
	oslo_messaging_rabbit: {
		amqp_durable_queues: true
		rabbit_quorum_queue: true
	}
}
