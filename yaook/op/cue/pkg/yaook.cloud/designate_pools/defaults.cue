// Copyright (c) 2024 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package designate_pools

designate_pools_conf_spec: [...] & [{
	#pdns_api_host:       string
	#pdns_webserver_host: string
	#designate_mdns_host: string
	#api_host:            string
	#api_token:           string

	"name":        *"default" | string
	"description": *"Default Pool" | string
	"attributes": {}

	#nameservers: [...]
	"nameservers": [...] & [
			{
			"host": *"\( #pdns_webserver_host )" | string
			"port": *53 | int
		},
		for rec in #nameservers {rec},
	]

	ns_records: [...]

	"targets": [...] & [
			{
			"type":        *"pdns4" | string
			"description": *"PowerDNS4 Server" | string

			"masters": [...] & [{
				"host": *"\( #designate_mdns_host )" | string
				"port": *5354 | int
			}]

			"options": {
				"host":         *"\( #pdns_webserver_host )" | string
				"port":         *53 | int
				"api_endpoint": *"https://\( #pdns_api_host ):8100" | string
				"api_token":    *"\( #api_token )" | string
				"api_ca_cert":  *"/etc/pki/tls/certs/ca-bundle.crt" | string
			}
		},
	]
}]
