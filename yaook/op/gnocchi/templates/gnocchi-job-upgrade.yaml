##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: batch/v1
kind: Job
metadata:
  generateName: "gnocchi-upgrade-"
spec:
  template:
    spec:
        automountServiceAccountToken: false
        enableServiceLinks: false
        containers:
          - name: gnocchi-setup
            image:  {{ versioned_dependencies['gnocchi_docker_image'] }}
            imagePullPolicy: IfNotPresent
            command: ["gnocchi-upgrade"]
            volumeMounts:
              - name: gnocchi-config-volume
                mountPath: /etc/gnocchi/gnocchi.conf
                subPath: gnocchi.conf
              - name: ca-certs
                mountPath: /etc/pki/tls/certs
              {% if use_ceph %}
              - name: gnocchi-ceph-conf-volume
                mountPath: /etc/ceph/ceph.conf
                subPath: ceph.conf
              - name: gnocchi-ceph-keyfile-volume
                mountPath: /etc/ceph/keyfile
                subPath: keyfile
              {% endif %}
            env:
              - name: REQUESTS_CA_BUNDLE
                value: /etc/pki/tls/certs/ca-bundle.crt
            resources: {{ crd_spec | resources('job.gnocchi-upgrade-job') }}
        volumes:
          - name: gnocchi-config-volume
            secret:
              secretName: {{ dependencies['config'].resource_name() }}
              items:
                - key: gnocchi.conf
                  path: gnocchi.conf
          - name: ca-certs
            configMap:
              name: {{ dependencies['ca_certs'].resource_name() }}
          {% if use_ceph %}
          - name: gnocchi-ceph-conf-volume
            secret:
              secretName: {{ dependencies['config'].resource_name() }}
              items:
                - key: ceph.conf
                  path: ceph.conf
          - name: gnocchi-ceph-keyfile-volume
            secret:
              secretName: {{ crd_spec.backends.ceph.keyringReference }}
              items:
                - key: {{ crd_spec.backends.ceph.keyringUsername }}
                  path: keyfile
          {% endif %}
{% if crd_spec.imagePullSecrets | default(False) %}
        imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
        restartPolicy: Never
