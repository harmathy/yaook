##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: Deployment
metadata:
  generateName: {{ "ceilometer-agent-notification-%s-" | format(labels['state.yaook.cloud/parent-name']) }}
spec:
  replicas:  {{ crd_spec.notification.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
      annotations:
        config-timestamp: {{ dependencies['config'].last_update_timestamp() }}
    spec:
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: {{ crd_spec.notification.scheduleRuleWhenUnsatisfiable }}
          labelSelector:
            matchLabels: {{ pod_labels }}
      automountServiceAccountToken: false
      enableServiceLinks: false
      containers:
        - name: "ceilometer-agent-notification"
          image:  {{ versioned_dependencies['ceilometer_notification_docker_image'] }}
          imagePullPolicy: IfNotPresent
          command: ["ceilometer-agent-notification"]
          volumeMounts:
            - name: ceilometer-config-volume
              mountPath: /etc/ceilometer/ceilometer.conf
              subPath: ceilometer.conf
            # TODO: as soon as we define default configs at defaults.cue,
            # this check needs to be removed/changed
            {% if crd_spec.ceilometerEventDefinitions | default(False) %}
            - name: event-definitions
              mountPath: /etc/ceilometer/event_definitions.yaml
              subPath: event_definitions.yaml
            {% endif %}
            {% if crd_spec.ceilometerEventPipeline | default(False) %}
            - name: ceilometer-config-volume
              mountPath: /etc/ceilometer/event_pipeline.yaml
              subPath: event_pipeline.yaml
            {% endif %}
            {% if crd_spec.ceilometerPipeline | default(False) %}
            - name: ceilometer-config-volume
              mountPath: /etc/ceilometer/pipeline.yaml
              subPath: pipeline.yaml
            {% endif %}
            {% if crd_spec.pankoConfig | default(False) %}
            - name: ceilometer-config-volume
              mountPath: /etc/panko/panko.conf
              subPath: panko.conf
            {% endif %}
            - name: ca-certs
              mountPath: /etc/ssl/certs
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-bundle.crt
          # There are no readiness and liveness probes defined for the notification deployment
          # because the notification agent has no api to test if it works.
          resources: {{ crd_spec | resources('notification.ceilometer-agent-notification') }}
      {% if crd_spec.additionalHosts | default(False) %}
      hostAliases:
        {% for entry in crd_spec.additionalHosts %}
        - ip: {{ entry["ip"] }}
          hostnames:
          {% for name in entry["hostnames"] %}
          - {{ name }}
          {% endfor %}
        {% endfor %}
      {% endif %}
      volumes:
        - name: ceilometer-config-volume
          secret:
            secretName: {{ dependencies['config'].resource_name() }}
            items:
              - key: ceilometer.conf
                path: ceilometer.conf
              {% if crd_spec.ceilometerEventPipeline | default(False) %}
              - key: ceilometer_event_pipeline.yaml
                path: event_pipeline.yaml
              {% endif %}
              {% if crd_spec.ceilometerPipeline | default(False) %}
              - key: ceilometer_pipeline.yaml
                path: pipeline.yaml
              {% endif %}
              {% if crd_spec.pankoConfig | default(False) %}
              - key: panko.conf
                path: panko.conf
              {% endif %}
        {% if crd_spec.ceilometerEventDefinitions | default(False) %}
        - name: event-definitions
          configMap:
            name: {{ dependencies['event_definition_config'].resource_name() }}
        {% endif %}
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
