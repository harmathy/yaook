##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: Deployment
metadata:
  generateName: {{ "ceilometer-agent-central-%s-" | format(labels['state.yaook.cloud/parent-name']) }}
spec:
  replicas:  {{ crd_spec.central.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
      annotations:
        config-timestamp: {{ dependencies['config'].last_update_timestamp() }}
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: {{ crd_spec.central.scheduleRuleWhenUnsatisfiable }}
          labelSelector:
            matchLabels: {{ pod_labels }}
      containers:
        - name: "ceilometer-agent-central"
          image:  {{ versioned_dependencies['ceilometer_docker_image'] }}
          imagePullPolicy: IfNotPresent
          command: ["ceilometer-polling", "--polling-namespaces", "central"]
          volumeMounts:
            - name: ceilometer-config-volume
              mountPath: /etc/ceilometer/ceilometer.conf
              subPath: ceilometer.conf
            - name: ca-certs
              mountPath: /etc/ssl/certs
            - name: polling
              mountPath: /etc/ceilometer/polling.yaml
              subPath: polling.yaml
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-bundle.crt
          # There are no readiness and liveness probes defined for the central deployment
          # because the central agent has no api to test if it works.
          resources: {{ crd_spec | resources('central.ceilometer-agent-central') }}
      volumes:
        - name: ceilometer-config-volume
          secret:
            secretName: {{ dependencies['config'].resource_name() }}
            items:
              - key: ceilometer.conf
                path: ceilometer.conf
        - name: polling
          configMap:
            name: {{ dependencies['polling_config'].resource_name() }}
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
