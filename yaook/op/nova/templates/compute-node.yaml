##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: compute.yaook.cloud/v1
kind: NovaComputeNode
metadata:
  name: {{ instance }}
spec:
  keystoneRef:
    name: {{ dependencies['keystone'].resource_name() }}
    kind: {{ crd_spec.keystoneRef.kind }}
  novaConfig: {{ vars.nova_config }}
  computeProviderConfig: {{ vars.nova_provider_config }}
{% if vars.hostAggregates | default(False) %}
  hostAggregates: {{ vars.hostAggregates }}
{% endif %}
  cephBackend: {{ vars.ceph_backend }}
  publicKeysSecretRef:
    name: {{ dependencies['public_keys'].resource_name() }}
  caConfigMapName: {{ dependencies['ca_certs'].resource_name() }}
  vnc:
    issuerRef:
      name: {{ dependencies['vnc_backend_ca'].resource_name() }}
    baseUrl: {{ "https://{}/vnc_lite.html".format(crd_spec.vnc.ingress.fqdn) }}
  eviction: {{ crd_spec.eviction }}
  messageQueue:
    amqpServerRef:
      name: {{ dependencies['cell1_mq'].resource_name() }}
{% if crd_spec.imagePullSecrets | default(False) %}
  imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
{% if crd_spec.ids | default(False) and crd_spec.ids.cinderGid | default(False) %}
  ids:
    cinderGid: {{ crd_spec.ids.cinderGid }}
{% endif %}
  targetRelease: {{ target_release }}
  memcachedRef:
    name: {{ dependencies['memcached'].resource_name() }}
  region:
    name: {{ crd_spec.region.name }}
  resources:
    keygen: {{ crd_spec | resources('compute.keygen') }}
    chown-nova: {{ crd_spec | resources('compute.chown-nova') }}
    nova-compute: {{ crd_spec | resources('compute.nova-compute') }}
    nova-compute-ssh: {{ crd_spec | resources('compute.nova-compute-ssh') }}
    libvirtd: {{ crd_spec | resources('compute.libvirtd') }}
    compute-evict-job: {{ crd_spec | resources('compute.compute-evict-job') }}
{% if crd_spec.sysinfo | default(False) %}
  sysinfoConfigMapRef:
    name: {{ dependencies['sysinfo_config'].resource_name() }}
{% endif %}
{% if versioned_dependencies %}
  imageRef:
{% if 'nova_compute_docker_image' in versioned_dependencies %}
    nova-compute: {{ versioned_dependencies['nova_compute_docker_image'] }}
{% endif %}
{% endif %}
