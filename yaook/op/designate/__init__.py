#!/usr/bin/env python3
#
# Copyright (c) 2024 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import base64
import typing

import kubernetes_asyncio.client as kclient

import yaook.op.common as common
import yaook.common.config as common_config
import yaook.op.scheduling_keys as scheduling_keys
import yaook.statemachine as sm


JOB_SCHEDULING_KEYS = [
    scheduling_keys.SchedulingKey.OPERATOR_DESIGNATE.value,
    scheduling_keys.SchedulingKey.OPERATOR_ANY.value,
]

SERVICE_NAME = MQ_NAME = DATABASE_NAME = "designate"
API_SVC_USERNAME = "api"
DESIGNATE_POOL_CREATION_ERR = "Failed to create layer for designate_pools. "


def _internal_endpoint_configured(ctx: sm.Context) -> bool:
    return ctx.parent_spec["api"].get("internal", {}) != {}


class DesignatePoolsLayer(sm.CueLayer):
    def __init__(
            self,
            *,
            target: str,
            pdns_api_service:
            sm.resources.KubernetesReference[kclient.V1Service],
            pdns_webserver_service:
            sm.resources.KubernetesReference[kclient.V1Service],
            designate_mdns_service:
            sm.resources.KubernetesReference[kclient.V1Service],
            api_key_secret:
            sm.KubernetesReference[kclient.V1Secret],
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._declare_dependencies(
            pdns_api_service, pdns_webserver_service,
            designate_mdns_service,
        )

        self._target = target
        self._pdns_api_service = pdns_api_service
        self._pdns_udp_service = pdns_webserver_service
        self._designate_mdns_service = designate_mdns_service
        self._api_key_secret = api_key_secret

    async def get_layer(self, ctx: sm.Context) -> sm.cue.CueInput:
        return {
            self._target: common_config.YAML_CONFIG.declare([
                await self.make_pools_option_config_overlay(
                    ctx,
                ),
            ])
        }

    async def get_host(
        self, ctx: sm.context.Context,
        svc: sm.resources.KubernetesReference[kclient.V1Service],
    ) -> str:

        service_ref = await svc.get(ctx)
        return f"{service_ref.name}.{service_ref.namespace}"

    async def get_cluster_ip(
            self, ctx: sm.context.Context,
            svc: sm.resources.KubernetesReference[kclient.V1Service],
    ) -> str:
        # FIXME this is not optimal because the same Kubernetes API endpoint is
        # called twice
        # yaook.statemachine changes are required for a single API call
        service_ref = await svc.get(ctx)
        svc_interface = svc.get_resource_interface(ctx)
        svc_spec = await svc_interface.read(
            service_ref.namespace, service_ref.name
        )
        return svc_spec.spec.cluster_ip

    async def make_pools_option_config_overlay(
            self,
            ctx: sm.context.Context,
            ) -> typing.Iterable[typing.Mapping[str, typing.Any]]:
        try:

            pdns_api_host = await self.get_host(
                ctx, self._pdns_api_service
            )

            # the IPs are used because Designate currently does not accept
            # host names for both Designate mini DNS and the PDNS DNS service
            designate_mdns_id_ip = await self.get_cluster_ip(
                ctx, self._designate_mdns_service
            )
            pdns_webserver_ip = await self.get_cluster_ip(
                ctx, self._pdns_udp_service
            )

            api_token = await self._get_api_secret(ctx)

            ns_records = ctx.parent_spec.get('nsRecords', [])
            nameservers = ctx.parent_spec.get('additionalNameservers', [])

            return [{
                "#pdns_api_host": f"{pdns_api_host}.svc",
                "#pdns_webserver_host": pdns_webserver_ip,
                "#designate_mdns_host": designate_mdns_id_ip,
                "#api_token": api_token,
                "#nameservers": nameservers,
                "ns_records": ns_records,
            }]
        except kclient.ApiException as e:
            raise RuntimeError(
                DESIGNATE_POOL_CREATION_ERR +
                "Failed to read Kubernetes dependency",
                repr(e),
            )

    async def _get_api_secret(self, ctx: sm.Context) -> str:
        secret_ref = await self._api_key_secret.get(ctx)
        secret_key = "password"

        secrets = sm.interfaces.secret_interface(ctx.api_client)
        secret = (await secrets.read(ctx.namespace, secret_ref.name)).data

        base64value = secret.get(secret_key)
        if not base64value:
            raise sm.ConfigurationInvalid(
                DESIGNATE_POOL_CREATION_ERR +
                f"Secret {secret_ref.name!r} has no key {secret_key!r} " +
                "which is configured as PowerDNS API key."
            )

        value = base64.b64decode(base64value).decode('utf-8')

        return value


class Designate(sm.ReleaseAwareCustomResource):
    API_GROUP = "yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "designatedeployments"
    KIND = "DesignateDeployment"
    RELEASES = [
        "2024.1",
    ]
    VALID_UPGRADE_TARGETS: typing.List[str] = []

    # region PowerDNS
    powerdns_version = sm.MappedVersionedDependency(
        mapping={
            "2024.1": "4.9",
        },
        targetfn=lambda ctx: sm.version_utils.get_target_release(ctx),
    )

    powerdns_api_key = sm.AutoGeneratedPassword(
        metadata=("powerdns-api-key-", True),
        copy_on_write=True,
    )

    powerdns = sm.TemplatedPowerDNSService(
        template="powerdns.yaml",
        add_dependencies=[powerdns_api_key],
        versioned_dependencies=[powerdns_version],
    )
    powerdns_api_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=powerdns,
        foreign_component=common.POWERDNS_API_SERVICE_COMPONENT,
    )
    powerdns_webserver_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=powerdns,
        foreign_component=common.POWERDNS_WEBSERVER_SERVICE_COMPONENT,
    )
    # endregion PowerDNS

    designate_docker_image = common.image_dependencies(
        "designate-{release}",
        RELEASES,
    )

    mariadb_version = sm.MappedVersionedDependency(
        mapping={
            release: "11.0"
            for release in [
                "2024.1",
            ]
        },
        targetfn=lambda ctx: sm.version_utils.get_target_release(ctx),
    )

    # region Message Queue
    rabbitmq_image = sm.VersionedDockerImage(
        "library/rabbitmq",
        sm.SemVerSelector([">=3.8.0", "<4.0.0"], suffix="-management"),
    )

    mq = sm.TemplatedAMQPServer(
        template="message-queue.yaml",
        params={
            "mq_name": MQ_NAME,
        },
        versioned_dependencies=[rabbitmq_image],
    )
    mq_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=mq,
        foreign_component=common.AMQP_SERVER_SERVICE_COMPONENT,
    )
    mq_api_user_password = sm.AutoGeneratedPassword(
        metadata=("designate-mq-user-", True),
        copy_on_write=True,
    )
    mq_api_user = sm.SimpleAMQPUser(
        metadata=("designate-api-", True),
        server=mq,
        username_format=API_SVC_USERNAME,
        password_secret=mq_api_user_password,
    )
    # endregion Message Queue

    # region Designate MariaDB

    db = sm.TemplatedMySQLService(
        template="database.yaml",
        params={
            "generate_name": SERVICE_NAME,
            "db_name": DATABASE_NAME,
        },
        versioned_dependencies=[mariadb_version],
    )

    db_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=db,
        foreign_component=common.MYSQL_DATABASE_SERVICE_COMPONENT,
    )

    db_api_user_password = sm.AutoGeneratedPassword(
        metadata=("designate-api-db-user-", True),
        copy_on_write=True,
    )

    db_api_user = sm.SimpleMySQLUser(
        metadata=("designate-api-", True),
        database=db,
        username=API_SVC_USERNAME,
        password_secret=db_api_user_password,
    )

    # endregion MariaDb

    # region Memcached

    memcached_image = sm.VersionedDockerImage(
        "bitnami/memcached",
        sm.BitnamiVersionSelector([
            ([">=1.6.10", "<2.0.0"], []),
        ]),
    )

    memcached = sm.TemplatedMemcachedService(
        template="memcached.yaml",
        versioned_dependencies=[memcached_image],
    )
    memcached_statefulset = sm.ForeignResourceDependency(
        resource_interface_factory=sm.stateful_set_interface,
        foreign_resource=memcached,
        foreign_component=common.MEMCACHED_STATEFUL_COMPONENT,
    )
    memcached_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=memcached,
        foreign_component=common.MEMCACHED_SERVICE_COMPONENT,
    )

    # endregion

    # region Policy validation
    new_designate_policy = sm.ReleaseAwarePolicyConfigMap(
        metadata=("designate-policy-", True),
        component=common.POLICY_CONFIGMAP_COPMONENT,
        copy_on_write=True,
        versioned_dependencies=[
            designate_docker_image,
        ]
    )

    ready_designate_policy = sm.ReadyPolicyConfigMapReference(
        configmap_reference=new_designate_policy,
    )

    policy_validation_management_role = sm.TemplatedRole(
        template="common-policy-validation-role.yaml",
        params={
            "name": SERVICE_NAME,
        },
        add_dependencies=[
            new_designate_policy,
        ]
    )

    policy_validation_management_service_account = \
        sm.TemplatedServiceAccount(
            template="common-policy-validation-serviceaccount.yaml",
            component=common.SERVICE_ACCOUNT_COPMONENT,
            params={
                "name": SERVICE_NAME,
            },
        )

    policy_validation_management_role_binding = \
        sm.TemplatedRoleBinding(
            template="common-policy-validation-role-binding.yaml",
            params={
                "name": SERVICE_NAME,
            },
            add_dependencies=[
                policy_validation_management_role,
                policy_validation_management_service_account,
            ]
        )

    script = sm.PolicyValidationScriptConfigMap(
        metadata=("designate-policy-validation-script-", True),
    )

    policy_validation = sm.PolicyValidator(
        template="common-policy-validator.yaml",
        params={
            "name": SERVICE_NAME,
        },
        scheduling_keys=JOB_SCHEDULING_KEYS,
        add_dependencies=[
            new_designate_policy,
            script,
            policy_validation_management_service_account,
        ],
        versioned_dependencies=[
            designate_docker_image,
        ]
    )
    # endregion Policy validation

    # region KeystoneReference
    keystone = sm.KeystoneReference()
    keystone_internal_api = common.keystone_api_config_reference(keystone)
    keystone_user = sm.StaticKeystoneUser(
        keystone=keystone,
        username="designate",
    )
    keystone_user_credentials = \
        common.keystone_user_credentials_reference(keystone_user)
    # endregion KeystoneReference

    # region SSL Termination
    ssl_terminator_image = sm.ConfigurableVersionedDockerImage(
        'ssl-terminator',
        sm.YaookSemVerSelector(),
    )
    service_reload_image = sm.ConfigurableVersionedDockerImage(
        'service-reload',
        sm.YaookSemVerSelector(),
    )
    certificate_secret = sm.EmptyTlsSecret(
        metadata=("designate-api-certificate-", True),
    )
    certificate = sm.TemplatedCertificate(
        template="designate-api-certificate.yaml",
        add_dependencies=[certificate_secret],
    )
    ready_certificate_secret = sm.ReadyCertificateSecretReference(
        certificate_reference=certificate,
    )
    ca_certs = sm.CAConfigMap(
        metadata=("designate-ca-certificates-", True),
        usercerts_spec_key="caCertificates",
        certificate_secrets_states=[
            ready_certificate_secret,
        ],
    )

    external_certificate_secret = sm.ExternalSecretReference(
        external_secret=lambda ctx: (
            ctx.parent_spec["api"]["ingress"]
            .get("externalCertificateSecretRef", {}).get("name")
        ),
        secret_reference=ready_certificate_secret,
    )

    internal_certificate_secret = sm.ExternalSecretReference(
       external_secret=lambda ctx: (
           ctx.parent_spec["api"].get("internal", {}).get("ingress", {})
           .get("externalCertificateSecretRef", {}).get("name")
       ),
       secret_reference=ready_certificate_secret,
    )
    # endregion SSL Termination

    # region Jobs and Deployments
    config = sm.CueSecret(
        metadata=("designate-config-", True),
        copy_on_write=True,
        add_cue_layers=[
            sm.SpecLayer(
                target="designate",
                accessor="designateConfig",
            ),
            sm.DatabaseConnectionLayer(
                target="designate",
                service=db_service,
                driver=lambda ctx:
                    ctx.parent_spec.get("database", {})
                                   .get("clientDriver", "pymysql"),
                database_name=DATABASE_NAME,
                username=API_SVC_USERNAME,
                password_secret=db_api_user_password,
                config_section="storage:sqlalchemy",
            ),
            sm.AMQPTransportLayer(
                target="designate",
                service=mq_service,
                username=API_SVC_USERNAME,
                password_secret=mq_api_user_password,
            ),
            sm.KeystoneAuthLayer(
                target="designate",
                credentials_secret=keystone_user_credentials,
                endpoint_config=keystone_internal_api,
            ),
            sm.MemcachedConnectionLayer(
                target="designate",
                memcached_sfs=memcached_statefulset,
                memcached_svc=memcached_service,
            ),
        ]
    )

    db_sync = sm.TemplatedJob(
        template="designate-job-db-sync.yaml",
        scheduling_keys=JOB_SCHEDULING_KEYS,
        add_dependencies=[config, ca_certs],
        versioned_dependencies=[designate_docker_image],
    )

    designate_deployment_dependencies = [
        config, db_sync,
        ca_certs,
        ready_certificate_secret,
        ready_designate_policy,
        internal_certificate_secret,
    ]
    deployment_versioned_dependencies = [
        designate_docker_image,
        ssl_terminator_image,
        service_reload_image,
    ]

    central_deployment = sm.TemplatedDeployment(
        template="designate-service-deployment.yaml",
        scheduling_keys=[
            scheduling_keys.SchedulingKey.DESIGNATE_CENTRAL.value,
            scheduling_keys.SchedulingKey.DESIGNATE_ANY_SERVICE.value,
        ],
        add_dependencies=designate_deployment_dependencies,
        versioned_dependencies=deployment_versioned_dependencies,
        params={
            "name": "central",
            "command": "designate-central",
        }
    )

    minidns_deployment = sm.TemplatedDeployment(
        template="designate-service-deployment.yaml",
        scheduling_keys=[
            scheduling_keys.SchedulingKey.DESIGNATE_MDNS.value,
            scheduling_keys.SchedulingKey.DESIGNATE_ANY_SERVICE.value,
        ],
        add_dependencies=designate_deployment_dependencies,
        versioned_dependencies=deployment_versioned_dependencies,
        params={
            "name": "minidns",
            "command": "designate-mdns",
        }
    )

    mdns_service = sm.TemplatedService(
        template="designate-mdns-service.yaml",
        add_dependencies=[minidns_deployment],
    )

    pools_config = sm.CueSecret(
        metadata=("designate-pools-", True),
        copy_on_write=True,
        add_cue_layers=[
            DesignatePoolsLayer(
                target="designate_pools",
                pdns_api_service=powerdns_api_service,
                pdns_webserver_service=powerdns_webserver_service,
                designate_mdns_service=mdns_service,
                api_key_secret=powerdns_api_key,
            ),
        ]
    )

    pool_update = sm.TemplatedJob(
        template="designate-job-pool-update.yaml",
        scheduling_keys=JOB_SCHEDULING_KEYS,
        add_dependencies=[config, ca_certs, central_deployment, pools_config],
        versioned_dependencies=[designate_docker_image],
    )

    api_deployment = sm.TemplatedDeployment(
        template="designate-api-deployment.yaml",
        scheduling_keys=[
            scheduling_keys.SchedulingKey.DESIGNATE_API.value,
            scheduling_keys.SchedulingKey.ANY_API.value,
        ],
        add_dependencies=designate_deployment_dependencies
        + [external_certificate_secret, powerdns, pools_config],
        versioned_dependencies=deployment_versioned_dependencies,
    )

    api_service = sm.TemplatedService(
        template="designate-api-service.yaml",
        add_dependencies=[api_deployment],
    )

    api_ingress = sm.Optional(
        condition=common.create_ingress,
        wrapped_state=sm.TemplatedIngress(
            template="designate-api-ingress.yaml",
            add_dependencies=[api_service],
        )
    )

    internal_api_ingress = sm.Optional(
        condition=_internal_endpoint_configured,
        wrapped_state=sm.TemplatedIngress(
            template="designate-internal-api-ingress.yaml",
            add_dependencies=[api_service],
        )
    )

    # the worker, producer and mdns services
    # require the pool update job to be run before
    designate_deployment_dependencies.append(pool_update)

    worker_deployment = sm.TemplatedDeployment(
        template="designate-service-deployment.yaml",
        scheduling_keys=[
            scheduling_keys.SchedulingKey.DESIGNATE_WORKER.value,
            scheduling_keys.SchedulingKey.DESIGNATE_ANY_SERVICE.value,
        ],
        add_dependencies=designate_deployment_dependencies + [pools_config],
        versioned_dependencies=deployment_versioned_dependencies,
        params={
            "name": "worker",
            "command": "designate-worker",
        }
    )

    producer_deployment = sm.TemplatedDeployment(
        template="designate-service-deployment.yaml",
        scheduling_keys=[
            scheduling_keys.SchedulingKey.DESIGNATE_PRODUCER.value,
            scheduling_keys.SchedulingKey.DESIGNATE_ANY_SERVICE.value,
        ],
        add_dependencies=designate_deployment_dependencies + [pools_config],
        versioned_dependencies=deployment_versioned_dependencies,
        params={
            "name": "producer",
            "command": "designate-producer",
            "replicas": 1,
            # TODO a distributed lock manager is required to run more than 1
            # replica
        }
    )
    # endregion Jobs and Deployments

    # region QuorumPodDisruptionBudget
    api_deployment_pdb = sm.QuorumPodDisruptionBudget(
        metadata=lambda ctx: f"{ctx.parent_name}-api_deployment_pdb",
        replicated=api_deployment,
    )
    minidns_deployment_pdb = sm.QuorumPodDisruptionBudget(
        metadata=lambda ctx: f"{ctx.parent_name}-minidns_deployment_pdb",
        replicated=minidns_deployment,
    )
    worker_deployment_pdb = sm.QuorumPodDisruptionBudget(
        metadata=lambda ctx: f"{ctx.parent_name}-worker_deployment_pdb",
        replicated=worker_deployment,
    )
    central_deployment_pdb = sm.QuorumPodDisruptionBudget(
        metadata=lambda ctx: f"{ctx.parent_name}-central_deployment_pdb",
        replicated=central_deployment,
    )
    producer_deployment_pdb = sm.QuorumPodDisruptionBudget(
        metadata=lambda ctx: f"{ctx.parent_name}-producer_deployment_pdb",
        replicated=producer_deployment,
    )
    # endregion QuorumPodDisruptionBudget

    # region API Monitoring
    internal_ssl_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-internal-ssl-service-monitor-",
            True),
        service=api_service,
        certificate=ready_certificate_secret,
        endpoints=["internal-ssl-terminator-prometheus"],
    )

    external_ssl_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-external-ssl-service-monitor-",
            True),
        service=api_service,
        certificate=external_certificate_secret,
        server_name_provider=lambda ctx: (
            ctx.parent_spec["api"]["ingress"]["fqdn"]
        ),
        endpoints=["external-ssl-terminator-prometheus"],
    )

    internal_ingress_ssl_service_monitor = sm.Optional(
        condition=_internal_endpoint_configured,
        wrapped_state=sm.GeneratedServiceMonitor(
            metadata=lambda ctx: (
                f"{ctx.parent_name}-internal-ingress-ssl-service-monitor-",
                True),
            service=api_service,
            certificate=internal_certificate_secret,
            server_name_provider=lambda ctx: (
                ctx.parent_spec["api"]["internal"]["ingress"]["fqdn"]
            ),
            endpoints=["internal-ingress-ssl-terminator-prometheus"],
        )
    )
    # endregion API Monitoring

    keystone_endpoint = sm.Optional(
        condition=common.publish_endpoint,
        wrapped_state=sm.TemplatedKeystoneEndpoint(
            template="designate-keystone-endpoint.yaml",
            add_dependencies=[keystone],
        )
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)


sm.register(Designate)
