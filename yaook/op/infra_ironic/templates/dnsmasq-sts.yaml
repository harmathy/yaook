##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ "%s-dnsmasq" | format(labels['state.yaook.cloud/parent-name']) }}
  annotations:
    test.yaook.cloud/skip-ca-certs: "true"
spec:
  replicas: 1
  serviceName: {{ dependencies['dnsmasq_service'].resource_name() }}
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: {{ crd_spec.dnsmasq.scheduleRuleWhenUnsatisfiable }}
          labelSelector:
            matchLabels: {{ pod_labels }}
      hostNetwork: true
      containers:
      - name: dnsmasq
        image: {{ versioned_dependencies["dnsmasq_image"] }}
        command:
        - bash
        - -euxo
        - pipefail
        - -c
        - |
          listen_ip=$(ip a | grepcidr "$DHCP_LISTEN_CIDR" | head -n1 | awk '{print $2}' | cut -d '/' -f1)
          LISTEN_IP="$listen_ip" envsubst '$LISTEN_IP' < /etc/dnsmasq.d/config.conf > /etc/dnsmasq.conf
          if [[ "$DHCP_LISTEN_CIDR" == *:* ]]; then
            # with IPv6, passing --listen-address causes havoc and dnsmasq will
            # not send router advertisments despite explicitly being asked to.
            # we need to instead set the --interface flag to the interface we
            # want to listen on.
            interface=$(ip -6 -o a | grepcidr "$DHCP_LISTEN_CIDR" | awk '{print $2}' | head -n1)
            exec dnsmasq -d --log-debug --interface "$interface" --except-interface=lo --bind-interfaces
          else
            # with IPv4, we need to pass --listen-address set to the actual
            # address bound to the interface on which we want to talk DHCP.
            # this is done using grepcidr.
            exec dnsmasq -d --listen-address="$listen_ip" --bind-interfaces --log-debug
          fi
        env:
        - name: DHCP_LISTEN_CIDR
          value: {{ crd_spec.pxe.listenNetwork }}
        volumeMounts:
        - name: config
          mountPath: /etc/dnsmasq.d
        - name: state
          mountPath: /var/lib/dnsmasq
        securityContext:
          capabilities:
            add:
            - NET_ADMIN
        resources: {{ crd_spec | resources('dnsmasq.dnsmasq') }}
      volumes:
      - name: config
        projected:
          sources:
          - configMap:
              name: {{ dependencies['dnsmasq_config'].resource_name() }}
              items:
              - key: dnsmasq.conf
                path: config.conf
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
  volumeClaimTemplates:
  - metadata:
      name: state
    spec:
      accessModes:
      - ReadWriteOnce
      storageClassName: {{ crd_spec.dnsmasq.storageClassName }}
      resources:
        requests:
          storage: {{ crd_spec.dnsmasq.storageSize }}
