##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: v1
kind: ConfigMap
metadata:
  generateName: {{ "%s-dnsmasq-" | format(labels['state.yaook.cloud/parent-name']) }}
data:
  dnsmasq.conf: |
    # Ensure dnsmasq sends router advertisments in IPv6.
    enable-ra

    # We do not want to offer general DNS resolution, only our injected
    # hostnames.
    no-resolv

    {% for x in vars.dhcpconfig %}
    {{ x | unsafe }}{% endfor %}

    # Expose DNS records for the ingress

{% if crd_spec.ingressAddress | default(False) %}
    host-record={{ crd_spec.api.ingress.fqdn | unsafe }},{{ crd_spec.ingressAddress | unsafe }}
    host-record={{ crd_spec.inspectorApi.ingress.fqdn | unsafe }},{{ crd_spec.ingressAddress | unsafe }}
    host-record={{ crd_spec.imageServer.ingress.fqdn | unsafe }},{{ crd_spec.ingressAddress |  unsafe }}
{% else %}
    host-record={{ crd_spec.api.ingress.fqdn | unsafe }},{{ apiIngressAddress | unsafe }}
    host-record={{ crd_spec.inspectorApi.ingress.fqdn | unsafe }},{{ inspectorApiIngressAddress | unsafe }}
    host-record={{ crd_spec.imageServer.ingress.fqdn | unsafe }},{{ imageServerIngressAddress |  unsafe }}
{% endif %}

    enable-tftp
    tftp-root=/tftpboot

    # Filter if we are booting using ipxe
    dhcp-match=set:ipxe,175

    # Filter if we are booting an x86 uefi system
    dhcp-match=set:x86-uefi,option:client-arch,6
    dhcp-match=set:x86-uefi,option:client-arch,7
    dhcp-match=set:x86-uefi,option:client-arch,9
    dhcp-match=set:x86-uefi,option6:61,6
    dhcp-match=set:x86-uefi,option6:61,7
    dhcp-match=set:x86-uefi,option6:61,9

    # Filter if we are booting an arm64 system
    dhcp-match=set:arm64-uefi,option:client-arch,11
    dhcp-match=set:arm64-uefi,option6:61,11

    # Detect iPXE over IPv6
    dhcp-userclass=set:ipxe6,iPXE
    dhcp-vendorclass=set:ipxe6,HTTP

    # Boot the ipxe chainloader for x86-uefi
    # Note that we need to filter for !ipxe as we overwrite it otherwise
    dhcp-boot=tag:x86-uefi,tag:!ipxe,/ipxe.efi
    dhcp-option=tag:!ipxe6,tag:x86-uefi,option6:bootfile-url,tftp://[{{ crd_spec.tftpAddress | default("$LISTEN_IP") | unsafe }}]/ipxe.efi

    # Boot ipxe for arm64
    dhcp-boot=tag:arm64-uefi,tag:!ipxe,/ipxe-arm64.efi
    dhcp-option=tag:!ipxe6,tag:arm64-uefi,option6:bootfile-url,tftp://[{{ crd_spec.tftpAddress | default("$LISTEN_IP") | unsafe }}]/ipxe-arm64.efi

    # Boot the ipxe image if we use ipxe based on the arch of the client
    dhcp-boot=tag:ipxe,tag:x86-uefi,http://{{ "%s:%s" | format(crd_spec.imageServer.ingress.fqdn, crd_spec.imageServer.ingress.port) | unsafe }}/boot-x86.ipxe
    dhcp-option=tag:ipxe6,tag:x86-uefi,option6:bootfile-url,http://{{ "%s:%s" | format(crd_spec.imageServer.ingress.fqdn, crd_spec.imageServer.ingress.port) | unsafe }}/boot-x86.ipxe
    dhcp-boot=tag:ipxe,tag:arm64-uefi,http://{{ "%s:%s" | format(crd_spec.imageServer.ingress.fqdn, crd_spec.imageServer.ingress.port) | unsafe }}/boot-arm64.ipxe
    dhcp-option=tag:ipxe6,tag:arm64-uefi,option6:bootfile-url,http://{{ "%s:%s" | format(crd_spec.imageServer.ingress.fqdn, crd_spec.imageServer.ingress.port) | unsafe }}/boot-arm64.ipxe

    # Boot the ipxe chainloader for bios
    # We do not need to filter for !ipxe because the last entry behaves differently
    dhcp-boot=/undionly.kpxe
