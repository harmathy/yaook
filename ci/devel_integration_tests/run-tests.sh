#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -ex

TEST_DIR=$(dirname ${0})

# Releases to run tests on as an array
readarray -t RELEASES < <("${TEST_DIR}"/os_services.py upgradepath --initial "${INITIAL_RELEASE:?}" --target "${TARGET_RELEASE}")

# Openstack Services to install, also include lower versions so we have a full stack
INSTALL_SERVICES=$("${TEST_DIR}"/os_services.py installables --release "${INITIAL_RELEASE:?}" --include-lower-versions)

function wait_until {
  local max_wait=${max_wait:-$((60 * 20))}
  local delay=${delay:-5}
  local attempt_count=0
  SECONDS=0

  while [[ $SECONDS -le $max_wait ]]; do
    $@ && return 0
    attempt_count=$((attempt_count+1))
    sleep $delay
  done
  echo "Command failed after $attempt_count attempts."
  return 1
}

function retry {
  local max=3;
  local delay=5;
  for i in $(seq $max); do
    "$@" && return 0;
    if [[ $i -lt $max ]]; then
      echo "Command failed. Attempt $i/$max:";
      sleep $delay;
    fi
  done
  echo "The command has failed after $i attempts.";
  return 1;
}

function deploy_openstack_service() {
  service=$1
  release=$2

  if [ -f ci/devel_integration_tests/deploy/${service}_${release}.yaml ]; then
    filename="ci/devel_integration_tests/deploy/${service}_${release}.yaml"
  else
    filename="ci/devel_integration_tests/deploy/${service}.yaml"
  fi
  sed "s/TARGET_RELEASE/\"${release}\"/" "$filename" | retry kubectl apply -n $NAMESPACE -f -
}

function eval_status_lines() {
    updating_allowed="${1:-0}"
    has_any=0
    is_ok=1
    # We do not `break` out of the loop but use flags in order to get the full
    # output to stdout always.
    while IFS= read -r line; do
        has_any=1
        # If there is a False in the line, it’s a problem for sure -> mark as
        # erroneous
        if ( ! test "$updating_allowed" = '1' || ! grep Updating <<<"$line" >/dev/null) && grep False <<<"$line" >/dev/null; then
            is_ok=0
        fi
        # If there is no status at all after the colon, the operator has not
        # yet picked up the resource, which is also not ok.
        if ! grep -P '^[^:]+: \w+' <<<"$line" >/dev/null; then
            is_ok=0
        fi
        # The generation is not yet updated, probably the operator did not yet
        # pick up the change.
        generation=$(echo $line | awk '{print $3}')
        observedGeneration=$(echo $line | awk '{print $4}')
        if [ "$generation" != "$observedGeneration" ]; then
            is_ok=0
        fi
        # Echo the input to stdout for debugging
        echo "$line"
    done
    if [ "$has_any" = 0 ] || [ "$is_ok" = 0 ]; then
        return 1
    fi
    return 0
}

retry helm repo add stable https://charts.helm.sh/stable 2>&1
retry helm repo add rook-release https://charts.rook.io/release 2>&1
retry helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx 2>&1
retry helm repo add jetstack https://charts.jetstack.io 2>&1
retry helm repo add prometheus-community https://prometheus-community.github.io/helm-charts 2>&1
retry helm repo update

retry helm upgrade --install --namespace $NAMESPACE cert-manager\
  --set "installCRDs=true"\
  --version 1.9.1 \
  jetstack/cert-manager
retry helm upgrade --install --force --namespace $NAMESPACE rook-ceph\
  --set "csi.enableRbdDriver=false,csi.enableCephfsDriver=false"\
  --version v1.9.12 \
  rook-release/rook-ceph
retry helm upgrade --install --namespace $NAMESPACE ingress-nginx\
  --set "controller.extraArgs.enable-ssl-passthrough=true"\
  --version 4.2.0 \
  ingress-nginx/ingress-nginx
retry helm upgrade --install --namespace $NAMESPACE prometheus-operator\
  --set "alertmanager.enabled=false,grafana.enabled=false"\
  prometheus-community/kube-prometheus-stack

${TEST_DIR}/label-nodes.sh
# The networknodes must fit to the nodes selected in label-nodes.sh
network_nodes="$(retry kubectl get node -l 'network.yaook.cloud/this-ci-setup-runs-a-gateway-here=true' -o 'jsonpath={ range .items[*] }{ .metadata.name }{ "\n" }{ end }')"
for node in $network_nodes; do
  sed "s/NODENAME/$node/" ${TEST_DIR}/setup-interfaces-job.yml | retry kubectl apply -n $NAMESPACE -f -
done

HELMFLAGS="--namespace $NAMESPACE --set operator.pythonOptimize=false --set operator.image.tag=$YAOOK_BUILD_VERSION --set operator.image.repository=$REGISTRY_URL/$REGISTRY_PROJECT/operator-test --set operator.extraEnv[0].name=YAOOK_OP_INTERFACE_INCONSISTENT_READ,operator.extraEnv[0].value='True'"
# Install Helm Charts as generated previously
retry helm install --namespace $NAMESPACE yaook-crds ./Charts/crds-*.tgz
retry helm install $HELMFLAGS infra-operator ./Charts/infra-operator-*.tgz
retry helm install $HELMFLAGS cds-operator ./Charts/cds-operator-*.tgz
retry helm install $HELMFLAGS keystone-operator ./Charts/keystone-operator-*.tgz
retry helm install $HELMFLAGS keystone-resources-operator ./Charts/keystone-resources-operator-*.tgz
retry helm install $HELMFLAGS barbican-operator ./Charts/barbican-operator-*.tgz
retry helm install $HELMFLAGS ceilometer-operator ./Charts/ceilometer-operator-*.tgz
retry helm install $HELMFLAGS cinder-operator ./Charts/cinder-operator-*.tgz
retry helm install $HELMFLAGS glance-operator ./Charts/glance-operator-*.tgz
retry helm install $HELMFLAGS gnocchi-operator ./Charts/gnocchi-operator-*.tgz
retry helm install $HELMFLAGS horizon-operator ./Charts/horizon-operator-*.tgz
retry helm install $HELMFLAGS neutron-operator ./Charts/neutron-operator-*.tgz
retry helm install $HELMFLAGS neutron-ovn-operator ./Charts/neutron-ovn-operator-*.tgz
retry helm install $HELMFLAGS nova-operator ./Charts/nova-operator-*.tgz
retry helm install $HELMFLAGS nova-compute-operator ./Charts/nova-compute-operator-*.tgz
retry helm install $HELMFLAGS designate-operator ./Charts/designate-operator-*.tgz


for i in deploy/*.yaml; do
    retry kubectl apply -n $NAMESPACE -f "$i"
done

# Wait for Cert-Manager to be ready
echo "Waiting for cert-manager"
while retry kubectl get pods -n "$NAMESPACE" --selector=app.kubernetes.io/instance=cert-manager -o 'jsonpath={ range .items[*] }{ .metadata.name }: { .status.phase } { .status.conditions[*].status } { .status.conditions[*].message }{ "\n" }{ end }' | grep False; do
  sleep 5
done

# Create a CA for the cert-manager
openssl genrsa -out ca.key 2048
openssl req -x509 -new -nodes -key ca.key -sha256 -days 3650 -out ca.crt -subj "/CN=YAOOK-CA"
retry kubectl -n $NAMESPACE create secret tls root-ca --key ca.key --cert ca.crt
retry kubectl -n $NAMESPACE apply -f docs/getting_started/ca-issuer.yaml
cp ca.crt "/etc/ssl/certs/$(openssl x509 -noout -hash -in ca.crt).0"
cat ca.crt >> /usr/local/lib/python3.11/site-packages/certifi/cacert.pem

# Deploy all custom resources
for i in realtime-hack rook-cluster rook-resources yaookdisruptionbudget secondkeystoneendpoint powerdns; do
  retry kubectl apply -n $NAMESPACE -f ci/devel_integration_tests/deploy/${i}.yaml
done


# Deploy Openstack Services
while IFS="," read -r service release
do
  deploy_openstack_service $service $release
done <<< ${INSTALL_SERVICES}

pip3 install openstackclient python-openstackclient
openstack --version
openstack configuration show

# Download cirros Image for Glance Tests
cirros_version="0.5.1"
cirros_image="cirros-${cirros_version}-x86_64-disk.img"
cirros_url="http://download.cirros-cloud.net/${cirros_version}/${cirros_image}"
retry wget -ncv "${cirros_url}"

# init PowerDNS parent zone for Designate testing
wait_until eval "kubectl get powerdnsservice powerdns -n \"$NAMESPACE\" -o 'jsonpath={ .metadata.name }: { .status.phase } { .status.conditions[*].status }{ \"\n\" }' | eval_status_lines"
if [[ $? -eq 0 ]]; then
  kubectl exec -it deploy/powerdns-powerdns -n $NAMESPACE -c powerdns -- bash -c \
    "pdnsutil create-zone yaook.cloud
    pdnsutil set-kind yaook.cloud primary"
fi

set +x

function _test_cinder() {
  openstack volume service list
  id=$(openstack volume create --size 10 testvolume -f value -c id)
  ok=false
  for i in $(seq 1 60); do
    status="$(openstack volume show "$id" -f value -c status)"
    if [[ "$status" = 'available' ]]; then
      ok=true
      break
    fi
    if [[ "$status" = 'error' ]]; then
      echo "volume failed"
      openstack volume show "$id"
      exit 1
    fi
    echo "$status"
    sleep 1
  done
  if [[ "$ok" != 'true' ]]; then
    echo "volume timed out"
    openstack volume show "$id"
    exit 1
  fi
}

function _test_glance() {
  # Glance Image Upload Test
  glance import-info
  openstack image create cirros-file-upload --progress --min-ram 512 --public --disk-format qcow2 --min-disk 1 --file "${cirros_image}"
}

function _test_designate() {
  # update PowerDNS zone file
  POWERDNS_DNS_IP=$(kubectl get svc designate-powerdns-powerdns-webserver -n $NAMESPACE -o json | jq '.spec.clusterIP')
  kubectl exec -it deploy/powerdns-powerdns -n $NAMESPACE -c powerdns -- bash -c \
    "pdnsutil clear-zone yaook.cloud
    pdnsutil add-record yaook.cloud designate NS designate-yaook-dev-ns.yaook.cloud
    pdnsutil add-record yaook.cloud designate-yaook-dev-ns A $POWERDNS_DNS_IP
    pdnsutil replace-rrset yaook.cloud . SOA 'designate.yaook.cloud. mail.domain.com. 1 10800 3600 604800 3600'"
  
  # check if zone creation succeeds
  openstack zone create designate.yaook.cloud. --email mail@yaook.cloud
  echo "Waiting for zone designate.yaook.cloud to become active ..."

  max_wait=$((60 * 80))

  SECONDS=0
  while ! openstack zone show designate.yaook.cloud. -f json | jq '.status' | grep \"ACTIVE\" > /dev/null; do
    sleep 5
    if [[ $SECONDS -gt $max_wait ]]; then
      echo "Time Limit reached waiting Designate zone to become active."
      exit 1
    fi
  done

  openstack zone delete designate.yaook.cloud.
}

function wait_and_test() {
  for cr in \
      keystonedeployments \
      glancedeployments \
      novadeployments \
      barbicandeployments \
      cinderdeployments \
      neutrondeployments \
      horizondeployments \
      keystoneusers \
      keystoneendpoints \
      ceilometerdeployments \
      gnocchideployments \
      novacomputenodes \
      ovnagents \
      powerdnsservices \
      designatedeployments
    do
      # skip if a deployment is not installed
      [[ $(echo $(retry kubectl -n $NAMESPACE get $cr -o json) | jq '.items | length') -lt 1 ]] && continue

      echo
      echo "Waiting for $cr to get ready ..."
      echo
      updating_allowed=0
      if [ "$cr" = 'novacomputenodes' ]; then
          # the nova-compute-operator is in what looks like a loop in the CI
          # cluster because the watches get terminated quicker than it can finish
          # updating all nodes (also because of other watches getting terminated)
          updating_allowed=1
      fi
      max_wait=$((60 * 20))
      if [ "$cr" = 'keystonedeployments' ]; then
          # The MySQLService needs about 3-4 minutes per container during the
          # upgrade from (10.2 -> 11.0). For Keystone we use 3 replicas.
          # With 3 replicas this is 3 * 4 = 12 minutes per step by step update
          # therefore for Keystone we need 6 * 12 minutes = 72 minutes
          # minimum plus some buffer we take 80 minutes.
          max_wait=$((60 * 80))
      fi
      SECONDS=0
      while ! kubectl get "$cr" -n "$NAMESPACE" -o 'jsonpath={ range .items[*] }{ .metadata.name }: { .status.phase } { .metadata.generation } { .status.observedGeneration } { .status.conditions[*].status } { .status.conditions[*].message }{ "\n" }{ end }' | eval_status_lines $updating_allowed; do
          sleep 5
          if [[ $SECONDS -gt $max_wait ]]; then
            echo "Time Limit reached waiting for $cr."
            exit 1
          fi
      done
  done

  echo
  echo "Waiting for Pods to get ready ..."
  echo
  while retry kubectl get pods -n "$NAMESPACE" -o 'jsonpath={ range .items[?(@.metadata.ownerReferences[*].kind!="Job")] }{ .metadata.name }: { .status.phase } { .status.conditions[*].status } { .status.conditions[*].message }{ "\n" }{ end }' | grep False; do
      sleep 5
  done

  ingress_ip="";
  while [ -z $ingress_ip ]; do
    echo "Waiting for public ip of ingress...";
    ingress_ip=$(retry kubectl -n $NAMESPACE get svc ingress-nginx-controller --template="{{range .status.loadBalancer.ingress}}{{.ip}}{{end}}");
    [ -z "$ingress_ip" ] && sleep 10
  done
  echo "Ingress ready at ${ingress_ip}"
  for i in keystone barbican nova glance cinder neutron gnocchi horizon vnc designate; do
    echo "${ingress_ip} ${i}" >> /etc/hosts
    echo "${ingress_ip} ${i}.yaook.cloud" >> /etc/hosts
  done

  eval "$(./tools/download-os-env.sh public -n $NAMESPACE)"

  while ! curl -v "$OS_AUTH_URL"; do echo "waiting for keystone api to come up"; sleep 15; done

  retry kubectl -n $NAMESPACE get pods

  openstack token issue -vvv
  openstack endpoint list -vvv

  endpoints=$(openstack endpoint list -f value --interface public -c "Service Name")

  # Loop through endpoints and run basic tests
  for e in $endpoints
  do
    case $e in
      barbican)
        openstack secret list -vvv
        ;;
      designate)
        openstack zone list -vvv
        _test_designate
        ;; 
      glance)
        openstack image list -vvv
        _test_glance
        ;;
      neutron)
        openstack network list -vvv
        openstack network agent list -f json | jq .
        # check that some agent is up - this is needed to see, if ovn-controller are up
        while ! openstack network agent list -f json | jq -e .[0].State | grep -Eiq "(UP|true)"; do echo "waiting for network agents to come up"; sleep 15; done
        # check to also see the propagation of information from the ovn-controllers to the neutron-api work
        while ! openstack network agent list | grep -Eiq "OVN Controller Gateway agent"; do echo "waiting for ovn gateways to come up"; sleep 15; done
        while ! openstack network agent list | grep -Eiq "OVN Metadata agent"; do echo "waiting for ovn metadata agent to come up"; sleep 15; done
        ;;
      nova)
        openstack server list -vvv
        openstack hypervisor list
        while ! openstack hypervisor list | grep -q up > /dev/null; do echo "waiting for hypervisors to come up"; sleep 15; done
        openstack hypervisor list
        openstack compute service list
        retry curl https://vnc.yaook.cloud -v
        ;;
      cinderv3)
        openstack volume list -vvv
        _test_cinder
        ;;
      gnocchi)
        gnocchi resource list
        ;;
      keystone)
        ;;
      *)
        echo "Test for $e not implemented"
    esac
  done

  # Check if mysqlservice backups are working
  SECONDS=0
  while [[ ! $(kubectl exec -n $NAMESPACE $(kubectl get po -n $NAMESPACE --selector=state.yaook.cloud/component=database -o json | jq -r '.items | .[] | .metadata | select(.name|test(".*keystone.*-db-2.*")) | .name') --container backup-creator -- ls /backup/warm) ]] ; do
    sleep 10
    if [[ $SECONDS -gt 360 ]]; then
      echo 'No mysqlservice backups are present. Please check if backups are still working'
      exit 1
    fi
  done

  # Check if second identity keystoneendpoint is created
  SECONDS=0
  while [[ $(openstack endpoint list --region RegionTwo -f value | wc -l) != 3 ]] ; do
    sleep 10
    if [[ $SECONDS -gt 360 ]]; then
      echo "Keystone endpoint for second region are not created."
      exit 1
    fi
  done


  # Checking if yaookdisruptionbudgets are updated correctly
  echo $(retry kubectl get yaookdisruptionbudgets -n $NAMESPACE test-disruption-budget -o json) | jq -e ".status.nodes[]"
  disruptionbudgettypes=("NovaDeployment-compute_nodes");
  for type in "${disruptionbudgettypes[@]}"; do
    echo "$type"
    echo $(retry kubectl get yaookdisruptionbudgets -n "$NAMESPACE" test-disruption-budget -o json) | jq -e ".status.nodes[] | select(.type == \"$type\") | .configuredInstances > 0"
  done

  [[ $(echo $(retry kubectl -n "$NAMESPACE" get horizondeployment -o json) | jq '.items | length') -gt 0 ]] && retry curl https://horizon.yaook.cloud -v

  # Check if placement cleanup cronjob is working
  SECONDS=0
  while ! kubectl get CronJobs --selector=state.yaook.cloud/component=placement_cleanup -o jsonpath='{.items[0].status.lastSuccessfulTime}' | grep . ; do
    sleep 10
    if [[ $SECONDS -gt 360 ]]; then
      echo 'No Cronjob found or Cronjob is failing. Please check if nova-placement-cleanup Cronjob is still working'
      exit 1
    fi
  done
}

# This will either upgrade the operator or create an initial deployment of the
# lowest supported version if an operator does not support any version <= INITIAL_RELEASE
# Note that this does not use VALID_UPGRADE_TARGETS to check wether an upgrade
# path to the next version exists and that the operator will fail if this is
# not the case
function upgrade_deployments() {
  release=${1}
  echo "$(date --iso-8601=seconds) - NOW RUNNING UPGRADE TO ${release} + TESTS"
  installable_services=$("${TEST_DIR}"/os_services.py installables --release "${release}")

  while IFS="," read -r service release
  do
    deploy_openstack_service $service $release
  done <<< ${installable_services}
}

# Wait for and test initial deployments
wait_and_test

# Now perform and test upgrades of the deployments.
for next_release in "${RELEASES[@]:1}"; do
  upgrade_deployments $next_release
  wait_and_test
done
