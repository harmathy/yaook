import copy
import contextlib
import os
import unittest
import unittest.mock
from unittest.mock import sentinel

import ddt
import jinja2

import kubernetes_asyncio.client as kclient

from yaook.statemachine import context
import yaook.statemachine.api_utils as api_utils
import yaook.statemachine.exceptions as exceptions
import yaook.statemachine.templating as templating

import yaook.statemachine.resources.base as base
import yaook.statemachine.resources.k8s as k8s
import yaook.statemachine.resources.k8s_storage as k8s_storage

from tests import testutils

from .utils import TemplateTestMixin

NAMESPACE = "test-namespace"


@ddt.ddt
class TestReadyInfo(unittest.TestCase):
    @ddt.data(True, False)
    def test_cast_from_bool_leaves_message_None(self, boolean):
        ri = base.ReadyInfo.cast(boolean)
        self.assertIsInstance(ri, base.ReadyInfo)
        self.assertIs(ri.is_ready, boolean)
        self.assertIs(bool(ri), boolean)
        self.assertIsNone(ri.message)

    def test_cast_from_continuable_error_forwards_ready_and_message(self):
        ce = exceptions.ContinueableError(
            sentinel.message,
            sentinel.ready,
        )

        ri = base.ReadyInfo.cast(ce)
        self.assertIsInstance(ri, base.ReadyInfo)
        self.assertEqual(ri.is_ready, sentinel.ready)
        self.assertEqual(ri.message, sentinel.message)

    def test_cast_from_ready_info_passes_through(self):
        ri1 = base.ReadyInfo(is_ready=sentinel.ready, message=sentinel.message)
        ri2 = base.ReadyInfo.cast(ri1)
        self.assertIs(ri1, ri2)


class Testevaluate_metadata(unittest.TestCase):
    def test_returns_string_as_fixed_name(self):
        self.assertDictEqual(
            base.evaluate_metadata(sentinel.ctx, "fixed-name"),
            {"name": "fixed-name"},
        )

    def test_returns_string_from_tuple_with_false_as_fixed_name(self):
        self.assertDictEqual(
            base.evaluate_metadata(sentinel.ctx, ("fixed-name", False)),
            {"name": "fixed-name"},
        )

    def test_returns_string_from_tuple_with_true_as_generated_name(self):
        self.assertDictEqual(
            base.evaluate_metadata(sentinel.ctx, ("prefix", True)),
            {"generateName": "prefix"},
        )

    def test_calls_callable_and_returns_str_as_fixed_name(self):
        cb = unittest.mock.Mock([])
        cb.return_value = "cb-name"

        result = base.evaluate_metadata(sentinel.ctx, cb)

        cb.assert_called_once_with(sentinel.ctx)

        self.assertDictEqual(
            result,
            {
                "name": "cb-name",
            },
        )

    def test_calls_callable_and_returns_str_from_tuple_with_false_as_fixed_name(self):  # noqa
        cb = unittest.mock.Mock([])
        cb.return_value = "cb-name", False

        result = base.evaluate_metadata(sentinel.ctx, cb)

        cb.assert_called_once_with(sentinel.ctx)

        self.assertDictEqual(
            result,
            {
                "name": "cb-name",
            },
        )

    def test_calls_callable_and_returns_str_from_tuple_with_true_as_generate_name(self):  # noqa
        cb = unittest.mock.Mock([])
        cb.return_value = "cb-name", True

        result = base.evaluate_metadata(sentinel.ctx, cb)

        cb.assert_called_once_with(sentinel.ctx)

        self.assertDictEqual(
            result,
            {
                "generateName": "cb-name",
            },
        )

    def test_calls_callable_and_returns_mapping_directly(self):
        cb = unittest.mock.Mock([])
        cb.return_value = {"foo": "bar"}

        result = base.evaluate_metadata(sentinel.ctx, cb)

        cb.assert_called_once_with(sentinel.ctx)

        self.assertDictEqual(
            result,
            {"foo": "bar"},
        )


class Test_scheduling_keys_to_selectors(unittest.TestCase):
    def test_wraps_scheduling_keys_in_LabelSelectors(self):
        self.assertEqual(
            base._scheduling_keys_to_selectors(["foo", "bar"]),
            [
                api_utils.LabelSelector(match_expressions=[
                    api_utils.LabelExpression(
                        key="foo",
                        operator=api_utils.SelectorOperator.EXISTS,
                        values=None,
                    ),
                ]),
                api_utils.LabelSelector(match_expressions=[
                    api_utils.LabelExpression(
                        key="bar",
                        operator=api_utils.SelectorOperator.EXISTS,
                        values=None,
                    ),
                ]),
            ],
        )


class TestResource(unittest.IsolatedAsyncioTestCase):
    class ResourceTest(base.Resource):
        def __init__(self, **kwargs):
            self.set_component_mock = unittest.mock.Mock([])
            self.set_owner_mock = unittest.mock.Mock([])
            super().__init__(**kwargs)

        async def reconcile(self, ctx, dependencies):
            pass

        def _set_component(self, component):
            self.set_component_mock(component)
            super()._set_component(component)

        def _set_owner(self, owner):
            self.set_owner_mock(owner)

        async def delete(self, ctx):
            pass

        async def cleanup_orphans(self, ctx, protect):
            pass

    def setUp(self):
        self.r = self.ResourceTest(component="test-component")

    def test_component(self):
        self.assertEqual(self.r.component, "test-component")

    def test_component_raises_AttributeError_if_unset(self):
        r = self.ResourceTest()
        with self.assertRaises(AttributeError):
            r.component

    def test_component_cannot_be_written_to(self):
        r = self.ResourceTest()
        with self.assertRaises(AttributeError):
            r.component = "foo"

    def test__set_component_not_called_during_init_if_unset(self):
        r = self.ResourceTest()
        r.set_component_mock.assert_not_called()

    def test__set_component_called_during_init(self):
        self.r.set_component_mock.assert_called_once_with("test-component")

    def test__set_component_called_via___set_name__(self):
        r = self.ResourceTest()
        r.__set_name__(unittest.mock.sentinel.owner, "some_name")

        r.set_component_mock.assert_called_once_with("some_name")

    def test_component_via___set_name__(self):
        r = self.ResourceTest()
        r.__set_name__(unittest.mock.sentinel.owner, "some_name")

        self.assertEqual(r.component, "some_name")

    def test___set_name___rejects_being_called_twice(self):
        r = self.ResourceTest()
        r.__set_name__(unittest.mock.sentinel.owner, "some_name")

        with self.assertRaisesRegex(
                ValueError,
                "used on two different parent objects"):
            r.__set_name__(unittest.mock.sentinel.owner, "some_name")

        with self.assertRaisesRegex(
                ValueError,
                "used on two different parent objects"):
            r.__set_name__(unittest.mock.sentinel.owner, "some_other_name")

        with self.assertRaisesRegex(
                ValueError,
                "used on two different parent objects"):
            r.__set_name__(unittest.mock.sentinel.other_owner, "some_name")

        with self.assertRaisesRegex(
                ValueError,
                "used on two different parent objects"):
            r.__set_name__(unittest.mock.sentinel.other_owner,
                           "some_other_name")

    def test__set_component_not_called_via___set_name___if_set_during_init(self):  # NOQA
        self.r.set_component_mock.reset_mock()
        self.r.__set_name__(unittest.mock.sentinel.owner, "other_name")
        self.assertEqual(self.r.component, "test-component")
        self.r.set_component_mock.assert_not_called()

    def test__set_owner_called_via___set_name__(self):
        r = self.ResourceTest()
        r.__set_name__(unittest.mock.sentinel.owner, "some_name")

        r.set_owner_mock.assert_called_once_with(unittest.mock.sentinel.owner)

    async def test_is_ready_defaults_to_true(self):
        self.assertTrue(await self.r.is_ready(unittest.mock.sentinel.ctx))

    async def test_get_used_resources_defaults_to_the_empty_list(self):
        self.assertCountEqual(
            await self.r.get_used_resources(unittest.mock.sentinel.ctx),
            [],
        )

    def test_get_listeners_is_empty_by_default(self):
        self.assertSequenceEqual(self.r.get_listeners(), [])


class TestTemplateMixin(TemplateTestMixin, unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        super().setUp()
        self.tm = base.TemplateMixin(
            templatedir=str(self._template_path),
        )

    async def test_render_template(self):
        value = "value with \"complex\" escaping 'requirements'"

        data = await self.tm._render_template(
            "simple",
            {"template_arg": value}
        )

        self.assertEqual(
            data,
            {
                "test": {
                    "arg": value,
                }
            }
        )

    def test__get_jinja_environment_initialises_environment_with_autoescape(self):  # noqa
        env = unittest.mock.Mock(jinja2.Environment)
        env.filters = {}

        with contextlib.ExitStack() as stack:
            Environment = stack.enter_context(unittest.mock.patch(
                "jinja2.Environment",
            ))
            Environment.return_value = env

            self.assertEqual(
                env,
                self.tm._get_jinja_environment(
                    unittest.mock.sentinel.loader,
                ),
            )

        Environment.assert_called_once_with(
            loader=unittest.mock.sentinel.loader,
            extensions=(
                templating.YAMLAutoescapeExtension,
            ),
            enable_async=True,
        )

    def test__get_jinja_environment_adds_filters_from_method(self):
        env = unittest.mock.Mock(jinja2.Environment)
        env.filters = {}

        with contextlib.ExitStack() as stack:
            Environment = stack.enter_context(unittest.mock.patch(
                "jinja2.Environment",
            ))
            Environment.return_value = env

            add_filters = stack.enter_context(unittest.mock.patch.object(
                self.tm, "_add_filters",
            ))

            self.tm._get_jinja_environment(
                unittest.mock.sentinel.loader,
            )

        add_filters.assert_called_once_with(env)

    def test_provides_flat_merge_filter(self):
        env = self.tm._jinja_env

        self.assertEqual(
            env.filters["flat_merge"],
            templating.flat_merge_dict,
        )

    def test_provides_chmod_filter(self):
        env = self.tm._jinja_env

        self.assertEqual(
            env.filters["chmod"],
            templating.parse_chmod,
        )


class Testextract_password(unittest.IsolatedAsyncioTestCase):
    async def test_extracts_password_from_secret(self):
        ctx = unittest.mock.Mock(["api_client"])
        secret_ref = unittest.mock.Mock(["namespace", "name"])
        secret_state = unittest.mock.Mock(k8s_storage.Secret)
        secret_state.get = unittest.mock.AsyncMock()
        secret_state.get.return_value = secret_ref
        secret = unittest.mock.Mock(["data"])
        secret.data = {}
        secret.data["password"] = unittest.mock.sentinel.b64password
        v1 = unittest.mock.Mock(["read_namespaced_secret"])
        v1.read_namespaced_secret = unittest.mock.AsyncMock()
        v1.read_namespaced_secret.return_value = secret

        with contextlib.ExitStack() as stack:
            CoreV1Api = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api",
            ))
            CoreV1Api.return_value = v1

            b64decode = stack.enter_context(unittest.mock.patch(
                "base64.b64decode",
            ))
            b64decode.return_value.decode.return_value = \
                unittest.mock.sentinel.password

            result = await base.extract_password(
                ctx,
                secret_state,
            )

        CoreV1Api.assert_called_once_with(ctx.api_client)
        secret_state.get.assert_called_once_with(ctx)
        v1.read_namespaced_secret.assert_called_once_with(
            secret_ref.name, secret_ref.namespace,
        )
        b64decode.assert_called_once_with(unittest.mock.sentinel.b64password)
        b64decode.return_value.decode.assert_called_once_with("utf-8")
        self.assertEqual(result, unittest.mock.sentinel.password)

    async def test_extracts_password_from_secret_with_custom_key(self):
        ctx = unittest.mock.Mock(["api_client"])
        secret_ref = unittest.mock.Mock(["namespace", "name"])
        secret_state = unittest.mock.Mock(k8s_storage.Secret)
        secret_state.get = unittest.mock.AsyncMock()
        secret_state.get.return_value = secret_ref
        secret = unittest.mock.Mock(["data"])
        secret.data = {}
        secret.data["custom-key"] = unittest.mock.sentinel.b64password
        secret.data["password"] = unittest.mock.sentinel.wrong_password
        v1 = unittest.mock.Mock(["read_namespaced_secret"])
        v1.read_namespaced_secret = unittest.mock.AsyncMock()
        v1.read_namespaced_secret.return_value = secret

        with contextlib.ExitStack() as stack:
            CoreV1Api = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api",
            ))
            CoreV1Api.return_value = v1

            b64decode = stack.enter_context(unittest.mock.patch(
                "base64.b64decode",
            ))
            b64decode.return_value.decode.return_value = \
                unittest.mock.sentinel.password

            result = await base.extract_password(
                ctx,
                secret_state,
                key="custom-key",
            )

        CoreV1Api.assert_called_once_with(ctx.api_client)
        secret_state.get.assert_called_once_with(ctx)
        v1.read_namespaced_secret.assert_called_once_with(
            secret_ref.name, secret_ref.namespace,
        )
        b64decode.assert_called_once_with(unittest.mock.sentinel.b64password)
        b64decode.return_value.decode.assert_called_once_with("utf-8")
        self.assertEqual(result, unittest.mock.sentinel.password)

    async def test_extracts_password_from_secret_name(self):
        ctx = unittest.mock.Mock(["api_client", "namespace"])
        secret = unittest.mock.Mock(["data"])
        secret.data = {}
        secret.data["password"] = unittest.mock.sentinel.b64password
        v1 = unittest.mock.Mock(["read_namespaced_secret"])
        v1.read_namespaced_secret = unittest.mock.AsyncMock()
        v1.read_namespaced_secret.return_value = secret

        with contextlib.ExitStack() as stack:
            CoreV1Api = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api",
            ))
            CoreV1Api.return_value = v1

            b64decode = stack.enter_context(unittest.mock.patch(
                "base64.b64decode",
            ))
            b64decode.return_value.decode.return_value = \
                unittest.mock.sentinel.password

            result = await base.extract_password(
                ctx,
                "test_secret_name",
            )

        CoreV1Api.assert_called_once_with(ctx.api_client)
        v1.read_namespaced_secret.assert_called_once_with(
            "test_secret_name", ctx.namespace,
        )
        b64decode.assert_called_once_with(unittest.mock.sentinel.b64password)
        b64decode.return_value.decode.assert_called_once_with("utf-8")
        self.assertEqual(result, unittest.mock.sentinel.password)

    async def test_extracts_password_from_secret_name_with_custom_key(self):
        ctx = unittest.mock.Mock(["api_client", "namespace"])
        secret = unittest.mock.Mock(["data"])
        secret.data = {}
        secret.data["custom-key"] = unittest.mock.sentinel.b64password
        secret.data["password"] = unittest.mock.sentinel.wrong_password
        v1 = unittest.mock.Mock(["read_namespaced_secret"])
        v1.read_namespaced_secret = unittest.mock.AsyncMock()
        v1.read_namespaced_secret.return_value = secret

        with contextlib.ExitStack() as stack:
            CoreV1Api = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api",
            ))
            CoreV1Api.return_value = v1

            b64decode = stack.enter_context(unittest.mock.patch(
                "base64.b64decode",
            ))
            b64decode.return_value.decode.return_value = \
                unittest.mock.sentinel.password

            result = await base.extract_password(
                ctx,
                "test_secret_name",
                key="custom-key",
            )

        CoreV1Api.assert_called_once_with(ctx.api_client)
        v1.read_namespaced_secret.assert_called_once_with(
            "test_secret_name", ctx.namespace,
        )
        b64decode.assert_called_once_with(unittest.mock.sentinel.b64password)
        b64decode.return_value.decode.assert_called_once_with("utf-8")
        self.assertEqual(result, unittest.mock.sentinel.password)


class Testget_injected_secrets(unittest.IsolatedAsyncioTestCase):
    async def test_injects_keystone_config_from_secrets(self):
        spec = [
            {
                "secretName": "keystone-database",
                "items": [
                    {
                        "key": "password",
                        "path": "/database/#connection_password",
                    },
                ],
            },
        ]

        ctx = unittest.mock.Mock([])
        ctx.namespace = "foo"
        api_client_mock = testutils.ApiClientMock()
        ctx.api_client = api_client_mock.client

        api_client_mock.put_object(
            "", "v1", "secrets", ctx.namespace, "keystone-database",
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "metadata": {
                    "name": "keystone-database",
                },
                "type": "Opaque",
                "data": {
                    "password": "Zm9vYmFy",
                }
            }
        )

        result = await base.get_injected_secrets(
            ctx,
            spec,
        )

        self.assertEqual(
            result,
            [
                {
                    "database": {
                        "#connection_password": "foobar"
                    }
                }
            ],
        )


class Testmake_db_config_overlay(unittest.IsolatedAsyncioTestCase):
    async def test_discovers_api_uri_components(self):
        ctx = unittest.mock.Mock()

        db_service = unittest.mock.Mock(k8s.KubernetesReference)
        db_service.get.return_value = kclient.V1ObjectReference(
            name="db-service-name",
            namespace="db-service-namespace",
        )

        with contextlib.ExitStack() as stack:
            extract_password = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.base.extract_password",
            ))
            extract_password.return_value = \
                unittest.mock.sentinel.db_password

            result = await base.make_db_config_overlay(
                ctx,
                db_service=db_service,
                db_driver="mysqlclient",
                db_username=sentinel.db_user,
                db_name=sentinel.db_name,
                db_user_password_secret=sentinel.password_secret,
                config_section=sentinel.config_section,
            )

        extract_password.assert_awaited_once_with(
            ctx,
            sentinel.password_secret,
        )

        self.assertEqual(
            result,
            {
                sentinel.config_section: {
                    "#connection_driver": "mysqlclient",
                    "#connection_username": unittest.mock.sentinel.db_user,
                    "#connection_password": unittest.mock.sentinel.db_password,
                    "#connection_host": "db-service-name.db-service-namespace",
                    "#connection_database": unittest.mock.sentinel.db_name,
                },
            },
        )

    async def test_defaults_to_database_config_section(self):
        ctx = unittest.mock.Mock()

        db_service = unittest.mock.Mock(k8s.KubernetesReference)
        db_service.get.return_value = kclient.V1ObjectReference(
            name="db-service-name",
            namespace="db-service-namespace",
        )

        with contextlib.ExitStack() as stack:
            extract_password = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.base.extract_password",
            ))
            extract_password.return_value = \
                unittest.mock.sentinel.db_password

            result = await base.make_db_config_overlay(
                ctx,
                db_driver="mysqlclient",
                db_service=db_service,
                db_username=sentinel.db_user,
                db_name=sentinel.db_name,
                db_user_password_secret=sentinel.password_secret,
            )

        self.assertEqual(
            result,
            {
                "database": {
                    "#connection_driver": "mysqlclient",
                    "#connection_username": unittest.mock.ANY,
                    "#connection_password": unittest.mock.ANY,
                    "#connection_host": unittest.mock.ANY,
                    "#connection_database": unittest.mock.ANY,
                },
            },
        )


class Testmake_mq_config_overlay(unittest.IsolatedAsyncioTestCase):
    async def test_discovers_api_uri_components(self):
        ctx = unittest.mock.Mock()

        mq_service = unittest.mock.Mock(k8s.KubernetesReference)
        mq_service.get.return_value = kclient.V1ObjectReference(
            name="mq-service-name",
            namespace="mq-service-namespace",
        )

        with contextlib.ExitStack() as stack:
            extract_password = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.base.extract_password",
            ))
            extract_password.return_value = \
                unittest.mock.sentinel.db_password

            result = await base.make_mq_config_overlay(
                ctx,
                mq_service=mq_service,
                mq_username=sentinel.db_user,
                mq_user_password_secret=sentinel.password_secret,
            )

        extract_password.assert_awaited_once_with(
            ctx,
            sentinel.password_secret,
        )

        self.assertEqual(
            result,
            {
                "DEFAULT": {
                    "#transport_url_hosts": [
                        "mq-service-name.mq-service-namespace",
                    ],
                    "#transport_url_password":
                        unittest.mock.sentinel.db_password,
                    "#transport_url_username": unittest.mock.sentinel.db_user,
                    "#transport_url_vhost": "",
                },
            },
        )


class Test_write_ca_certificates(unittest.IsolatedAsyncioTestCase):

    @unittest.mock.patch(
        "yaook.statemachine.interfaces.ResourceInterface.read")
    async def test_writes_ca_certs(self, read):
        ctx = unittest.mock.Mock(["api_client", "parent_uid"])
        ctx.parent_uid = "ec85fea3-1fe7-45ab-94db-7ec887f42a16"
        read.return_value = kclient.V1ConfigMap(data={
            "ca-bundle.crt": unittest.mock.sentinel.cabundle
        })

        mo = unittest.mock.mock_open()
        with unittest.mock.patch("builtins.open", mo, create=True):
            ca_config = kclient.V1ObjectReference(
                name="ca-config-name",
                namespace="ca-config-namespace",
            )
            path = await base.write_ca_certificates(ca_config, ctx)
            self.assertEqual(path,
                             "/tmp/ec85fea3-1fe7-45ab-94db-7ec887f42a16.crt")

        mo.assert_called_with(
            "/tmp/ec85fea3-1fe7-45ab-94db-7ec887f42a16.crt", "w")
        handle = mo()
        handle.write.assert_called_once_with(unittest.mock.sentinel.cabundle)


class Testinject_scheduling_keys(unittest.TestCase):
    def test_does_nothing_new_if_no_scheduling_keys_are_given(self):
        ctx = unittest.mock.sentinel.ctx
        ctx.namespace = NAMESPACE
        pod_spec = {}
        pod_spec_bak = copy.deepcopy(pod_spec)

        base.inject_scheduling_keys(ctx, pod_spec, [])

        self.assertEqual(pod_spec, pod_spec_bak)

    def test_adds_scheduling_key_tolerations(self):
        ctx = unittest.mock.sentinel.ctx
        ctx.namespace = NAMESPACE
        pod_spec = {}

        base.inject_scheduling_keys(
            ctx,
            pod_spec,
            {"foo", "bar"},
        )

        self.assertCountEqual(
            pod_spec["tolerations"],
            [
                {
                    "key": "foo",
                    "operator": "Exists",
                },
                {
                    "key": "bar",
                    "operator": "Exists",
                },
            ],
        )

    def test_adds_scheduling_key_tolerations_node_down(self):
        ctx = unittest.mock.sentinel.ctx
        ctx.namespace = NAMESPACE
        pod_spec = {}

        base.inject_scheduling_keys(
            ctx,
            pod_spec,
            {"foo", "bar"},
            tolerate_node_down=True,
        )

        self.assertCountEqual(
            pod_spec["tolerations"],
            [
                {
                    "key": "foo",
                    "operator": "Exists",
                },
                {
                    "key": "bar",
                    "operator": "Exists",
                },
                {
                    "key": "node.kubernetes.io/not-ready",
                    "operator": "Exists",
                    "effect": "NoExecute",
                },
                {
                    "key": "node.kubernetes.io/unreachable",
                    "operator": "Exists",
                    "effect": "NoExecute",
                },
            ],
        )

    def test_keeps_existing_tolerations_in_place(self):
        ctx = unittest.mock.sentinel.ctx
        ctx.namespace = NAMESPACE
        pod_spec = {
            "tolerations": [
                {
                    "key": "other",
                    "operator": "Exists",
                    "effect": "NoSchedule",
                },
            ],
        }

        base.inject_scheduling_keys(
            ctx,
            pod_spec,
            {"foo", "bar"},
        )

        self.assertCountEqual(
            pod_spec["tolerations"],
            [
                {
                    "key": "foo",
                    "operator": "Exists",
                },
                {
                    "key": "bar",
                    "operator": "Exists",
                },
                {
                    "key": "other",
                    "operator": "Exists",
                    "effect": "NoSchedule",
                },
            ],
        )

    def test_keeps_existing_tolerations_in_place_node_down(self):
        ctx = unittest.mock.sentinel.ctx
        ctx.namespace = NAMESPACE
        pod_spec = {
            "tolerations": [
                {
                    "key": "other",
                    "operator": "Exists",
                    "effect": "NoSchedule",
                },
            ],
        }

        base.inject_scheduling_keys(
            ctx,
            pod_spec,
            {"foo", "bar"},
            tolerate_node_down=True,
        )

        self.assertCountEqual(
            pod_spec["tolerations"],
            [
                {
                    "key": "foo",
                    "operator": "Exists",
                },
                {
                    "key": "bar",
                    "operator": "Exists",
                },
                {
                    "key": "other",
                    "operator": "Exists",
                    "effect": "NoSchedule",
                },
                {
                    "key": "node.kubernetes.io/not-ready",
                    "operator": "Exists",
                    "effect": "NoExecute",
                },
                {
                    "key": "node.kubernetes.io/unreachable",
                    "operator": "Exists",
                    "effect": "NoExecute",
                },
            ],
        )

    def test_adds_node_affinity_for_scheduling_keys(self):
        ctx = unittest.mock.sentinel.ctx
        ctx.namespace = NAMESPACE
        pod_spec = {}

        base.inject_scheduling_keys(
            ctx,
            pod_spec,
            {"foo", "bar"},
        )

        self.assertCountEqual(
            pod_spec["affinity"]["nodeAffinity"]
            ["requiredDuringSchedulingIgnoredDuringExecution"]
            ["nodeSelectorTerms"],
            [
                {
                    "matchExpressions": [
                        {
                            "key": "foo",
                            "operator": "Exists",
                            "values": None,
                        },
                    ],
                },
                {
                    "matchExpressions": [
                        {
                            "key": "bar",
                            "operator": "Exists",
                            "values": None,
                        },
                    ],
                },
            ],
        )

    def test_adds_node_affinity_for_scheduling_keys_namespace_aware(self):
        self._env_multiple_namespaces = os.environ.pop(
            "YAOOK_OP_MULTIPLE_NAMESPACES", None
        )
        os.environ["YAOOK_OP_MULTIPLE_NAMESPACES"] = 'True'
        ctx = unittest.mock.sentinel.ctx
        ctx.namespace = NAMESPACE
        pod_spec = {}

        base.inject_scheduling_keys(
            ctx,
            pod_spec,
            {"foo", "bar"},
        )

        self.assertCountEqual(
            pod_spec["affinity"]["nodeAffinity"]
            ["requiredDuringSchedulingIgnoredDuringExecution"]
            ["nodeSelectorTerms"],
            [
                {
                    "matchExpressions": [
                        {
                            "key": "foo",
                            "operator": "Exists",
                            "values": None,
                        },
                        {
                            "key": context.LABEL_NAMESPACE,
                            "operator": "In",
                            "values": [NAMESPACE],
                        },
                    ],
                },
                {
                    "matchExpressions": [
                        {
                            "key": "bar",
                            "operator": "Exists",
                            "values": None,
                        },
                        {
                            "key": context.LABEL_NAMESPACE,
                            "operator": "In",
                            "values": [NAMESPACE],
                        },
                    ],
                },
            ],
        )
        os.environ["YAOOK_OP_MULTIPLE_NAMESPACES"] = "False"

    def test_rejects_existing_node_affinity_requirements(self):
        ctx = unittest.mock.sentinel.ctx
        ctx.namespace = "test-namespace"
        pod_spec = {
            "affinity": {
                "nodeAffinity": {
                    "requiredDuringSchedulingIgnoredDuringExecution": {
                        "nodeSelectorTerms": [
                            unittest.mock.sentinel.anything,
                        ],
                    },
                },
            },
        }
        pod_spec_bak = copy.deepcopy(pod_spec)

        with self.assertRaisesRegex(
                ValueError,
                "conflict: scheduling keys given, but the Pod already has "
                "scheduling requirements set"):
            base.inject_scheduling_keys(
                ctx,
                pod_spec,
                {"foo", "bar"},
            )

        self.assertEqual(pod_spec, pod_spec_bak)

    def test_rejects_existing_node_selector(self):
        ctx = unittest.mock.sentinel.ctx
        ctx.namespace = NAMESPACE
        pod_spec = {
            "nodeSelector": unittest.mock.sentinel.anything,
        }
        pod_spec_bak = copy.deepcopy(pod_spec)

        with self.assertRaisesRegex(
                ValueError,
                "conflict: scheduling keys given, but the Pod already has "
                "scheduling requirements set"):
            base.inject_scheduling_keys(
                ctx,
                pod_spec,
                {"foo", "bar"},
            )

        self.assertEqual(pod_spec, pod_spec_bak)
