#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
import json
import tempfile
from unittest import TestCase
import unittest.mock
import yaml

import ddt
from oslo_policy import policy

from yaook.common.validator import (
    build_policy_configmap,
    validate_policies,
    get_default_policies,
    PolicyInvalid
)

POLICY_FILENAME = "policy.yaml"


@ddt.ddt
class TestPolicies(TestCase):

    # some porlicy rules to be used in the tests as defaults
    _default_policies = {
        "volume:attachment_create": "",
        "volume:attachment_update": "rule:admin_or_owner",
        "admin_or_owner":
            "is_admin:True or (role:admin and is_admin_project:True) or "
            "project_id:%(project_id)s",
        "admin_api": "is_admin:True",
    }
    _default_policies_str = yaml.dump(_default_policies)

    # temporary files to be used as default policy files.
    _empty_file = {
        "file": tempfile.NamedTemporaryFile(prefix="empty"),
        "content": "",
        "parsed_content": {},
    }
    _tmp_dir = {
        "file": tempfile.TemporaryDirectory(prefix="dir"),
        "content": None,
        "parsed_content": None,
    }
    _default_policy_file = {
        "file": tempfile.NamedTemporaryFile(delete=False, prefix="def"),
        "content": _default_policies_str,
        "parsed_content": _default_policies,
    }

    # ddt inputs for the tests
    _valid_policies = [
        json.dumps(
            {"admin_api": "is_admin:True or "
                "(role:admin and is_admin_project:True)"}),
        # json.dumps({}),
        json.dumps(
            {"volume:attachment_create": ""}
        ),
        json.dumps(
            {"volume:attachment_update": "rule:admin_or_owner"}
        ),
        json.dumps(
            {"admin_or_owner": "is_admin:True or "
                "(role:admin and is_admin_project:True) or "
                "(project_id:%(project_id)s and rule:admin_api)"}
        ),
        json.dumps(
            {"admin_or_owner": "is_admin:True and not rule:somerule",
                "somerule": "project_id:%(project_id)s"}
        ),
        json.dumps(
            {"admin_or_owner": "is_admin:True or "
                "(role:admin and is_admin_project:True) or "
                "(project_id:%(project_id)s and "
                "rule:new_used_key) or is_admin:True",
                "new_used_key": "!"}
        ),
    ]
    _invalid_policies = [
        {"invalid_policies": json.dumps({"unused key": ""}),
         "exception": policy.PolicyNotRegistered},
        {"invalid_policies": json.dumps({"": "is_admin:True"}),
         "exception": policy.PolicyNotRegistered},  # empty key
        {"invalid_policies": json.dumps(
            {"volume:attachment_create": "is_admin"}
        ),
         "exception": policy.InvalidDefinitionError},  # bad syntax in value
        {"invalid_policies": json.dumps(
            {"admin_or_owner": "rule:undefined_rule"}
        ),
         "exception": policy.InvalidDefinitionError},
        {"invalid_policies": json.dumps(
            {"admin_or_owner": "project_id:%(project_id)"}
        ),
         "exception": policy.InvalidDefinitionError},  # bad syntax in value
        {"invalid_policies": json.dumps({"circular": "rule:circular"}),
         "exception": policy.InvalidDefinitionError},
        {"invalid_policies": json.dumps(
            {"circular1": "rule:circular2",
             "circular2": "rule:circular1"}
        ),
         "exception": policy.InvalidDefinitionError},
    ]

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        with cls._default_policy_file["file"] as f:
            f.write(cls._default_policies_str.encode("utf-8"))

    @classmethod
    def tearDownClass(cls) -> None:
        if not cls._default_policy_file["file"].closed:
            raise AssertionError("file should have been closed.")
        try:
            os.remove(cls._default_policy_file["file"].name)
        except FileNotFoundError:
            pass
        super().tearDownClass()

    @ddt.data(*_valid_policies)
    def test_validate_policies(self, policies):
        """Test that validate_policies lets valid rules pass"""
        expected_policies = {}
        expected_policies.update(json.loads(policies))

        out = validate_policies(
            policies,
            self._default_policy_file["file"].name,
        )

        self.assertEqual(
            out,
            expected_policies
        )

    @ddt.data(*_valid_policies)
    @unittest.mock.patch("yaook.common.validator.get_default_policies")
    @unittest.mock.patch("oslo_policy.policy.Enforcer")
    @unittest.mock.patch("oslo_policy.policy.Rules")
    @unittest.mock.patch("oslo_config.cfg.CONF")
    def test_validate_policies_uses_oslo_policy(
            self, policies, mock_conf, mock_rules, mock_enforcer,
            mock_get_default_policies):
        """Test that validate_policies uses oslo_policy and
        get_default_policcies"""
        default_policies_dict = self._default_policy_file["parsed_content"]
        expected_default_filename = self._default_policy_file["file"].name

        provided_unvalidated = json.loads(policies)
        expected_unvalidated_dict = dict(default_policies_dict)
        expected_unvalidated_dict.update(json.loads(policies))
        expected_unvalidated_rules = unittest.mock.MagicMock()

        mock_rules.from_dict.return_value = expected_unvalidated_rules

        mock_get_default_policies.return_value = default_policies_dict

        validate_policies(
            json.dumps(provided_unvalidated), expected_default_filename,
        )

        mock_get_default_policies.assert_called_once_with(
            expected_default_filename
        )
        mock_rules.from_dict.assert_called_once_with(expected_unvalidated_dict)
        mock_enforcer.assert_called_once_with(
            mock_conf,
            policy_file=expected_default_filename,
            use_conf=False
        )
        mock_enforcer().set_rules.assert_called_once_with(
            expected_unvalidated_rules
        )
        mock_enforcer().check_rules.assert_called_once_with(
            raise_on_violation=True
        )

    @ddt.unpack
    @ddt.data(*_invalid_policies)
    def test_validate_policies_raises(self, invalid_policies, exception):
        """Test that validate_policies detects invalid rules"""
        valid_default_policy_file_path = self._default_policy_file["file"].name
        expected_error = list(json.loads(invalid_policies).keys())[0]
        if not expected_error:
            expected_error = "''"

        with self.assertRaisesRegex(exception, expected_error):
            validate_policies(
                invalid_policies,
                valid_default_policy_file_path,
            )

    @ddt.unpack
    @ddt.data(
        {"invalid_defaults_filename": "non_existing_file",
         "expected_exception": FileNotFoundError},
        {"invalid_defaults_filename": _empty_file["file"].name,
         "expected_exception": ValueError},
        {"invalid_defaults_filename": _tmp_dir["file"].name,
         "expected_exception": IsADirectoryError},
    )
    def test_validate_policies_invalid_file(
            self, invalid_defaults_filename, expected_exception):
        """Test that incorrect policy file paths are detected"""
        valid_policies = self._default_policy_file["parsed_content"]
        with self.assertRaisesRegex(
                expected_exception, invalid_defaults_filename):
            validate_policies(
                valid_policies,
                invalid_defaults_filename,
            )

    def test_get_default_policies(self):
        """Test returns correctly"""
        expected_parsed = self._default_policy_file["parsed_content"]

        observed_parsed = \
            get_default_policies(self._default_policy_file["file"].name)

        self.assertEqual(expected_parsed, observed_parsed)

    @unittest.mock.patch("oslo_policy.policy.parse_file_contents")
    @unittest.mock.patch("builtins.open")
    def test_get_default_policies_uses_oslo(self, mock_open, mock_parser):
        """Test default file is read and oslo parser is used"""

        expected_file_content = self._default_policy_file["content"]
        expected_defaults_filename = self._default_policy_file["file"].name

        expected_parsed = {"parsed dummy": "content"}
        mock_parser.return_value = expected_parsed

        mock_fp = unittest.mock.MagicMock()
        mock_open().__enter__.return_value = mock_fp
        mock_fp.read.return_value = self._default_policy_file["content"]

        observed_parsed = get_default_policies(expected_defaults_filename)

        self.assertEqual(expected_parsed, observed_parsed)
        mock_parser.assert_called_once_with(expected_file_content)
        mock_open.assert_called_with(expected_defaults_filename)

    @ddt.unpack
    @ddt.data(
        {"invalid_filename": None, "exception": TypeError},
        {"invalid_filename": "", "exception": FileNotFoundError},
        {"invalid_filename": "non_existing_file",
         "exception": FileNotFoundError},
        {"invalid_filename": _empty_file["file"].name,
         "exception": ValueError},
        {"invalid_filename": _tmp_dir["file"].name,
         "exception": IsADirectoryError},
    )
    def test_get_default_policy_file_raises(self, invalid_filename, exception):
        with self.assertRaisesRegex(exception, invalid_filename):
            get_default_policies(invalid_filename)

    @unittest.mock.patch("yaook.common.validator.validate_policies")
    @ddt.data(_default_policy_file, _empty_file)
    def test_build_policy_configmap(
            self, valid_policies_file_dict, mock_validate):
        valid_policies = valid_policies_file_dict["parsed_content"]
        # expected_policy_filename = "non-default policy filename"
        expected_default_filename = self._default_policy_file["file"].name

        expected_policies = {"some valid": "value"}
        mock_validate.return_value = expected_policies

        observed = build_policy_configmap(
            valid_policies,
            defaults_filename=expected_default_filename,
        )

        self.assertEqual(len(observed), 1)
        self.assertEqual(expected_policies, observed)
        mock_validate\
            .assert_called_once_with(
                valid_policies,
                expected_default_filename,
                None,
            )

    @ddt.unpack
    @ddt.data(*_invalid_policies)
    # We ignore the exception here as `build_policy_configmap` should wrap them
    def test_build_policy_configmap_with_invalid_policies(
            self, invalid_policies, exception):
        expected_error_msg = list(json.loads(invalid_policies).keys())[0]
        with self.assertRaisesRegex(PolicyInvalid,
                                    expected_error_msg):
            build_policy_configmap(
                invalid_policies,
                additional_policy=None,
                defaults_filename=self._default_policy_file["file"].name
            )

    @unittest.mock.patch("yaook.common.validator.get_default_policies")
    @unittest.mock.patch("json.loads")
    def test_validate_policies_uses_json(
            self, mock_json, mock_get_default_policies):
        expected_policies = self._default_policy_file["parsed_content"]
        expected_filename = "dummy filename"

        validate_policies(
            expected_policies,
            expected_filename,
        )

        mock_json.assert_called_once_with(expected_policies)
        mock_get_default_policies.assert_called_once_with(expected_filename)

    def test_build_policy_configmap_with_additional_policies(self):
        policies = \
            '{"volume:attachment_update": "example_policy1", ' + \
            '"volume:attachment_create": "role:example_role"}'
        additional_policy = \
            '{"volume:attachment_update": "additional_policy_1", ' + \
            '"admin_or_owner": "additional_policy_2"}'

        observed = build_policy_configmap(
            policies,
            additional_policy=additional_policy,
            defaults_filename=self._default_policy_file["file"].name
        )
        self.assertEqual(len(observed), 3)
        self.assertEqual(
            observed["volume:attachment_update"],
            "(example_policy1) or additional_policy_1")
        self.assertEqual(
            observed["volume:attachment_create"],
            "role:example_role")
        self.assertEqual(
            observed["admin_or_owner"],
            "(is_admin:True or (role:admin and is_admin_project:True) or "
            "project_id:%(project_id)s) or additional_policy_2")
