#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest
import unittest.mock

import yaook.common.config
import yaook.statemachine as sm
import yaook.op.gnocchi as gnocchi


class TestCephLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.cl = gnocchi.CephLayer()

    def test_is_cue_layer(self):
        self.assertIsInstance(self.cl, sm.CueLayer)

    async def test_returns_empty_if_ceph_disabled(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {}

        self.assertDictEqual(await self.cl.get_layer(ctx), {})

    async def test_injects_ceph_config_in_cue(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "backends":
                {"ceph": {
                    "enabled": True,
                    "keyringReference": "gnocchi-client-key",
                    "keyringUsername": "gnocchi",
                }}
        }

        result = await self.cl.get_layer(ctx)

        self.assertIsInstance(
            result["gnocchi"].contents[-1]["storage"],
            yaook.common.config.CueConfigReference
        )


class TestUseCeph(unittest.IsolatedAsyncioTestCase):

    def test_use_ceph_true(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "backends":
                {"ceph": {}}}
        self.assertTrue(gnocchi._use_ceph(ctx))

    def test_use_ceph_false(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "backends": {"s3": {}}}
        self.assertFalse(gnocchi._use_ceph(ctx))


class TestUseS3(unittest.IsolatedAsyncioTestCase):

    def test_use_s3_true(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "backends":
                {"s3": {}}}
        self.assertTrue(gnocchi._use_s3(ctx))

    def test_use_s3_false(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "backends": {"ceph": {}}}
        self.assertFalse(gnocchi._use_s3(ctx))
