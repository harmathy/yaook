#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import base64
import contextlib
from http import HTTPStatus
import re
import ssl
import ddt
import unittest
import unittest.mock
from unittest.mock import sentinel, call

import kubernetes_asyncio.client as kclient

from yaook.common import genpassword
import yaook.statemachine as sm

import yaook.op.infra.resources as infra_resources
from yaook.statemachine import exceptions


class Testtemporary_file(unittest.TestCase):
    def test_makes_data_available_at_given_path(self):
        data = b"foobar2342"
        with infra_resources.temporary_file(data) as f:
            self.assertIsNotNone(f)
            with open(f, "rb") as fin:
                data_in = fin.read()

            self.assertEqual(data_in, data)

    def test_uses_named_temporary_file(self):
        with contextlib.ExitStack() as stack:
            tempfile = unittest.mock.Mock(["write", "flush", "name"])

            ntf = stack.enter_context(
                unittest.mock.patch(
                    "tempfile.NamedTemporaryFile",
                    new=unittest.mock.MagicMock(),
                )
            )
            ntf.return_value.__enter__.return_value = tempfile

            with infra_resources.temporary_file(b"contents") as f:
                # sequencing of the calls is tested by the other test which
                # requires that the data is readable
                tempfile.write.assert_called_once_with(b"contents")
                tempfile.flush.assert_called_once_with()
                self.assertEqual(f, tempfile.name)

            ntf.assert_called_once_with(mode="wb")


class Testconnect_mysql(unittest.TestCase):
    def test_uses_utf8mb4(self):
        with contextlib.ExitStack() as stack:
            connect = stack.enter_context(
                unittest.mock.patch(
                    "pymysql.connect",
                )
            )

            with infra_resources.connect_mysql(
                infra_resources.MySQLConnectionTemplate(
                    hostname=sentinel.host,
                    user=sentinel.user,
                    password=sentinel.password,
                    ca_cert=b"ca certificate",
                )
            ) as conn:
                connect.assert_called_once_with(
                    host=sentinel.host,
                    user=sentinel.user,
                    password=sentinel.password,
                    charset="utf8mb4",
                    ssl=unittest.mock.ANY,
                )
                self.assertEqual(conn, connect())

    def test_provides_ca_using_temporary_file(self):
        with contextlib.ExitStack() as stack:
            connect = stack.enter_context(
                unittest.mock.patch(
                    "pymysql.connect",
                )
            )

            temporary_file = stack.enter_context(
                unittest.mock.patch(
                    "yaook.op.infra.resources.temporary_file",
                    new=unittest.mock.MagicMock(),
                )
            )
            temporary_file.return_value.__enter__.return_value = (
                sentinel.ca_filename
            )

            with infra_resources.connect_mysql(
                infra_resources.MySQLConnectionTemplate(
                    hostname=sentinel.host,
                    user=sentinel.user,
                    password=sentinel.password,
                    ca_cert=sentinel.ca_certificate,
                )
            ) as conn:

                temporary_file.assert_called_once_with(sentinel.ca_certificate)

                connect.assert_called_once_with(
                    host=sentinel.host,
                    user=sentinel.user,
                    password=sentinel.password,
                    charset=unittest.mock.ANY,
                    ssl={
                        "ca": sentinel.ca_filename,
                    },
                )
                self.assertEqual(conn, connect())

    def test_closes_on_exit(self):
        with contextlib.ExitStack() as stack:
            connection = unittest.mock.Mock(["close"])
            connect = stack.enter_context(
                unittest.mock.patch(
                    "pymysql.connect",
                )
            )
            connect.return_value = connection

            with infra_resources.connect_mysql(
                infra_resources.MySQLConnectionTemplate(
                    hostname=sentinel.host,
                    user=sentinel.user,
                    password=sentinel.password,
                    ca_cert=b"ca certificate",
                )
            ) as conn:
                self.assertIs(conn, connection)
                self.assertSequenceEqual(connection.mock_calls, [])

            connection.close.assert_called_once_with()

    def test_closes_on_exception(self):
        class FooException(Exception):
            pass

        with contextlib.ExitStack() as stack:
            connection = unittest.mock.Mock(["close"])
            connect = stack.enter_context(
                unittest.mock.patch(
                    "pymysql.connect",
                )
            )
            connect.return_value = connection

            stack.enter_context(self.assertRaises(FooException))
            with infra_resources.connect_mysql(
                infra_resources.MySQLConnectionTemplate(
                    hostname=sentinel.host,
                    user=sentinel.user,
                    password=sentinel.password,
                    ca_cert=b"ca certificate",
                )
            ) as conn:
                self.assertIs(conn, connection)
                self.assertSequenceEqual(connection.mock_calls, [])
                raise FooException()

        connection.close.assert_called_once_with()


@ddt.ddt
class Testmanipulate_grant_for_comparison(unittest.TestCase):
    @ddt.data(
        # input, expected
        ("", ""),
        (
            "(GRANT ALL PRIVILEGES ON `api`.* TO `api`@`%`)",
            "GRANT ALL PRIVILEGES ON api.* TO api",
        ),
        (
            "(GRANT SELECT, INSERT, UPDATE ON `api`.* TO `api`@`%`)",
            "GRANT SELECT, INSERT, UPDATE ON api.* TO api",
        ),
        ("(garbage@db)", "garbage"),
        ("('garbage@`db`')", "garbage"),
        ("('`garbage`@`db`')", "garbage"),
        (
            "(GRANT ALL PRIVILEGES ON `api`.* TO `api-cfn`@`%`)",
            "GRANT ALL PRIVILEGES ON api.* TO api-cfn",
        ),
    )
    def test_call_manipulate_succeded(self, data):
        in_string, expected_output = data
        output = infra_resources.manipulate_grant_for_comparison(in_string)
        self.assertEqual(expected_output, output)


class TestMySQLUser(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.db_svc = unittest.mock.Mock(sm.KubernetesReference)
        self.db_creds = unittest.mock.Mock(sm.KubernetesReference)
        self.tls_secret = unittest.mock.Mock(sm.KubernetesReference)
        self.optional_secret_ref = unittest.mock.AsyncMock(
            infra_resources.DynamicSecretHandler()
        )
        self.optional_secret_ref._make_body.return_value = {
            "apiVersion": "v1",
            "kind": "Secret",
            "metadata": {"name": "test-secret"},
            "type": "Opaque",
            "data": {"key": "value"},
            "immutable": True,
        }
        self.mu = infra_resources.MySQLUser(
            finalizer="foo.baz/bar",
            database_service=self.db_svc,
            database_credentials=self.db_creds,
            tls_secret=self.tls_secret,
            password_secret=self.optional_secret_ref,
        )

    async def test__get_root_credentials_extracts_credentials(self):
        ctx = unittest.mock.Mock(["api_client", "namespace"])

        root_pw = "foobar2342"
        db_name = "fnord-db"

        db_secret = unittest.mock.Mock([])
        db_secret.data = sm.api_utils.encode_secret_data(
            {
                "mariadb-root-password": root_pw,
                "databaseName": db_name,
            }
        )

        tls_secret = unittest.mock.Mock([])
        tls_secret.data = sm.api_utils.encode_secret_data(
            {
                "ca.crt": "the ca certificate",
            }
        )

        self.db_svc.get.return_value = kclient.V1ObjectReference(
            name="db-service-name",
            namespace="db-service-namespace",
        )
        self.db_creds.get.return_value = kclient.V1ObjectReference(
            name=sentinel.secret_name,
            namespace=sentinel.secret_namespace,
        )
        self.tls_secret.get.return_value = kclient.V1ObjectReference(
            name=sentinel.tls_name,
            namespace=sentinel.tls_namespace,
        )

        def read_impl(namespace, name):
            if (
                namespace == sentinel.secret_namespace and
                name == sentinel.secret_name
            ):
                return db_secret
            if (
                namespace == sentinel.tls_namespace and
                name == sentinel.tls_name
            ):
                return tls_secret
            self.assertFalse((namespace, name))

        with contextlib.ExitStack() as stack:
            secret_interface = unittest.mock.Mock(sm.ResourceInterface)
            stack.enter_context(
                unittest.mock.patch(
                    "yaook.statemachine.secret_interface",
                    new=unittest.mock.Mock(return_value=secret_interface),
                )
            )

            secret_interface.read.side_effect = read_impl

            result = await self.mu._get_root_credentials(ctx)

        self.assertIn(
            unittest.mock.call(
                sentinel.secret_namespace,
                sentinel.secret_name
            ),
            secret_interface.read.await_args_list,
        )

        self.assertIn(
            unittest.mock.call(sentinel.tls_namespace, sentinel.tls_name),
            secret_interface.read.await_args_list,
        )

        connection_info, dbname = result
        self.assertEqual(
            connection_info.hostname,
            "db-service-name.db-service-namespace",
        )
        self.assertEqual(
            connection_info.user,
            "yaook-sys-maint",
        )
        self.assertEqual(connection_info.password, root_pw)
        self.assertEqual(connection_info.ca_cert, b"the ca certificate")
        self.assertEqual(dbname, db_name)

    async def test_update_calls__update_user_with_correct_parameters(self):
        ctx = unittest.mock.Mock()
        ctx.parent_spec = {
            "user": unittest.mock.sentinel.username,
            "databasePrivileges": unittest.mock.sentinel.db_privs,
            "globalPrivileges": unittest.mock.sentinel.global_privs,
            "passwordSecretKeyRef": {
                "name": unittest.mock.sentinel.pwd_secret,
                "key": unittest.mock.sentinel.pwd_key,
            },
        }

        with contextlib.ExitStack() as stack:
            get_root_credentials = stack.enter_context(
                unittest.mock.patch.object(
                    self.mu,
                    "_get_root_credentials",
                )
            )
            get_root_credentials.return_value = (
                unittest.mock.sentinel.conn_info,
                unittest.mock.sentinel.dbname,
            )

            extract_password = stack.enter_context(
                unittest.mock.patch(
                    "yaook.statemachine.extract_password",
                )
            )
            extract_password.return_value = (
                unittest.mock.sentinel.user_password
            )

            loop = unittest.mock.Mock([])
            loop.run_in_executor = unittest.mock.AsyncMock()
            stack.enter_context(
                unittest.mock.patch(
                    "asyncio.get_event_loop",
                    new=unittest.mock.Mock(return_value=loop),
                )
            )

            password_secret_get_current = stack.enter_context(
                unittest.mock.patch.object(
                    self.mu.password_secret,
                    "_get_current",
                )
            )

            mock_secret = unittest.mock.Mock()
            mock_secret.data = {
                unittest.mock.sentinel.pwd_key:
                base64.b64encode(b"mock_password").decode('ascii')
            }
            password_secret_get_current.return_value = mock_secret

            await self.mu.update(ctx, unittest.mock.sentinel.deps)

        get_root_credentials.assert_awaited_once_with(ctx)

    async def test_delete_calls__delete_user_with_correct_parameters(self):
        ctx = unittest.mock.Mock()
        ctx.parent_spec = {
            "user": unittest.mock.sentinel.username,
        }

        with contextlib.ExitStack() as stack:
            get_root_credentials = stack.enter_context(
                unittest.mock.patch.object(
                    self.mu,
                    "_get_root_credentials",
                )
            )
            get_root_credentials.return_value = (
                unittest.mock.sentinel.conn_info,
                unittest.mock.sentinel.dbname,
            )

            loop = unittest.mock.Mock([])
            loop.run_in_executor = unittest.mock.AsyncMock()
            stack.enter_context(
                unittest.mock.patch(
                    "asyncio.get_event_loop",
                    new=unittest.mock.Mock(return_value=loop),
                )
            )

            await self.mu.delete(ctx, unittest.mock.sentinel.deps)

        get_root_credentials.assert_awaited_once_with(ctx)
        loop.run_in_executor.assert_awaited_once_with(
            None,
            self.mu._delete_user,
            unittest.mock.sentinel.conn_info,
            unittest.mock.sentinel.username,
        )

    async def test_delete_returns_successfully_if_db_not_found(self):
        ctx = unittest.mock.Mock()
        ctx.parent_spec = {
            "user": unittest.mock.sentinel.username,
        }

        with contextlib.ExitStack() as stack:
            get_root_credentials = stack.enter_context(
                unittest.mock.patch.object(
                    self.mu,
                    "_get_root_credentials",
                )
            )
            get_root_credentials.side_effect = sm.DependencyNotReady(
                sentinel.component,
                unittest.mock.Mock(),
            )

            loop = unittest.mock.Mock([])
            loop.run_in_executor = unittest.mock.AsyncMock()
            stack.enter_context(
                unittest.mock.patch(
                    "asyncio.get_event_loop",
                    new=unittest.mock.Mock(return_value=loop),
                )
            )

            await self.mu.delete(ctx, unittest.mock.sentinel.deps)

        get_root_credentials.assert_awaited_once_with(ctx)
        loop.run_in_executor.assert_not_called()

    async def test_delete_reraises_other_api_errors(self):
        ctx = unittest.mock.Mock()
        ctx.parent_spec = {
            "user": unittest.mock.sentinel.username,
        }

        with contextlib.ExitStack() as stack:
            get_root_credentials = stack.enter_context(
                unittest.mock.patch.object(
                    self.mu,
                    "_get_root_credentials",
                )
            )
            get_root_credentials.side_effect = kclient.exceptions.ApiException(
                status=401
            )

            loop = unittest.mock.Mock([])
            loop.run_in_executor = unittest.mock.AsyncMock()
            stack.enter_context(
                unittest.mock.patch(
                    "asyncio.get_event_loop",
                    new=unittest.mock.Mock(return_value=loop),
                )
            )

            with self.assertRaises(kclient.exceptions.ApiException):
                await self.mu.delete(ctx, unittest.mock.sentinel.deps)

        get_root_credentials.assert_awaited_once_with(ctx)
        loop.run_in_executor.assert_not_called()


class Testdeclare_amqp_user(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self._response = unittest.mock.Mock()
        self._session = unittest.mock.Mock([])

    @contextlib.asynccontextmanager
    async def _response_cm(self, *args, **kwargs):
        yield self._response

    async def test_composes_put_request_with_payload(self):
        self._session.put = unittest.mock.Mock([])
        self._session.put.side_effect = self._response_cm

        await infra_resources.declare_amqp_user(
            self._session,
            "http://base-url",
            user="some-user",
            password=unittest.mock.sentinel.password,
            tags=unittest.mock.sentinel.tags,
        )

        self._session.put.assert_called_once_with(
            "http://base-url/users/some-user",
            json={
                "password": unittest.mock.sentinel.password,
                "tags": unittest.mock.sentinel.tags,
            },
        )

        self._response.raise_for_status.assert_called_once_with()

    async def test_quotes_user_name_appropriately(self):
        self._session.put = unittest.mock.Mock([])
        self._session.put.side_effect = self._response_cm

        with contextlib.ExitStack() as stack:
            quote = stack.enter_context(
                unittest.mock.patch(
                    "urllib.parse.quote",
                )
            )
            quote.return_value = "quoted_username"

            await infra_resources.declare_amqp_user(
                self._session,
                "http://base-url",
                user=unittest.mock.sentinel.user,
                password=unittest.mock.sentinel.password,
                tags=unittest.mock.sentinel.tags,
            )

        quote.assert_called_once_with(
            unittest.mock.sentinel.user,
            safe="",
        )

        self._session.put.assert_called_once_with(
            "http://base-url/users/quoted_username",
            json=unittest.mock.ANY,
        )


class Testdeclare_amqp_user_permissions(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self._response = unittest.mock.Mock()
        self._session = unittest.mock.Mock([])

    @contextlib.asynccontextmanager
    async def _response_cm(self, *args, **kwargs):
        yield self._response

    async def test_composes_put_request_with_payload(self):
        self._session.put = unittest.mock.Mock([])
        self._session.put.side_effect = self._response_cm

        await infra_resources.declare_amqp_user_permissions(
            self._session,
            "http://base-url",
            vhost="some-vhost",
            user="some-user",
            configure=unittest.mock.sentinel.configure,
            read=unittest.mock.sentinel.read,
            write=unittest.mock.sentinel.write,
        )

        self._session.put.assert_called_once_with(
            "http://base-url/permissions/some-vhost/some-user",
            json={
                "configure": unittest.mock.sentinel.configure,
                "read": unittest.mock.sentinel.read,
                "write": unittest.mock.sentinel.write,
            },
        )

        self._response.raise_for_status.assert_called_once_with()

    async def test_quotes_url_arguments_appropriately(self):
        self._session.put = unittest.mock.Mock([])
        self._session.put.side_effect = self._response_cm

        await infra_resources.declare_amqp_user_permissions(
            self._session,
            "http://base-url",
            vhost="/",
            user="some/nasty/user",
            configure=unittest.mock.sentinel.configure,
            read=unittest.mock.sentinel.read,
            write=unittest.mock.sentinel.write,
        )

        self._session.put.assert_called_once_with(
            "http://base-url/permissions/%2F/some%2Fnasty%2Fuser",
            json=unittest.mock.ANY,
        )


class Testdelete_amqp_user(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self._response = unittest.mock.Mock()
        self._session = unittest.mock.Mock([])

    @contextlib.asynccontextmanager
    async def _response_cm(self, *args, **kwargs):
        yield self._response

    async def test_composes_put_request_with_payload(self):
        self._session.delete = unittest.mock.Mock([])
        self._session.delete.side_effect = self._response_cm

        await infra_resources.delete_amqp_user(
            self._session,
            "http://base-url",
            user="some-user",
        )

        self._session.delete.assert_called_once_with(
            "http://base-url/users/some-user",
        )

        self._response.raise_for_status.assert_called_once_with()

    async def test_quotes_user_name_appropriately(self):
        self._session.delete = unittest.mock.Mock([])
        self._session.delete.side_effect = self._response_cm

        with contextlib.ExitStack() as stack:
            quote = stack.enter_context(
                unittest.mock.patch(
                    "urllib.parse.quote",
                )
            )
            quote.return_value = "quoted_username"

            await infra_resources.delete_amqp_user(
                self._session,
                "http://base-url",
                user=unittest.mock.sentinel.user,
            )

        quote.assert_called_once_with(
            unittest.mock.sentinel.user,
            safe="",
        )

        self._session.delete.assert_called_once_with(
            "http://base-url/users/quoted_username",
        )


class TestDynamicSecretHandler(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.ctx = unittest.mock.Mock(spec=sm.Context)
        self.ctx.api_client = unittest.mock.Mock()
        self.ctx.namespace = unittest.mock.Mock()
        self.ctx.parent = unittest.mock.Mock()
        self.ctx.parent_intf = unittest.mock.Mock()
        self.ctx.instance = unittest.mock.Mock()
        self.ctx.instance_data = unittest.mock.Mock()
        self.ctx.api_client = unittest.mock.Mock()
        self.ctx.logger = unittest.mock.Mock()
        self.ctx.field_manager = unittest.mock.Mock()
        self.ctx.parent_spec = {
            "passwordSecretKeyRef": {"key": "test-key", "name": "test-name"}
        }
        self.dependencies = unittest.mock.Mock(spec=sm.DependencyMap)
        self.handler = infra_resources.DynamicSecretHandler()

    async def test_generate_password(self):
        with unittest.mock.patch(
            "yaook.common.genpassword", return_value=genpassword(length=64)
        ):
            result = genpassword(length=64)[0]
            self.assertTrue(
                re.search(r"^[A-Za-z0-9]{64,}==$", result)
            )

    async def test_make_body_returns_existing_secret(self):
        current_secret = kclient.V1Secret()
        self.current_password: str = (
            "gCaEtCbIpdsRrfnH1hfyKXeKBJXfFLspQ"
            "Mg4Kf6bahJCYFsvSs7mm3jxLhvAAxnl"
        )
        current_secret.data = {
            "test-key": self.current_password
        }
        self.handler._get_current = unittest.mock.AsyncMock(
            return_value=current_secret
        )

        result = await self.handler._make_body(self.ctx, self.dependencies)
        self.assertEqual(
            result["data"]["test-key"],
            self.current_password,
        )

    async def test_make_body_handles_resource_not_present(self):
        self.handler._get_current = unittest.mock.AsyncMock(
            side_effect=exceptions.ResourceNotPresent(
                component=self,
                context=self.ctx
            )
        )

        with unittest.mock.patch(
            "yaook.common.genpassword", return_value=["generated-password"]
        ):
            result = await self.handler._make_body(self.ctx, self.dependencies)
            self.assertTrue(
                len(result["data"]["test-key"]) != 64
                or re.search(r"[A-Za-z]", result["data"]["test-key"])
                or not re.search(r"\d", result["data"]["test-key"])
            )

    async def test_get_current_retrieves_secret(self):
        api_item = unittest.mock.Mock(spec=kclient.V1Secret)
        self.handler.get_resource_interface = unittest.mock.Mock(
            return_value=unittest.mock.AsyncMock(
                read=unittest.mock.AsyncMock(return_value=api_item)
            )
        )
        result = await self.handler._get_current(self.ctx)
        self.assertEqual(result, api_item)

    async def test_get_current_handles_api_exception(self):
        type(self.handler).component = unittest.mock.PropertyMock()

        api_exception = kclient.ApiException(status=HTTPStatus.NOT_FOUND)
        self.handler.get_resource_interface = unittest.mock.Mock(
            return_value=unittest.mock.AsyncMock(
                read=unittest.mock.AsyncMock(side_effect=api_exception)
            )
        )

        with self.assertRaises(exceptions.ResourceNotPresent):
            await self.handler._get_current(self.ctx)

    def test_needs_update_always_false(self):
        self.assertFalse(self.handler._needs_update(None, None))


class TestAMQPUser(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self._api_session = unittest.mock.sentinel.session

        @contextlib.asynccontextmanager
        async def api_session_mock(rootuser, rootpassword, cacert):
            yield self._api_session

        self._api_session_mock = unittest.mock.Mock([])
        self._api_session_mock.side_effect = api_session_mock

        self.amqp_svc = unittest.mock.Mock(sm.KubernetesReference)
        self.amqp_creds = unittest.mock.Mock(sm.KubernetesReference)
        self.amqp_frontend_cert = unittest.mock.Mock(sm.KubernetesReference)
        self.optional_secret_ref = unittest.mock.AsyncMock(
            infra_resources.DynamicSecretHandler()
        )

        self.ctx = unittest.mock.Mock(spec=sm.Context)
        self.ctx.api_client = unittest.mock.Mock()
        self.ctx.namespace = unittest.mock.Mock()
        self.ctx.parent = unittest.mock.Mock()
        self.ctx.parent_intf = unittest.mock.Mock()
        self.ctx.instance = unittest.mock.Mock()
        self.ctx.instance_data = unittest.mock.Mock()
        self.ctx.api_client = unittest.mock.Mock()
        self.ctx.logger = unittest.mock.Mock()
        self.ctx.field_manager = unittest.mock.Mock()
        self.ctx.parent_spec = {
            "passwordSecretKeyRef": {"key": "test-key", "name": "test-name"}
        }

        self.au = infra_resources.AMQPUser(
            finalizer="foo.bar/baz",
            amqp_credentials=self.amqp_creds,
            amqp_service=self.amqp_svc,
            amqp_frontend_certificate=self.amqp_frontend_cert,
            password_secret=self.optional_secret_ref,
        )

    async def test__get_root_credentials_extracts_connection_info(self):
        self.ctx = unittest.mock.Mock(["api_client", "namespace"])

        self.amqp_svc.get.return_value = kclient.V1ObjectReference(
            name="mq-svc-name",
            namespace="mq-svc-namespace",
        )
        self.amqp_creds.get.return_value = kclient.V1ObjectReference(
            name=sentinel.secret_name,
            namespace=sentinel.secret_namespace,
        )
        self.amqp_frontend_cert.get.return_value = kclient.V1ObjectReference(
            name=sentinel.tls_name,
            namespace=sentinel.tls_namespace,
        )

        root_pw = "foobar2342-foo-mq-password"

        amqp_secret = unittest.mock.Mock([])
        amqp_secret.data = sm.api_utils.encode_secret_data(
            {
                "rabbitmq-password": root_pw,
            }
        )

        tls_secret = unittest.mock.Mock([])
        tls_secret.data = sm.api_utils.encode_secret_data(
            {
                "ca.crt": "<ca certificate data>",
            }
        )

        def read_impl(namespace, name):
            if (
                namespace == sentinel.secret_namespace and
                name == sentinel.secret_name
            ):

                return amqp_secret
            if (
                namespace == sentinel.tls_namespace and
                name == sentinel.tls_name
            ):

                return tls_secret
            self.assertFalse((namespace, name))

        with contextlib.ExitStack() as stack:
            secret_interface = unittest.mock.Mock(sm.ResourceInterface)
            stack.enter_context(
                unittest.mock.patch(
                    "yaook.statemachine.secret_interface",
                    new=unittest.mock.Mock(return_value=secret_interface),
                )
            )
            secret_interface.read.side_effect = read_impl

            result = await self.au._get_root_credentials(self.ctx)

        self.assertCountEqual(
            secret_interface.read.await_args_list,
            [
                unittest.mock.call(
                    sentinel.secret_namespace,
                    sentinel.secret_name),
                unittest.mock.call(
                    sentinel.tls_namespace,
                    sentinel.tls_name),
            ],
        )

        api_url, rootuser, rootpassword, cacert = result
        self.assertEqual(
            api_url,
            "https://mq-svc-name.mq-svc-namespace:15671/api",
        )
        self.assertEqual(
            rootuser,
            "yaook-sys-maint",
        )
        self.assertEqual(rootpassword, root_pw)
        self.assertEqual(cacert, b"<ca certificate data>")

    async def test__api_session_creates_custom_ssl_context_with_cacert(self):
        @contextlib.asynccontextmanager
        async def acm(result):
            yield result

        @contextlib.contextmanager
        def cm(result):
            yield result

        with contextlib.ExitStack() as stack:
            ClientSession = stack.enter_context(
                unittest.mock.patch(
                    "aiohttp.ClientSession",
                )
            )
            ClientSession.return_value = acm(sentinel.session)

            temporary_file = stack.enter_context(
                unittest.mock.patch(
                    "yaook.op.infra.resources.temporary_file",
                )
            )
            temporary_file.return_value = cm(sentinel.cafilename)

            create_default_context = stack.enter_context(
                unittest.mock.patch(
                    "ssl.create_default_context",
                )
            )
            create_default_context.return_value = sentinel.ssl_context

            TCPConnector = stack.enter_context(
                unittest.mock.patch(
                    "aiohttp.TCPConnector",
                )
            )
            TCPConnector.return_value = sentinel.connector

            BasicAuth = stack.enter_context(
                unittest.mock.patch(
                    "aiohttp.BasicAuth",
                )
            )
            BasicAuth.return_value = sentinel.auth

            async with self.au._api_session(
                unittest.mock.sentinel.rootuser,
                unittest.mock.sentinel.rootpassword,
                unittest.mock.sentinel.cacert,
            ) as session:
                self.assertEqual(session, unittest.mock.sentinel.session)

        temporary_file.assert_called_once_with(
            sentinel.cacert,
        )

        create_default_context.assert_called_once_with(
            cafile=sentinel.cafilename,
        )

        TCPConnector.assert_called_once_with(
            ssl=sentinel.ssl_context,
        )

        ClientSession.assert_called_once_with(
            auth=unittest.mock.ANY,
            connector=sentinel.connector,
        )

    async def test__api_session_preloads_credentials(self):
        @contextlib.asynccontextmanager
        async def cm():
            yield unittest.mock.sentinel.session

        ctx = unittest.mock.Mock(ssl.SSLContext)

        with contextlib.ExitStack() as stack:
            ClientSession = stack.enter_context(
                unittest.mock.patch(
                    "aiohttp.ClientSession",
                )
            )
            ClientSession.return_value = cm()

            create_default_context = stack.enter_context(
                unittest.mock.patch(
                    "ssl.create_default_context",
                )
            )
            create_default_context.return_value = ctx

            BasicAuth = stack.enter_context(
                unittest.mock.patch(
                    "aiohttp.BasicAuth",
                )
            )

            async with self.au._api_session(
                unittest.mock.sentinel.rootuser,
                unittest.mock.sentinel.rootpassword,
                b"dummy",
            ) as session:
                self.assertEqual(session, unittest.mock.sentinel.session)

        BasicAuth.assert_called_once_with(
            login=unittest.mock.sentinel.rootuser,
            password=unittest.mock.sentinel.rootpassword,
            encoding="utf-8",
        )
        ClientSession.assert_called_once_with(
            auth=BasicAuth(), connector=unittest.mock.ANY
        )

    async def test_delete_calls_delete_api(self):
        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {
            "user": unittest.mock.sentinel.user,
        }

        with contextlib.ExitStack() as stack:
            _get_root_credentials = stack.enter_context(
                unittest.mock.patch.object(
                    self.au,
                    "_get_root_credentials",
                )
            )
            _get_root_credentials.return_value = (
                unittest.mock.sentinel.api_url,
                unittest.mock.sentinel.rootuser,
                unittest.mock.sentinel.rootpassword,
                unittest.mock.sentinel.cacert,
            )

            _api_session = stack.enter_context(
                unittest.mock.patch.object(
                    self.au,
                    "_api_session",
                    new=self._api_session_mock,
                )
            )

            delete_amqp_user = stack.enter_context(
                unittest.mock.patch(
                    "yaook.op.infra.resources.delete_amqp_user",
                )
            )

            await self.au.delete(
                ctx,
                unittest.mock.sentinel.deps,
            )

        _get_root_credentials.assert_awaited_once_with(ctx)
        _api_session.assert_called_once_with(
            unittest.mock.sentinel.rootuser,
            unittest.mock.sentinel.rootpassword,
            unittest.mock.sentinel.cacert,
        )
        delete_amqp_user.assert_awaited_once_with(
            self._api_session,
            unittest.mock.sentinel.api_url,
            unittest.mock.sentinel.user,
        )

    async def test_delete_returns_successfully_if_secret_not_ready(self):
        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {
            "user": unittest.mock.sentinel.user,
        }

        with contextlib.ExitStack() as stack:
            _get_root_credentials = stack.enter_context(
                unittest.mock.patch.object(
                    self.au,
                    "_get_root_credentials",
                )
            )
            _get_root_credentials.side_effect = sm.DependencyNotReady(
                sentinel.component,
                unittest.mock.Mock(),
            )

            _api_session = stack.enter_context(
                unittest.mock.patch.object(
                    self.au,
                    "_api_session",
                    new=self._api_session_mock,
                )
            )

            delete_amqp_user = stack.enter_context(
                unittest.mock.patch(
                    "yaook.op.infra.resources.delete_amqp_user",
                )
            )

            await self.au.delete(
                ctx,
                unittest.mock.sentinel.deps,
            )

        _get_root_credentials.assert_awaited_once_with(ctx)
        _api_session.assert_not_called()
        delete_amqp_user.assert_not_called()

    async def test_delete_reraises_other_api_errors(self):
        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {
            "user": unittest.mock.sentinel.user,
        }

        with contextlib.ExitStack() as stack:
            _get_root_credentials = stack.enter_context(
                unittest.mock.patch.object(
                    self.au,
                    "_get_root_credentials",
                )
            )
            _get_root_credentials.side_effect = (
                kclient.exceptions.ApiException(500)
            )

            _api_session = stack.enter_context(
                unittest.mock.patch.object(
                    self.au,
                    "_api_session",
                    new=self._api_session_mock,
                )
            )

            delete_amqp_user = stack.enter_context(
                unittest.mock.patch(
                    "yaook.op.infra.resources.delete_amqp_user",
                )
            )

            with self.assertRaises(kclient.exceptions.ApiException):
                await self.au.delete(
                    ctx,
                    unittest.mock.sentinel.deps,
                )

        _get_root_credentials.assert_awaited_once_with(ctx)
        _api_session.assert_not_called()
        delete_amqp_user.assert_not_called()

    async def test_upgrade_calls_amqp_api(self):
        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {
            "user": unittest.mock.sentinel.user,
            "passwordSecretKeyRef": {
                "name": unittest.mock.sentinel.secret_name,
                "key": unittest.mock.sentinel.secret_key,
            },
        }

        with contextlib.ExitStack() as stack:
            _get_root_credentials = stack.enter_context(
                unittest.mock.patch.object(
                    self.au,
                    "_get_root_credentials",
                )
            )
            _get_root_credentials.return_value = (
                unittest.mock.sentinel.api_url,
                unittest.mock.sentinel.rootuser,
                unittest.mock.sentinel.rootpassword,
                unittest.mock.sentinel.cacert,
            )

            _api_session = stack.enter_context(
                unittest.mock.patch.object(
                    self.au,
                    "_api_session",
                    new=self._api_session_mock,
                )
            )

            extract_password = stack.enter_context(
                unittest.mock.patch(
                    "yaook.statemachine.extract_password",
                )
            )
            extract_password.return_value = unittest.mock.sentinel.password

            # use a wrapper mock to assert sequence
            api = unittest.mock.Mock([])
            api.declare_amqp_user = unittest.mock.AsyncMock([])
            api.declare_amqp_user_permissions = unittest.mock.AsyncMock([])

            stack.enter_context(
                unittest.mock.patch(
                    "yaook.op.infra.resources.declare_amqp_user",
                    new=api.declare_amqp_user,
                )
            )

            stack.enter_context(
                unittest.mock.patch(
                    "yaook.op.infra.resources.declare_amqp_user_permissions",
                    new=api.declare_amqp_user_permissions,
                )
            )

            password_secret_get_current = stack.enter_context(
                unittest.mock.patch.object(
                    self.au.password_secret,
                    "_get_current",
                )
            )

            mock_secret = unittest.mock.Mock()
            mock_secret.data = {
                unittest.mock.sentinel.secret_key:
                base64.b64encode(b"mock_password").decode('ascii')
            }
            password_secret_get_current.return_value = mock_secret

            await self.au.update(
                ctx,
                unittest.mock.sentinel.deps,
            )

            password_secret: list[kclient.V1Secret] = (
                await self.au.password_secret._get_current(ctx=ctx)
            )
            key: str = ctx.parent_spec["passwordSecretKeyRef"].get("key")
            current_password: str = base64.b64decode(
                password_secret.data[key]
            ).decode('ascii')

        _get_root_credentials.assert_awaited_once_with(ctx)
        _api_session.assert_called_once_with(
            unittest.mock.sentinel.rootuser,
            unittest.mock.sentinel.rootpassword,
            unittest.mock.sentinel.cacert,
        )

        self.assertSequenceEqual(
            api.mock_calls,
            [
                unittest.mock.call.declare_amqp_user(
                    self._api_session,
                    unittest.mock.sentinel.api_url,
                    user=unittest.mock.sentinel.user,
                    password=current_password,
                    tags="",
                ),
                unittest.mock.call.declare_amqp_user_permissions(
                    self._api_session,
                    unittest.mock.sentinel.api_url,
                    vhost="/",
                    user=unittest.mock.sentinel.user,
                    configure=".*",
                    read=".*",
                    write=".*",
                ),
            ],
        )
        api.declare_amqp_user.assert_awaited()
        api.declare_amqp_user_permissions.assert_awaited()


class TestHaproxyTemplateConfigMap(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        super().setUp()
        self.dps = unittest.mock.Mock(["get_all"])
        self.dps.get_all = unittest.mock.AsyncMock()
        self.dps.get_all.return_value = {
            "service1": kclient.V1ObjectReference(
                name="service1-resource", namespace="namespace"
            ),
            "service2": kclient.V1ObjectReference(
                name="service2-resource", namespace="namespace"
            ),
        }
        self.htcm = infra_resources.HaproxyTemplateConfigMap(
            metadata="metadata",
            template_map={"template": "template"},
            database_pod_services=self.dps,
            component="htcm",
        )

    def test_is_configmap(self):
        self.assertIsInstance(self.htcm, sm.ConfigMap)

    @unittest.mock.patch(
        "yaook.statemachine.resources.DefaultTemplateParamsMixin."
        "_get_template_parameters"
    )
    async def test__get_template_parameters(
        self,
        super__get_template_parameters
    ):

        super__get_template_parameters.return_value = {}
        ctx = unittest.mock.Mock()

        params = await self.htcm._get_template_parameters(ctx, {})

        self.assertEqual(
            {"service1": "service1-resource", "service2": "service2-resource"},
            params["services"],
        )

        super__get_template_parameters.assert_called_once_with(ctx, {})
        self.dps.get_all.assert_called_once_with(ctx)


class TestAMQPPolicies(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self._api_session = unittest.mock.AsyncMock(["get", "put", "delete"])
        self.ctx = unittest.mock.Mock(["api_client"])

        @contextlib.asynccontextmanager
        async def api_session_mock(rootuser, rootpassword, cacert):
            yield self._api_session

        self._api_session_mock = unittest.mock.Mock([])
        self._api_session_mock.side_effect = api_session_mock

        self._api_session_get = (
            unittest.mock.AsyncMock(["raise_for_status", "json"])
        )
        self._api_session_get.json = unittest.mock.AsyncMock()
        self._api_session_get.json.return_value = []

        @contextlib.asynccontextmanager
        async def api_session_get_mock(url):
            yield self._api_session_get

        self._api_session_get_mock = unittest.mock.Mock([])
        self._api_session_get_mock.side_effect = api_session_get_mock

        self._api_session_put = unittest.mock.AsyncMock(["raise_for_status"])

        @contextlib.asynccontextmanager
        async def api_session_put_mock(url, json):
            yield self._api_session_put

        self._api_session_put_mock = unittest.mock.Mock([])
        self._api_session_put_mock.side_effect = api_session_put_mock

        self._api_session_delete = (
            unittest.mock.AsyncMock(["raise_for_status"])
        )
        self._api_session_delete.json = unittest.mock.AsyncMock()
        self._api_session_delete.json.return_value = []

        @contextlib.asynccontextmanager
        async def api_session_delete_mock(url):
            yield self._api_session_delete

        self._api_session_delete_mock = unittest.mock.Mock([])
        self._api_session_delete_mock.side_effect = api_session_delete_mock

        self.amqp_svc = unittest.mock.Mock(sm.KubernetesReference)
        self.amqp_creds = unittest.mock.Mock(sm.KubernetesReference)
        self.amqp_frontend_cert = unittest.mock.Mock(sm.KubernetesReference)

        self.ap = infra_resources.AMQPPolicies(
            finalizer="foo.bar/baz",
            amqp_credentials=self.amqp_creds,
            amqp_service=self.amqp_svc,
            amqp_frontend_certificate=self.amqp_frontend_cert,
        )

    async def test__create_ampq_policy_default(self):
        self.ctx.parent_spec = {}

        with contextlib.ExitStack() as stack:
            _get_root_credentials = stack.enter_context(
                unittest.mock.patch.object(
                    self.ap,
                    "_get_root_credentials",
                )
            )
            _get_root_credentials.return_value = (
                unittest.mock.sentinel.api_url,
                unittest.mock.sentinel.rootuser,
                unittest.mock.sentinel.rootpassword,
                unittest.mock.sentinel.cacert,
            )

            _api_session = stack.enter_context(
                unittest.mock.patch.object(
                    self.ap,
                    "_api_session",
                    new=self._api_session_mock,
                )
            )
            self._api_session.get.side_effect = self._api_session_get_mock
            self._api_session.put.side_effect = self._api_session_put_mock

            await self.ap.update(self.ctx, {})
        test_call = call(
            unittest.mock.sentinel.rootuser,
            unittest.mock.sentinel.rootpassword,
            unittest.mock.sentinel.cacert,
        )
        _api_session.assert_has_calls([test_call])
        self.assertEqual(_api_session.call_count, 1)

        self._api_session.get.assert_called_once_with(
            str(unittest.mock.sentinel.api_url) + "/policies/"
        )
        self._api_session.put.assert_called_once_with(
            f"{unittest.mock.sentinel.api_url}/policies/%2F/openstack-default",
            json={
                "pattern": "^(?!(amq\\.)|(.*_fanout_)|(reply_)).*",
                "definition": {
                    "expires": 3600000,
                    "ha-mode": "all",
                    "ha-promote-on-failure": "always",
                    "ha-promote-on-shutdown": "always",
                    "ha-sync-mode": "manual",
                    "message-ttl": 600000,
                    "queue-master-locator": "client-local",
                },
                "priority": 1,
                "apply-to": "all",
            },
        )
        self._api_session.delete.asssert_not_called()

    async def test__create_ampq_policy_empty(self):
        self.ctx.parent_spec = {
            "policies": {},
        }

        with contextlib.ExitStack() as stack:
            _get_root_credentials = stack.enter_context(
                unittest.mock.patch.object(
                    self.ap,
                    "_get_root_credentials",
                )
            )
            _get_root_credentials.return_value = (
                unittest.mock.sentinel.api_url,
                unittest.mock.sentinel.rootuser,
                unittest.mock.sentinel.rootpassword,
                unittest.mock.sentinel.cacert,
            )

            _api_session = stack.enter_context(
                unittest.mock.patch.object(
                    self.ap,
                    "_api_session",
                    new=self._api_session_mock,
                )
            )
            self._api_session.get.side_effect = self._api_session_get_mock

            await self.ap.update(self.ctx, {})

        _api_session.assert_called_once_with(
            unittest.mock.sentinel.rootuser,
            unittest.mock.sentinel.rootpassword,
            unittest.mock.sentinel.cacert,
        )
        self._api_session.get.assert_called_once_with(
            str(unittest.mock.sentinel.api_url) + "/policies/"
        )
        self._api_session.delete.asssert_not_called()
        self._api_session.put.asssert_not_called()

    async def test__create_ampq_policy_defiened(self):
        self.ctx.parent_spec = {
            "policies": {
                "/": {
                    "testPolicy": {
                        "pattern": "^federated\\.",
                        "definition": {
                            "ha-mode": "exactly",
                            "ha-params": 2,
                        },
                        "priority": 1,
                        "applyto": "exchanges",
                    }
                },
            }
        }

        with contextlib.ExitStack() as stack:
            _get_root_credentials = stack.enter_context(
                unittest.mock.patch.object(
                    self.ap,
                    "_get_root_credentials",
                )
            )
            _get_root_credentials.return_value = (
                unittest.mock.sentinel.api_url,
                unittest.mock.sentinel.rootuser,
                unittest.mock.sentinel.rootpassword,
                unittest.mock.sentinel.cacert,
            )

            _api_session = stack.enter_context(
                unittest.mock.patch.object(
                    self.ap,
                    "_api_session",
                    new=self._api_session_mock,
                )
            )
            self._api_session.get.side_effect = self._api_session_get_mock
            self._api_session.put.side_effect = self._api_session_put_mock
            await self.ap.update(self.ctx, {})

        test_call = call(
            unittest.mock.sentinel.rootuser,
            unittest.mock.sentinel.rootpassword,
            unittest.mock.sentinel.cacert,
        )
        _api_session.assert_has_calls([test_call])
        self.assertEqual(_api_session.call_count, 1)

        self._api_session.get.assert_called_once_with(
            str(unittest.mock.sentinel.api_url) + "/policies/"
        )
        self._api_session.put.assert_called_once_with(
            f"{unittest.mock.sentinel.api_url}/policies/%2F/testPolicy",
            json={
                "pattern": "^federated\\.",
                "definition": {
                    "ha-mode": "exactly",
                    "ha-params": 2,
                },
                "priority": 1,
                "apply-to": "exchanges",
            },
        )
        self._api_session.delete.asssert_not_called()

    async def test__update_ampq_policy_defiened(self):
        self.ctx.parent_spec = {
            "policies": {
                "/": {
                    "testPolicy": {
                        "pattern": "^federated\\.",
                        "definition": {
                            "ha-mode": "exactly",
                            "ha-params": 2,
                        },
                        "priority": 1,
                        "applyto": "exchanges",
                    },
                    "testPolicy2": {
                        "pattern": "federated",
                        "definition": {
                            "ha-mode": "exactly",
                            "ha-params": 3,
                        },
                        "priority": 1,
                        "applyto": "exchanges",
                    },
                },
            }
        }

        with contextlib.ExitStack() as stack:
            _get_root_credentials = stack.enter_context(
                unittest.mock.patch.object(
                    self.ap,
                    "_get_root_credentials",
                )
            )
            _get_root_credentials.return_value = (
                unittest.mock.sentinel.api_url,
                unittest.mock.sentinel.rootuser,
                unittest.mock.sentinel.rootpassword,
                unittest.mock.sentinel.cacert,
            )
            self._api_session_get.json.return_value = [
                {
                    "vhost": str(unittest.mock.sentinel.existing_vhost),
                    "name": str(unittest.mock.sentinel.existing_name),
                }
            ]

            _api_session = stack.enter_context(
                unittest.mock.patch.object(
                    self.ap,
                    "_api_session",
                    new=self._api_session_mock,
                )
            )
            self._api_session.get.side_effect = self._api_session_get_mock
            self._api_session.delete.side_effect = (
                self._api_session_delete_mock
            )
            self._api_session.put.side_effect = self._api_session_put_mock
            await self.ap.update(self.ctx, {})

        test_call = call(
            unittest.mock.sentinel.rootuser,
            unittest.mock.sentinel.rootpassword,
            unittest.mock.sentinel.cacert,
        )
        _api_session.assert_has_calls([test_call])
        self.assertEqual(_api_session.call_count, 1)

        self._api_session.get.assert_called_once_with(
            str(unittest.mock.sentinel.api_url) + "/policies/"
        )

        test_put_call_1 = call(
            f"{unittest.mock.sentinel.api_url}/policies/%2F/testPolicy",
            json={
                "pattern": "^federated\\.",
                "definition": {
                    "ha-mode": "exactly",
                    "ha-params": 2,
                },
                "priority": 1,
                "apply-to": "exchanges",
            },
        )
        test_put_call_2 = call(
            f"{unittest.mock.sentinel.api_url}/policies/%2F/testPolicy2",
            json={
                "pattern": "federated",
                "definition": {
                    "ha-mode": "exactly",
                    "ha-params": 3,
                },
                "priority": 1,
                "apply-to": "exchanges",
            },
        )
        self._api_session.put.assert_has_calls(
            [test_put_call_1, test_put_call_2], any_order=True
        )
        self.assertEqual(_api_session.call_count, 1)

        self._api_session.delete.assert_called_once_with(
            str(unittest.mock.sentinel.api_url)
            + f"/policies/{str(unittest.mock.sentinel.existing_vhost)}/"
            f"{str(unittest.mock.sentinel.existing_name)}"
        )

    async def test__update_ampq_policy_empty(self):
        self.ctx.parent_spec = {"policies": {}}

        with contextlib.ExitStack() as stack:
            _get_root_credentials = stack.enter_context(
                unittest.mock.patch.object(
                    self.ap,
                    "_get_root_credentials",
                )
            )

            self._api_session_get.json.return_value = [
                {
                    "vhost": str(unittest.mock.sentinel.existing_vhost),
                    "name": str(unittest.mock.sentinel.existing_name),
                }
            ]

            _get_root_credentials.return_value = (
                unittest.mock.sentinel.api_url,
                unittest.mock.sentinel.rootuser,
                unittest.mock.sentinel.rootpassword,
                unittest.mock.sentinel.cacert,
            )

            _api_session = stack.enter_context(
                unittest.mock.patch.object(
                    self.ap,
                    "_api_session",
                    new=self._api_session_mock,
                )
            )
            self._api_session.get.side_effect = self._api_session_get_mock
            self._api_session.delete.side_effect = (
                self._api_session_delete_mock
            )

            await self.ap.update(self.ctx, {})

        test_call = call(
            unittest.mock.sentinel.rootuser,
            unittest.mock.sentinel.rootpassword,
            unittest.mock.sentinel.cacert,
        )
        _api_session.assert_has_calls([test_call])
        self.assertEqual(_api_session.call_count, 1)

        self._api_session.get.assert_called_once_with(
            str(unittest.mock.sentinel.api_url) + "/policies/"
        )

        self._api_session.delete.assert_called_once_with(
            str(unittest.mock.sentinel.api_url)
            + f"/policies/{str(unittest.mock.sentinel.existing_vhost)}/"
            f"{str(unittest.mock.sentinel.existing_name)}"
        )

        self._api_session.put.assert_not_called()

    async def test__update_ampq_policy_default(self):
        self.ctx.parent_spec = {}

        with contextlib.ExitStack() as stack:
            _get_root_credentials = stack.enter_context(
                unittest.mock.patch.object(
                    self.ap,
                    "_get_root_credentials",
                )
            )

            self._api_session_get.json.return_value = [
                {
                    "vhost": str(unittest.mock.sentinel.existing_vhost),
                    "name": str(unittest.mock.sentinel.existing_name),
                },
                {"vhost": "/", "name": "openstack-default"},
            ]

            _get_root_credentials.return_value = (
                unittest.mock.sentinel.api_url,
                unittest.mock.sentinel.rootuser,
                unittest.mock.sentinel.rootpassword,
                unittest.mock.sentinel.cacert,
            )

            _api_session = stack.enter_context(
                unittest.mock.patch.object(
                    self.ap,
                    "_api_session",
                    new=self._api_session_mock,
                )
            )
            self._api_session.get.side_effect = self._api_session_get_mock
            self._api_session.delete.side_effect = (
                self._api_session_delete_mock
            )
            self._api_session.put.side_effect = self._api_session_put_mock
            await self.ap.update(self.ctx, {})

        test_call = call(
            unittest.mock.sentinel.rootuser,
            unittest.mock.sentinel.rootpassword,
            unittest.mock.sentinel.cacert,
        )
        _api_session.assert_has_calls([test_call])
        self.assertEqual(_api_session.call_count, 1)

        self._api_session.get.assert_called_once_with(
            str(unittest.mock.sentinel.api_url) + "/policies/"
        )

        self._api_session.delete.assert_called_once_with(
            str(unittest.mock.sentinel.api_url)
            + f"/policies/{str(unittest.mock.sentinel.existing_vhost)}/"
            f"{str(unittest.mock.sentinel.existing_name)}"
        )

        self._api_session.put.assert_called_once_with(
            f"{unittest.mock.sentinel.api_url}/policies/%2F/openstack-default",
            json={
                "pattern": "^(?!(amq\\.)|(.*_fanout_)|(reply_)).*",
                "definition": {
                    "expires": 3600000,
                    "ha-mode": "all",
                    "ha-promote-on-failure": "always",
                    "ha-promote-on-shutdown": "always",
                    "ha-sync-mode": "manual",
                    "message-ttl": 600000,
                    "queue-master-locator": "client-local",
                },
                "priority": 1,
                "apply-to": "all",
            },
        )

    async def test__update_does_nothing_if_policy_matches(self):
        self.ctx.parent_spec = {
            "policies": {
                "/": {
                    "testPolicy": {
                        "pattern": "^federated\\.",
                        "definition": {
                            "ha-mode": "exactly",
                            "ha-params": 2,
                        },
                        "priority": 1,
                        "applyto": "exchanges",
                    }
                },
            }
        }

        with contextlib.ExitStack() as stack:
            _get_root_credentials = stack.enter_context(
                unittest.mock.patch.object(
                    self.ap,
                    "_get_root_credentials",
                )
            )

            self._api_session_get.json.return_value = [
                {
                    "vhost": "/",
                    "name": "testPolicy",
                    "pattern": "^federated\\.",
                    "definition": {
                        "ha-mode": "exactly",
                        "ha-params": 2,
                    },
                    "priority": 1,
                    "apply-to": "exchanges",
                }
            ]

            _get_root_credentials.return_value = (
                unittest.mock.sentinel.api_url,
                unittest.mock.sentinel.rootuser,
                unittest.mock.sentinel.rootpassword,
                unittest.mock.sentinel.cacert,
            )

            _api_session = stack.enter_context(
                unittest.mock.patch.object(
                    self.ap,
                    "_api_session",
                    new=self._api_session_mock,
                )
            )
            self._api_session.get.side_effect = self._api_session_get_mock
            self._api_session.delete.side_effect = (
                self._api_session_delete_mock
            )
            self._api_session.put.side_effect = self._api_session_put_mock
            await self.ap.update(self.ctx, {})

        test_call = call(
            unittest.mock.sentinel.rootuser,
            unittest.mock.sentinel.rootpassword,
            unittest.mock.sentinel.cacert,
        )
        _api_session.assert_has_calls([test_call])
        self.assertEqual(_api_session.call_count, 1)

        self._api_session.get.assert_called_once_with(
            str(unittest.mock.sentinel.api_url) + "/policies/"
        )

        self._api_session.delete.assert_not_called()

        self._api_session.put.assert_not_called()
