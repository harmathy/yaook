#
# Copyright (c) 2024 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import base64
import os
import kubernetes_asyncio.client as kclient
import unittest
import ddt
from unittest.mock import AsyncMock, MagicMock

import yaml
from tests import testutils

import tests.op.common_tests as ct
from yaook.op import scheduling_keys
import yaook.statemachine as sm
import yaook.op.designate as designate
from yaook.statemachine import interfaces
from yaook.statemachine import context
import yaook.op.common as op_common

NAME = "designate"
NAMESPACE = "test-namespace"

CONFIG_FILE_NAME = "designate.conf"
CONFIG_SECRET_NAME = "powerdns-api-key"
CONFIG_SECRET_KEY = "password"
CONFIG_SECRET_VALUE = "mysecretvalue"
POLICY_FILE_NAME = "policy.yaml"
POLICY_RULE_KEY = "create_blacklist"
POLICY_RULE_VALUE = "role:admin"
DESIGNATE_POLICY = {"policy": {POLICY_RULE_KEY: POLICY_RULE_VALUE}}

NAMESERVERS = [
    {"nameserver": "name1.ser.ver", "port": 53},
    {"nameserver": "name2.ser.ver", "port": 54}
]
NS_RECORDS = [
    {"hostname": "ho.st.name",  "priority": 1},
    {"hostname": "gho.st.name", "priority": 2}
]
LOAD_BALANCER_IP = "185.185.185.185"
MDNS_CLUSTER_IP = "10.0.0.1"
PDNS_DNS_CLUSTER_IP = "10.0.0.2"


class TestDesignatePoolsLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self._target = "designate_pools"
        self.expected_secret_key = "secret-key"
        self.expected_secret_value = "secret-value"
        self.loadBalancerIP = LOAD_BALANCER_IP
        self.pdns_api_svc_name = "pdns-api-service"

        self.ns_records = [
            {"hostname": "ho.st.name",  "priority": 1},
            {"hostname": "gho.st.name", "priority": 2}
        ]
        self.nameservers = [
            {"nameserver": "name1.ser.ver", "port": 53},
            {"nameserver": "name2.ser.ver", "port": 54}
        ]

        self.default_parent_spec = {
            "powerdns": {
                "loadBalancerIP": self.loadBalancerIP,
            },
            "nsRecords": NS_RECORDS,
            "additionalNameservers": NAMESERVERS
        }

        self.pdns_api_svc = unittest.mock.AsyncMock(sm.KubernetesReference)
        self.pdns_webserver_svc = unittest.mock.AsyncMock(sm.KubernetesReference) # Noqa 501
        self.mdns_svc = unittest.mock.AsyncMock(sm.KubernetesReference)
        self.api_key_secret = unittest.mock.AsyncMock(sm.KubernetesReference)

        self._setup_svc(self.pdns_api_svc, self.pdns_api_svc_name)
        self._setup_svc(
            self.pdns_webserver_svc,
            "pdns-webserver-service",
            PDNS_DNS_CLUSTER_IP
        )
        self._setup_svc(
            self.mdns_svc,
            "designate-mdns-service",
            MDNS_CLUSTER_IP
        )

    @unittest.mock.patch(
         "yaook.statemachine.interfaces.secret_interface")
    async def test_get_layer_inserts_pools_data(self, secret_interface):
        self._setup_secret_interface(secret_interface)

        expected_output = {
            "#pdns_api_host": f"{self.pdns_api_svc_name}.{NAMESPACE}.svc",
            "#pdns_webserver_host": PDNS_DNS_CLUSTER_IP,
            "#designate_mdns_host": MDNS_CLUSTER_IP,
            "#api_token": self.expected_secret_value,
            "#nameservers": self.nameservers,
            "ns_records": self.ns_records
        }

        layer = designate.DesignatePoolsLayer(
            target=self._target,
            pdns_api_service=self.pdns_api_svc,
            pdns_webserver_service=self.pdns_webserver_svc,
            designate_mdns_service=self.mdns_svc,
            api_key_secret=self.api_key_secret
        )

        ctx = unittest.mock.Mock(["api_client", "namespace"])
        ctx.parent_spec = self.default_parent_spec

        res = await layer.get_layer(ctx)

        target_content = res[self._target].contents
        self.assertEqual(1, len(target_content))
        self.assertDictEqual(expected_output, target_content[0][0])

    @unittest.mock.patch(
        "yaook.statemachine.interfaces.secret_interface")
    async def test_get_layer_api_key_secret_interface_raises_api_exception(
        self, secret_interface
    ):
        secret_interface_mock = unittest.mock.AsyncMock(sm.ResourceInterface)
        secret_interface_mock.read.side_effect = kclient.ApiException()

        secret_interface.return_value = secret_interface_mock

        layer = designate.DesignatePoolsLayer(
            target=self._target,
            pdns_api_service=self.pdns_api_svc,
            pdns_webserver_service=self.pdns_webserver_svc,
            designate_mdns_service=self.mdns_svc,
            api_key_secret=self.api_key_secret
        )

        ctx = unittest.mock.AsyncMock(["api_client", "namespace"])
        ctx.parent_spec = self.default_parent_spec

        with self.assertRaisesRegex(
            RuntimeError, f"{designate.DESIGNATE_POOL_CREATION_ERR}.*"
        ):
            await layer.get_layer(ctx)

    @unittest.mock.patch(
         "yaook.statemachine.interfaces.secret_interface")
    @unittest.mock.patch(
        "yaook.statemachine.interfaces.service_interface")
    async def test_get_layer_api_key_service_interface_raises_api_exception(
        self, secret_interface, service_interface
    ):
        self._setup_secret_interface(secret_interface)

        svc_interface_mock = unittest.mock.AsyncMock(sm.ResourceInterface)
        svc_interface_mock.read.side_effect = kclient.ApiException()

        service_interface.return_value = svc_interface_mock

        layer = designate.DesignatePoolsLayer(
            target=self._target,
            pdns_api_service=self.pdns_api_svc,
            pdns_webserver_service=self.pdns_webserver_svc,
            designate_mdns_service=self.mdns_svc,
            api_key_secret=self.api_key_secret
        )

        ctx = unittest.mock.AsyncMock(["api_client", "namespace"])
        ctx.parent_spec = self.default_parent_spec

        with self.assertRaisesRegex(
            RuntimeError, f"{designate.DESIGNATE_POOL_CREATION_ERR}.*"
        ):
            await layer.get_layer(ctx)

    def _setup_secret_interface(self, secret_interface: MagicMock):
        secret_interface_mock = unittest.mock.AsyncMock(sm.ResourceInterface)
        expected_secret_key = "password"
        expected_secret_value = "secret-value"

        secret_interface.return_value = secret_interface_mock
        secretdata = sm.api_utils.encode_secret_data({
            expected_secret_key: expected_secret_value
        })
        secret = kclient.V1Secret(data=secretdata)
        secret_interface_mock.read.return_value = secret

        secret_object = kclient.V1ObjectReference(name="test")
        self.api_key_secret.get.return_value = secret_object

    def _setup_svc(self, svc, name, ip=None):
        service_ref = kclient.V1ObjectReference(
            name=name,
            namespace=NAMESPACE,
        )

        svc.get.return_value = service_ref

        svc_interface = unittest.mock.AsyncMock(sm.ResourceInterface)
        spec = kclient.V1ServiceSpec(cluster_ip=ip)

        svc_interface.read.return_value = kclient.V1Service(spec=spec)
        svc.get_resource_interface.return_value = svc_interface


async def mock_get_cluster_ip(ctx, svc):
    if svc.component == 'mdns_service':
        return MDNS_CLUSTER_IP
    elif svc.component == 'powerdns_webserver_service':
        return PDNS_DNS_CLUSTER_IP
    return None


@ddt.ddt
@unittest.mock.patch.object(
    designate.DesignatePoolsLayer, 'get_cluster_ip',
    AsyncMock(side_effect=mock_get_cluster_ip))
class TestDesignateDeployments(
        testutils.ReleaseAwareCustomResourceTestCase,
        testutils.DatabaseTestMixin,
        testutils.MessageQueueTestMixin,
        testutils.MemcachedTestMixin,
        testutils.KeystoneRefTestMixin):

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._config_file_name = "designate.conf"
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            designate.Designate,
            self.get_os_deployment_yaml(),
        )

    def configure_cr(self, deployment_yaml: dict):
        self._configure_cr(designate.Designate, deployment_yaml)

    def get_os_deployment_yaml(self):
        return {
            "metadata": {
                "name": NAME,
                "namespace": NAMESPACE
            },
            "spec": {
                "keystoneRef": {
                    "name": self._keystone_name,
                    "kind": "KeystoneDeployment",
                },
                "api": {
                    "replicas": 3,
                    "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                    "ingress": {
                        "fqdn": f"designate.{NAMESPACE}.cloud",
                        "port": 32443,
                        "ingressClassName": "nginx",
                    },
                    "resources": testutils.generate_resources_dict(
                        "api.designate-api",
                        "api.ssl-terminator",
                        "api.ssl-terminator-external",
                        "api.service-reload",
                        "api.service-reload-external",
                    ),
                },
                "database": ct.get_default_database_config(),
                "memcached": {
                    "replicas": 2,
                    "memory": "512",
                    "connections": "2000",
                    "resources": testutils.generate_memcached_resources(),
                },
                "messageQueue": {
                    "replicas": 1,
                    "storageSize": "2Gi",
                    "storageClassName": "bar-class",
                    "resources": testutils.generate_amqp_resources(),
                },
                "issuerRef": {
                    "name": "issuername"
                },
                "targetRelease": "2024.1",
                "region": {
                    "name": "regionname",
                    "parent": "parentregionname",
                },
                "nsRecords": NS_RECORDS,
                "additionalNameservers": NAMESERVERS,
                "powerdns": {
                    "replicas": 2,
                    "targetRelease": "4.9",
                    "subnetCidr": "10.0.0.0/24",
                    "loadBalancerIP": LOAD_BALANCER_IP,
                    "apiKeySecret": {
                        "secretName": CONFIG_SECRET_NAME,
                        "key": CONFIG_SECRET_KEY
                    },
                    "database": ct.get_default_database_config(),
                },
                "central": {
                    "replicas": 2,
                    "resources":
                        testutils.generate_resources_dict("central.central")
                },
                "minidns": {
                    "replicas": 2,
                    "resources":
                        testutils.generate_resources_dict("minidns.minidns")
                },
                "producer": {
                    "replicas": 2,
                    "resources":
                        testutils.generate_resources_dict("producer.producer")
                },
                "worker": {
                    "replicas": 2,
                    "resources":
                        testutils.generate_resources_dict("worker.worker")
                },
                "jobResources": testutils.generate_resources_dict(
                    "job.designate-db-sync-job",
                    "job.designate-pool-update-job"
                ),
            }
        }

    def get_config_name(self):
        return "designate.conf"

    async def test_creates_certificate_and_halts(self):
        await ct.test_creates_certificate_and_halts_async(self, NAMESPACE)

    async def test_certificate_contains_service_name(self):
        await ct.test_certificate_contains_service_name_async(
            self, "api_service", NAMESPACE
        )

    async def test_certificate_contains_issuer_name(self):
        await ct.test_certificate_contains_issuer_name_async(self, NAMESPACE)

    async def test_creates_database_and_user(self):
        await ct.test_creates_database_and_user_async(self, NAMESPACE)

    async def test_creates_config_with_database_uri(self):
        await ct.test_creates_config_with_database_uri(
            self,
            NAMESPACE,
            "designate.conf",
            "storage:sqlalchemy",
            "connection"
        )

    async def test_creates_pools_config(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        cfg_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "pools_config",
            },
        )

        secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "powerdns_api_key",
            }
        )

        pdns_api_svc, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    op_common.POWERDNS_API_SERVICE_COMPONENT,
                context.LABEL_PARENT_PLURAL: "powerdnsservices",
            },
        )

        conf = cfg_secret.data["designate_pools.yaml"]
        decoded_conf = base64.b64decode(conf.encode("ascii")).decode("utf-8")

        parsed_yaml = yaml.safe_load(decoded_conf)
        default_pool = parsed_yaml[0]

        self.assertEqual(
            [{'host': PDNS_DNS_CLUSTER_IP, 'port': 53}] + NAMESERVERS,
            default_pool["nameservers"]
        )
        self.assertEqual(NS_RECORDS, default_pool["ns_records"])
        self.assertEqual(
            MDNS_CLUSTER_IP,
            default_pool["targets"][0]["masters"][0]["host"]
        )
        self.assertEqual(
            PDNS_DNS_CLUSTER_IP,
            default_pool["targets"][0]["options"]["host"]
        )
        self.assertEqual(
            f"https://{pdns_api_svc.metadata.name}.{NAMESPACE}.svc:8100",
            default_pool["targets"][0]["options"]["api_endpoint"]
        )

        sec = secret.data[CONFIG_SECRET_KEY]
        decoded_api_key = base64.b64decode(sec.encode("ascii")).decode("utf-8")

        self.assertEqual(
            decoded_api_key,
            default_pool["targets"][0]["options"]["api_token"],
        )

    async def test_creates_message_queue_and_user(self):
        await ct.test_creates_message_queue_and_user(
            self,
            NAMESPACE,
            "mq_api_user",
            "mq_api_user_password"
        )

    async def test_creates_config_with_transport_url(self):
        await ct.test_creates_config_with_transport_url(
            self,
            NAMESPACE,
            self._config_file_name
        )

    async def test_amqp_server_frontendIssuer_name(self):
        await ct.test_amqp_server_frontendIssuer_name(self, NAMESPACE)

    async def test_creates_memcached(self):
        await ct.test_creates_memcached(self, NAMESPACE)

    async def test_creates_api_service(self):
        await ct.test_creates_service_for_deployment_async(
            self,
            NAMESPACE,
            "api_deployment",
            "api_service"
        )

    async def test_creates_mdns_service(self):
        await ct.test_creates_service_for_deployment_async(
            self,
            NAMESPACE,
            "minidns_deployment",
            "mdns_service"
        )

    async def test_creates_endpoint_with_ingress(self):
        await ct.test_creates_endpoint_with_ingress(
            self,
            NAMESPACE,
            "designate-api-endpoint",
            f"https://designate.{NAMESPACE}.cloud:32443"
        )

    async def test_creates_ingress(self):
        await ct.test_creates_ingress(
            self,
            NAMESPACE,
            "designate",
            f"designate.{NAMESPACE}.cloud"
        )

    async def test_disable_ingress_creation(self):
        await ct.test_disable_ingress_creation(
            self,
            NAMESPACE,
            designate.Designate,
            self.get_os_deployment_yaml
        )

    async def test_enable_ingress_creation(self):
        await ct.test_enable_ingress_creation(
            self,
            NAMESPACE,
            designate.Designate,
            self.get_os_deployment_yaml
        )

    async def test_ingress_force_ssl_annotation(self):
        await ct.test_ingress_force_ssl_annotation(
            self,
            NAMESPACE,
            "designate"
        )

    async def test_ingress_matches_service(self):
        await ct.test_ingress_matches_service(
            self,
            NAMESPACE,
            "designate",
            "designate-api"
        )

    async def test_applies_scheduling_key_to_api_deployment(self):
        await ct.test_applies_scheduling_key_to_deployment(
            self,
            NAMESPACE,
            "api_deployment",
            [
                scheduling_keys.SchedulingKey.DESIGNATE_API.value,
                scheduling_keys.SchedulingKey.ANY_API.value,
            ]
        )

    @ddt.unpack
    @ddt.data(
        ["central_deployment",
         scheduling_keys.SchedulingKey.DESIGNATE_CENTRAL.value],
        ["minidns_deployment",
         scheduling_keys.SchedulingKey.DESIGNATE_MDNS.value],
        ["worker_deployment",
         scheduling_keys.SchedulingKey.DESIGNATE_WORKER.value],
        ["producer_deployment",
         scheduling_keys.SchedulingKey.DESIGNATE_PRODUCER.value],
    )
    async def test_applies_scheduling_key_to_deployment(
        self,
        component,
        scheduling_key
    ):
        await ct.test_applies_scheduling_key_to_deployment(
            self,
            NAMESPACE,
            component,
            [
                scheduling_key,
                scheduling_keys.SchedulingKey.DESIGNATE_ANY_SERVICE.value,
            ]
        )

    @ddt.data("db_sync", "pool_update")
    async def test_applies_scheduling_key_to_db_sync_job(self, job):
        await ct.test_applies_scheduling_key_to_job(
            self,
            NAMESPACE,
            job,
            designate.JOB_SCHEDULING_KEYS
        )

    @ddt.data({}, DESIGNATE_POLICY)
    async def test_creates_config_with_policy_file(self, policy):
        deployment_yaml = self.get_os_deployment_yaml()
        deployment_yaml["spec"].update(policy)
        self._configure_cr(designate.Designate, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        expected_policy_file = "/etc/designate/policy.yaml"

        secrets = interfaces.secret_interface(self.api_client)

        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        conf_content = testutils._parse_config(
            config_secret.data[CONFIG_FILE_NAME],
            decode=True
        )

        observed_policy_file = \
            conf_content.get("oslo_policy", "policy_file")

        self.assertEqual(observed_policy_file, expected_policy_file)

    container_indices = {
        "designate-api": 0,
        "ssl-terminator": 1,
        "ssl-terminator-external": 2,
        "service-reload": 3,
        "service-reload-external": 4,
    }
    expected_api_depl_volume_indices = {
        "config": 0,
        "ca_certs": 1,
        "tls-secret": 2,
        "tls-secret-external": 3,
        "ssl-terminator-config": 4,
        "ssl-terminator-external-config": 5,
        "pools": 6,
    }
    expected_api_depl_vol_mount_indices = {
        "designate-api": {
            "config": 0,
            "ca_certs": 1,
            "policy": 2,
            "pools": 3,
        },
        "ssl_term_container": {
            "config": 0,
            "tls": 1,
            "ca_certs": 2,
        },
        "reload_container": {
            "config": 0,
            "tls": 1,
        }
    }

    @ddt.data({}, DESIGNATE_POLICY)
    async def test_creates_api_deployment_with_projected_volume(self, policy):
        deployment_yaml = self.get_os_deployment_yaml()
        deployment_yaml["spec"].update(policy)
        self._configure_cr(designate.Designate, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_maps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )

        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        policy_config_map, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "policy"},
        )

        designate_api_container_spec = \
            api_deployment.spec.template.spec.containers[0]

        # Build list with names of volumes and volume mounts of the container
        # in the Deployment
        observed_volumes = list(
            map(lambda v: v.name, api_deployment.spec.template.spec.volumes)
        )
        observed_volume_mounts = list(
            map(lambda v: v.name, designate_api_container_spec.volume_mounts)
        )

        # Assert container mounts config file correctly
        config_volume_name = "designate-config-volume"
        self.assertIn(config_volume_name, observed_volumes)
        self.assertIn(config_volume_name, observed_volume_mounts)

        observed_projected_volume = \
            api_deployment.spec.template.spec.volumes[
                observed_volumes.index(config_volume_name)
            ]
        observed_config_volume_mount = \
            designate_api_container_spec.volume_mounts[
                observed_volume_mounts.index(config_volume_name)
            ]

        # assert that config secret is in projected volume
        observed_projected_volume_sources = \
            observed_projected_volume.projected.sources
        policy_vol_sources = [
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.secret and
            vol_source.secret.name == config_secret.metadata.name
        ]

        self.assertEqual(1, len(policy_vol_sources))

        self.assertEqual(
            observed_config_volume_mount.mount_path,
            os.path.join("/etc/designate", CONFIG_FILE_NAME),
        )
        self.assertEqual(
            observed_config_volume_mount.sub_path,
            CONFIG_FILE_NAME,
        )

        # assert policy
        policy_vol_source = [
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.config_map and
            vol_source.config_map.name == policy_config_map.metadata.name
        ]

        self.assertEquals(1, len(policy_vol_source))

        observed_config_volume_mount = \
            designate_api_container_spec.volume_mounts[
                self.expected_api_depl_vol_mount_indices["designate-api"]["policy"]  # Noqa 501
            ]
        self.assertEqual(
            observed_config_volume_mount.mount_path,
            os.path.join("/etc/designate", POLICY_FILE_NAME),
        )
        self.assertEqual(
            observed_config_volume_mount.sub_path,
            POLICY_FILE_NAME,
        )

    @ddt.data(
        "api_deployment",
        "central_deployment",
        "minidns_deployment",
        "worker_deployment",
        "producer_deployment"
    )
    async def test_creates_deployment_with_mounted_ca_certs(self, deployment):
        deployment_yaml = self.get_os_deployment_yaml()
        self._configure_cr(designate.Designate, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        config_maps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: deployment},
        )
        ca_cert_config_map, = await config_maps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
                context.LABEL_PARENT_NAME: "designate"
            },
        )

        designate_container_spec = \
            deployment.spec.template.spec.containers[
                self.container_indices["designate-api"]
            ]

        ca_volume = \
            deployment.spec.template.spec.volumes[
                self.expected_api_depl_volume_indices["ca_certs"]
            ]
        ca_volume_mount = \
            designate_container_spec.volume_mounts[
                self.expected_api_depl_vol_mount_indices["designate-api"]["ca_certs"] # Noqa 501
            ]

        self.assertEqual(
            ca_volume.config_map.name,
            ca_cert_config_map.metadata.name
        )
        self.assertEqual(
            ca_volume_mount.mount_path,
            "/etc/pki/tls/certs"
        )

    @ddt.data("api", "central", "minidns", "worker", "producer")
    async def test_database_uri_refers_to_mounted_ca_bundle(
        self, container_name
    ):
        deployment_yaml = self.get_os_deployment_yaml()
        self._configure_cr(designate.Designate, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_maps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: f"{container_name}_deployment"
            },
        )
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        ca_cert_config_map, = await config_maps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
                context.LABEL_PARENT_NAME: "designate"
            },
        )

        designate_conf = config_secret.data["designate.conf"]
        cfg = testutils._parse_config(designate_conf, decode=True)

        cert_mountpoint = testutils.find_volume_mountpoint(
            api_deployment.spec.template.spec,
            testutils.find_configmap_volume(
                api_deployment.spec.template.spec,
                ca_cert_config_map.metadata.name,
            ),
            container_name
        )

        self.assertIn(
            f"ssl_ca={cert_mountpoint}/ca-bundle.crt",
            cfg.get("storage:sqlalchemy", "connection"),
        )

    @ddt.unpack
    @ddt.data(
        ["ssl-terminator", "ssl-terminator-config", ],
        ["ssl-terminator-external", "ssl-terminator-external-config"]
    )
    async def test_creates_api_deployment_with_term_containers_volume_mounts(
            self, container, config):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )

        # assert container has correct number of volume mounts
        ssl_terminator_container_spec = \
            api_deployment.spec.template.spec.containers[
                self.container_indices[container]
            ]
        expected_ssl_terminator_container_volume_mounts = \
            self.expected_api_depl_vol_mount_indices["ssl_term_container"]
        self.assertEqual(
            len(ssl_terminator_container_spec.volume_mounts),
            len(expected_ssl_terminator_container_volume_mounts)
        )

        observed_config_volume = \
            api_deployment.spec.template.spec.volumes[
                self.expected_api_depl_volume_indices[config]
            ]
        observed_config_volume_mount = \
            ssl_terminator_container_spec.volume_mounts[
                expected_ssl_terminator_container_volume_mounts["config"]
            ]
        self.assertEqual(
            observed_config_volume_mount.name,
            observed_config_volume.name,
        )
        self.assertEqual(
            observed_config_volume_mount.mount_path,
            "/config"
        )

        # assert container mounting certs correctly
        observed_certs_volume = \
            api_deployment.spec.template.spec.volumes[
                self.expected_api_depl_volume_indices["ca_certs"]
            ]
        observed_certs_volume_mount = \
            ssl_terminator_container_spec.volume_mounts[
                expected_ssl_terminator_container_volume_mounts["ca_certs"]
            ]
        self.assertEqual(
            observed_certs_volume_mount.name,
            observed_certs_volume.name,
        )
        self.assertEqual(
            observed_certs_volume_mount.mount_path,
            "/etc/ssl/certs/ca-certificates.crt",
        )
        self.assertEqual(
            observed_certs_volume_mount.sub_path,
            "ca-bundle.crt",
        )

        observed_tls_volume_mount = \
            ssl_terminator_container_spec.volume_mounts[
                expected_ssl_terminator_container_volume_mounts["tls"]
            ]
        observed_tls_volume = \
            api_deployment.spec.template.spec.volumes[
                self.expected_api_depl_volume_indices[
                    observed_tls_volume_mount.name
                ]
            ]
        self.assertEqual(
            observed_tls_volume_mount.name,
            observed_tls_volume.name,
        )
        self.assertEqual(
            observed_tls_volume_mount.mount_path,
            "/data",
        )

    @ddt.unpack
    @ddt.data(
        ["service-reload", "ssl-terminator-config"],
        ["service-reload-external", "ssl-terminator-external-config"]
    )
    async def test_service_reload_volume_mounts(self, container, config):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )

        # assert container has correct number of volume mounts
        service_reload_container_spec = \
            api_deployment.spec.template.spec.containers[
                self.container_indices[container]
            ]
        expected_ssl_terminator_container_volume_mounts = \
            self.expected_api_depl_vol_mount_indices["reload_container"]
        self.assertEqual(
            len(service_reload_container_spec.volume_mounts),
            len(expected_ssl_terminator_container_volume_mounts)
        )

        # ssl-terminator-config
        observed_config_volume = \
            api_deployment.spec.template.spec.volumes[
                self.expected_api_depl_volume_indices[config]
            ]
        observed_config_volume_mount = \
            service_reload_container_spec.volume_mounts[
                expected_ssl_terminator_container_volume_mounts["config"]
            ]
        self.assertEqual(
            observed_config_volume_mount.name,
            observed_config_volume.name,
        )
        self.assertEqual(
            observed_config_volume_mount.mount_path,
            "/config"
        )

        # tls-secret
        observed_tls_volume_mount = \
            service_reload_container_spec.volume_mounts[
                expected_ssl_terminator_container_volume_mounts["tls"]
            ]
        observed_tls_volume = \
            api_deployment.spec.template.spec.volumes[
                self.expected_api_depl_volume_indices[
                    observed_tls_volume_mount.name
                ]
            ]
        self.assertEqual(
            observed_tls_volume_mount.name,
            observed_tls_volume.name,
        )
        self.assertEqual(
            observed_tls_volume_mount.mount_path,
            "/data",
        )

    async def test_creates_api_containers_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )

        self.assertEqual(
            testutils.container_resources(api_deployment, 0),
            testutils.unique_resources("api.designate-api")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 1),
            testutils.unique_resources("api.ssl-terminator")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 2),
            testutils.unique_resources("api.ssl-terminator-external")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 3),
            testutils.unique_resources("api.service-reload")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 4),
            testutils.unique_resources("api.service-reload-external")
        )

    @ddt.data("central", "minidns", "worker", "producer")
    async def test_creates_designate_services_with_resources(
        self, name
    ):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)

        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: f"{name}_deployment"}
        )

        self.assertEqual(
            testutils.container_resources(deployment, 0),
            testutils.unique_resources(f"{name}.{name}")
        )

    @ddt.data("db_sync", "pool_update")
    async def test_creates_designate_jobs_with_resources(
        self, job_name
    ):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        jobs = interfaces.job_interface(self.api_client)

        job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: job_name}
        )

        self.assertEqual(
            testutils.container_resources(job, 0),
            testutils.unique_resources(
                f"job.designate-{job_name}-job".replace("_", "-")
            )
        )
