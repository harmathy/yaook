#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import copy
import unittest
import unittest.mock
import uuid

import kubernetes_asyncio.client as kclient

import yaook.op.cds.resources as cds_resources
import yaook.statemachine.context as context
import yaook.statemachine.resources.instancing as instancing
import yaook.statemachine.watcher as watcher

from ...statemachine.resources.utils import SingleObjectMock


class Testhash_pod(unittest.TestCase):
    def test_hash_pod(self):
        podspec = {
            "metadata": "test123",
            "spec": "abc123",
            "irrelevantkey": "irrelevantvalue",
            "status": "irrelevantvalue",
            "apiVersion": "irrelevantvalue",
            "kind": "irrelevantvalue"
        }
        EXPECTED_HASH = ("2.05b0f156cff49931101e74eafbc2b150ae9babc21d97af8"
                         "a29e6794e5431842f")
        h = cds_resources.hash_pod(podspec)
        self.assertEqual(h, EXPECTED_HASH)

    def test_hash_pod_ignores_irrelevant_keys(self):
        podspec = {
            "metadata": "test123",
            "spec": "abc123",
            "irrelevantkey": "irrelevantvalue2",
            "status": "irrelevantvalue2",
            "apiVersion": "irrelevantvalue2",
            "kind": "irrelevantvalue2"
        }
        EXPECTED_HASH = ("2.05b0f156cff49931101e74eafbc2b150ae9babc21d97af8"
                         "a29e6794e5431842f")
        h = cds_resources.hash_pod(podspec)
        self.assertEqual(h, EXPECTED_HASH)


class Testmake_pod(unittest.TestCase):
    def test_make_pod(self):
        cds_spec = {
            "volumeTemplates": [
                {
                    "nodeMap": {
                        "node1": {
                            "template": {
                                "configMap": {
                                    "name": "map1",
                                },
                            },
                        },
                        "node2": {
                            "template": {
                                "configMap": {
                                    "name": "map2",
                                },
                            },
                        },
                    },
                    "volumeName": "vol1"
                }
            ],
            "template": {
                "metadata": {
                    "labels": {
                        "labelkey": "labelvalue"
                    }
                },
                "spec": {
                    "containers": "dummy",
                    "tolerations": [
                        "testtoleration"
                    ],
                    "volumes": [
                        {
                            "name": "testvolume"
                        },
                    ]
                }
            }
        }
        EXPECTED = {
            "apiVersion": "v1",
            "kind": "Pod",
            'metadata': {
                'labels': {
                    'labelkey': 'labelvalue',
                }
            },
            'spec': {
                'containers': 'dummy',
                'tolerations': [
                    'testtoleration',
                    {'effect': 'NoExecute',
                        'key': 'node.kubernetes.io/not-ready',
                        'operator': 'Exists'},
                    {'effect': 'NoExecute',
                        'key': 'node.kubernetes.io/unreachable',
                        'operator': 'Exists'},
                    {'effect': 'NoSchedule',
                        'key': 'node.kubernetes.io/disk-pressure',
                        'operator': 'Exists'},
                    {'effect': 'NoSchedule',
                        'key': 'node.kubernetes.io/memory-pressure',
                        'operator': 'Exists'},
                    {'effect': 'NoSchedule',
                        'key': 'node.kubernetes.io/pid-pressure',
                        'operator': 'Exists'},
                    {'effect': 'NoSchedule',
                        'key': 'node.kubernetes.io/unschedulable',
                        'operator': 'Exists'}
                ],
                'volumes': [
                    {'name': 'testvolume'},
                    {'configMap': {'name': 'map1'}, 'name': 'vol1'}
                ],
                'affinity': {'nodeAffinity': {
                    'requiredDuringSchedulingIgnoredDuringExecution': {
                        'nodeSelectorTerms': [
                            {'matchFields': [
                                {
                                    'key': 'metadata.name',
                                    'operator': 'In',
                                    'values': ['node1']
                                }
                            ]}
                        ]
                    }
                }}
            }
        }
        pod = cds_resources.make_pod(cds_spec, "node1")
        self.assertEqual(pod, EXPECTED)


class Testset_node_affinity(unittest.TestCase):
    def test_set_node_affinity(self):
        EXPECTED = [
            {
                "matchFields": [
                    {
                        "key": "metadata.name",
                        "operator": "In",
                        "values": ["testnode"],
                    }
                ]
            }
        ]

        nodeSelectorTerms = []
        cds_resources._set_node_affinity(nodeSelectorTerms, "testnode")
        self.assertEqual(nodeSelectorTerms, EXPECTED)

        nodeSelectorTerms = ["abc123"]
        cds_resources._set_node_affinity(nodeSelectorTerms, "testnode")
        self.assertEqual(nodeSelectorTerms, EXPECTED)

        nodeSelectorTerms = [EXPECTED]
        cds_resources._set_node_affinity(nodeSelectorTerms, "testnode")
        self.assertEqual(nodeSelectorTerms, EXPECTED)


class Testupdate_volumes(unittest.TestCase):
    def test_update_volumes_no_default(self):
        volume_templates = [
            {
                "volumeName": "vol1",
                "nodeMap": {"node1": unittest.mock.sentinel.foo},
            }
        ]
        pod_volumes = []

        with self.assertRaises(ValueError):
            cds_resources._update_volumes(
                pod_volumes,
                volume_templates,
                "node2",
            )

    def test_update_volumes_instantiates_template_from_node_map(self):
        volume_templates = [
            {
                "volumeName": "vol1",
                "nodeMap": {
                    "node1": {
                        "template": {
                            "configMap": unittest.mock.sentinel.cfg,
                        },
                    },
                },
            }
        ]

        result = cds_resources._update_volumes([], volume_templates, "node1")

        self.assertEqual(
            result,
            [
                {
                    "name": "vol1",
                    "configMap": unittest.mock.sentinel.cfg,
                },
            ]
        )

    def test_update_volumes_copies_template(self):
        volume_templates = [
            {
                "volumeName": "vol1",
                "nodeMap": {
                    "node1": {
                        "template": {
                            "configMap": unittest.mock.sentinel.cfg,
                        },
                    },
                },
            }
        ]

        result = cds_resources._update_volumes([], volume_templates, "node1")

        volume_templates[0]["nodeMap"]["node1"]["foo"] = "bar"

        self.assertEqual(
            result,
            [
                {
                    "name": "vol1",
                    "configMap": unittest.mock.sentinel.cfg,
                },
            ]
        )

    def test_update_volumes_instantiates_default_template(self):
        volume_templates = [
            {
                "volumeName": "vol1",
                "default": {
                    "template": {
                        "configMap": unittest.mock.sentinel.other_cfg,
                    },
                },
                "nodeMap": {
                    "node1": {
                        "configMap": unittest.mock.sentinel.cfg,
                    },
                },
            }
        ]

        result = cds_resources._update_volumes([], volume_templates, "node2")

        self.assertEqual(
            result,
            [
                {
                    "name": "vol1",
                    "configMap": unittest.mock.sentinel.other_cfg,
                },
            ]
        )

    def test_update_volumes_rejects_if_same_named_volume_exists(self):
        volume_templates = [
            {
                "volumeName": "vol1",
                "nodeMap": {
                    "node1": {
                        "template": {
                            "configMap": unittest.mock.sentinel.cfg,
                        },
                    },
                },
            }
        ]
        pod_volumes = [
            {
                "name": "vol1",
            }
        ]

        with self.assertRaisesRegex(
                ValueError,
                r"volume 'vol1' exists already inside pod"):
            cds_resources._update_volumes(
                pod_volumes,
                volume_templates,
                "node1",
            )

    def test_update_volumes_rejects_duplicate_template_name(self):
        volume_templates = [
            {
                "volumeName": "vol1",
                "nodeMap": {
                    "node1": {
                        "template": {
                            "configMap": unittest.mock.sentinel.cfg,
                        },
                    },
                },
            },
            {
                "volumeName": "vol1",
                "nodeMap": {
                    "node1": {
                        "template": {
                            "configMap": unittest.mock.sentinel.cfg,
                        },
                    },
                },
            }
        ]

        with self.assertRaisesRegex(
                ValueError,
                r"volume 'vol1' is declared multiple times"):
            cds_resources._update_volumes([], volume_templates, "node1")

    def test_update_volumes_overwrites_existing_name(self):
        volume_templates = [
            {
                "volumeName": "vol1",
                "nodeMap": {
                    "node1": {
                        "template": {
                            "name": "foobar",
                            "configMap": unittest.mock.sentinel.cfg,
                        },
                    },
                },
            }
        ]

        result = cds_resources._update_volumes([], volume_templates, "node1")

        self.assertEqual(
            result,
            [
                {
                    "configMap": unittest.mock.sentinel.cfg,
                    "name": "vol1",
                }
            ]
        )

    def test_update_volumes_multiple_volumes(self):
        volume_templates = [
            {
                "volumeName": "vol1",
                "nodeMap": {
                    "node1": {
                        "template": {
                            "configMap": unittest.mock.sentinel.cfg1,
                        },
                    },
                },
            },
            {
                "volumeName": "vol2",
                "nodeMap": {
                    "node1": {
                        "template": {
                            "secretRef": unittest.mock.sentinel.secret1,
                        },
                    },
                },
            },
            {
                "volumeName": "vol3",
                "nodeMap": {
                    "node1": {
                        "template": {
                            "persistentVolumeClaim":
                                unittest.mock.sentinel.pvc1,
                        },
                    },
                },
            }
        ]

        result = cds_resources._update_volumes([], volume_templates, "node1")

        self.assertEqual(
            result,
            [
                {
                    "name": "vol1",
                    "configMap": unittest.mock.sentinel.cfg1,
                },
                {
                    "name": "vol2",
                    "secretRef": unittest.mock.sentinel.secret1,
                },
                {
                    "name": "vol3",
                    "persistentVolumeClaim": unittest.mock.sentinel.pvc1,
                },
            ]
        )

    def test_update_volumes_preserves_existing_volumes(self):
        volume_templates = [
            {
                "volumeName": "vol1",
                "nodeMap": {
                    "node1": {
                        "template": {
                            "configMap": unittest.mock.sentinel.cfg1,
                        },
                    },
                },
            },
            {
                "volumeName": "vol2",
                "nodeMap": {
                    "node1": {
                        "template": {
                            "secretRef": unittest.mock.sentinel.secret1,
                        },
                    },
                },
            },
            {
                "volumeName": "vol3",
                "nodeMap": {
                    "node1": {
                        "template": {
                            "persistentVolumeClaim":
                                unittest.mock.sentinel.pvc1,
                        },
                    },
                },
            }
        ]

        pod_volumes = [
            {
                "name": "existing-vol1",
                "persistentVolumeClaim": unittest.mock.sentinel.pvc2,
            },
            {
                "name": "existing-vol2",
                "configMap": unittest.mock.sentinel.cfg2,
            },
        ]
        pod_volumes_bak = copy.deepcopy(pod_volumes)

        result = cds_resources._update_volumes(
            pod_volumes,
            volume_templates,
            "node1",
        )

        self.assertEqual(
            result,
            pod_volumes + [
                {
                    "name": "vol1",
                    "configMap": unittest.mock.sentinel.cfg1,
                },
                {
                    "name": "vol2",
                    "secretRef": unittest.mock.sentinel.secret1,
                },
                {
                    "name": "vol3",
                    "persistentVolumeClaim": unittest.mock.sentinel.pvc1,
                },
            ]
        )
        self.assertEqual(pod_volumes, pod_volumes_bak)


class TestCDSPodResource(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.component = str(uuid.uuid4())
        self.cdspr = cds_resources.CDSPodResource(
           component=str(self.component),
        )

    def test_labels_returns_controller_uid_only(self):
        uid = str(uuid.uuid4())
        ctx = unittest.mock.Mock([
            "base_label_match",
        ])
        ctx.parent = {"metadata": {"uid": uid}}
        ctx.base_label_match.return_value = {
            context.LABEL_PARENT_GROUP: "foo",
            context.LABEL_PARENT_PLURAL: "foo",
            context.LABEL_PARENT_VERSION: "foo",
            context.LABEL_PARENT_NAME: "foo",
            context.LABEL_INSTANCE: "foo",
        }

        labels = self.cdspr.labels(ctx)

        self.assertDictEqual(
            labels,
            {
                context.LABEL_CDS_CONTROLLER_UID: uid,
            },
        )

    async def test_adopt_object_does_not_set_sm_labels(self):
        obj = {}
        ctx = unittest.mock.Mock([
            "parent_api_version",
            "parent_name",
            "parent_kind",
            "parent_uid",
            "base_label_match",
            "instance",
        ])
        ctx.parent = {"metadata": {"uid": str(uuid.uuid4())}}
        ctx.base_label_match.return_value = {
            context.LABEL_PARENT_GROUP: "foo",
            context.LABEL_PARENT_PLURAL: "foo",
            context.LABEL_PARENT_VERSION: "foo",
            context.LABEL_PARENT_NAME: "foo",
            context.LABEL_INSTANCE: "foo",
        }

        await self.cdspr.adopt_object(ctx, obj)

        self.assertNotIn(
            context.LABEL_COMPONENT,
            obj["metadata"]["labels"],
        )
        self.assertNotIn(
            context.LABEL_PARENT_GROUP,
            obj["metadata"]["labels"],
        )
        self.assertNotIn(
            context.LABEL_PARENT_PLURAL,
            obj["metadata"]["labels"],
        )
        self.assertNotIn(
            context.LABEL_PARENT_VERSION,
            obj["metadata"]["labels"],
        )
        self.assertNotIn(
            context.LABEL_PARENT_NAME,
            obj["metadata"]["labels"],
        )
        self.assertNotIn(
            context.LABEL_INSTANCE,
            obj["metadata"]["labels"],
        )

    async def test_adopt_object_adds_pod_hash_before_anything_else(self):
        obj = {"spec": unittest.mock.sentinel.spec}
        obj_bak = copy.deepcopy(obj)
        ctx = unittest.mock.Mock([
            "parent_api_version",
            "parent_name",
            "parent_kind",
            "parent_uid",
            "base_label_match",
            "instance",
        ])
        pod_hash = unittest.mock.sentinel.pod_hash
        ctx.parent = {"metadata": {"uid": str(uuid.uuid4())}}
        ctx.base_label_match.return_value = {}

        def capture_hash_pod(pod):
            self.assertEqual(pod, obj_bak)
            return pod_hash

        with contextlib.ExitStack() as stack:
            hash_pod = stack.enter_context(unittest.mock.patch(
                "yaook.op.cds.resources.hash_pod",
            ))
            hash_pod.side_effect = capture_hash_pod

            await self.cdspr.adopt_object(ctx, obj)

        self.assertEqual(
            obj["metadata"]["annotations"][cds_resources.HASH_ANNOTATION],
            pod_hash,
        )

    async def test_adopt_object_sets_generate_name(self):
        obj = {}
        ctx = unittest.mock.Mock([
            "parent_api_version",
            "parent_name",
            "parent_kind",
            "parent_uid",
            "base_label_match",
            "instance",
        ])
        ctx.parent = {"metadata": {"uid": str(uuid.uuid4())}}
        ctx.base_label_match.return_value = {}

        await self.cdspr.adopt_object(ctx, obj)

        self.assertEqual(
            obj["metadata"]["generateName"],
            "{}-".format(ctx.parent_name),
        )

    async def test_adopt_object_clears_name(self):
        obj = {
            "metadata": {"name": "foo"},
        }
        ctx = unittest.mock.Mock([
            "parent_api_version",
            "parent_name",
            "parent_kind",
            "parent_uid",
            "base_label_match",
            "instance",
        ])
        ctx.parent = {"metadata": {"uid": str(uuid.uuid4())}}
        ctx.base_label_match.return_value = {}

        await self.cdspr.adopt_object(ctx, obj)

        self.assertNotIn(
            "name",
            obj["metadata"],
        )

    async def test__needs_update_returns_true_on_label_change(self):
        self.assertTrue(self.cdspr._needs_update(
            {
                "metadata": {
                    "labels": {
                        "a": "b",
                    },
                },
                "spec": {},
            },
            {
                "metadata": {
                    "labels": {
                        "a": "c",
                    },
                },
                "spec": {},
            },
        ))

    async def test__make_body_uses_make_pod(self):
        ctx = unittest.mock.Mock(["instance", "parent_spec"])

        with contextlib.ExitStack() as stack:
            make_pod = stack.enter_context(unittest.mock.patch(
                "yaook.op.cds.resources.make_pod",
            ))
            make_pod.return_value = unittest.mock.sentinel.pod

            result = await self.cdspr._make_body(
                ctx,
                unittest.mock.sentinel.deps,
            )

        make_pod.assert_called_once_with(
            ctx.parent_spec,
            ctx.instance,
        )

        self.assertEqual(result, unittest.mock.sentinel.pod)


class TestCDSPods(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.component = str(uuid.uuid4())
        self.wrapped = SingleObjectMock()
        with unittest.mock.patch(
                "yaook.op.cds.resources.CDSPodResource") as CDSPodResource:
            CDSPodResource.return_value = self.wrapped
            self.cdsp = cds_resources.CDSPods(component=self.component)

        self.ctx = unittest.mock.Mock(["logger"])

    def _make_node_affinity(self, for_node):
        return kclient.V1NodeAffinity(
            required_during_scheduling_ignored_during_execution=kclient.
            V1NodeSelector(
                node_selector_terms=[
                    kclient.V1NodeSelectorTerm(
                        match_fields=[
                            kclient.V1NodeSelectorRequirement(
                                key="metadata.name",
                                operator="In",
                                values=[for_node],
                            )
                        ],
                    ),
                ],
            ),
        )

    async def test__get_current_instances_uses_superclass_and_regroups_by_target_node(self):  # NOQA
        target_nodes = {
            unittest.mock.sentinel.pod1: "node-1",
            unittest.mock.sentinel.pod2: "node-1",
            unittest.mock.sentinel.pod3: "node-2",
            unittest.mock.sentinel.pod4: "node-3",
            unittest.mock.sentinel.pod5: "node-2",
            unittest.mock.sentinel.pod6: None,
        }

        def get_pod_target_node_impl(pod):
            return target_nodes[pod]

        with contextlib.ExitStack() as stack:
            get_current_instances = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.instancing.StatefulInstancedResource."
                "_get_current_instances",
            ))
            get_current_instances.return_value = (
                {
                    unittest.mock.sentinel.foo: [
                        instancing.ResourceInfo(
                            unittest.mock.sentinel.metadata1,
                            unittest.mock.sentinel.pod1,
                        ),
                        instancing.ResourceInfo(
                            unittest.mock.sentinel.metadata6,
                            unittest.mock.sentinel.pod6,
                        ),
                    ],
                    unittest.mock.sentinel.bar: [
                        instancing.ResourceInfo(
                            unittest.mock.sentinel.metadata2,
                            unittest.mock.sentinel.pod2,
                        ),
                        instancing.ResourceInfo(
                            unittest.mock.sentinel.metadata3,
                            unittest.mock.sentinel.pod3,
                        ),
                    ],
                },
                [
                    instancing.ResourceInfo(
                        unittest.mock.sentinel.metadata4,
                        unittest.mock.sentinel.pod4,
                    ),
                    instancing.ResourceInfo(
                        unittest.mock.sentinel.metadata5,
                        unittest.mock.sentinel.pod5,
                    ),
                ],
            )

            get_pod_target_node = stack.enter_context(unittest.mock.patch(
                "yaook.op.cds.resources.get_pod_target_node",
            ))
            get_pod_target_node.side_effect = get_pod_target_node_impl

            assignment, leftovers = await self.cdsp._get_current_instances(
                unittest.mock.sentinel.ctx,
            )

        get_current_instances.assert_awaited_once_with(
            unittest.mock.sentinel.ctx,
        )

        self.assertDictEqual(
            assignment,
            {
                "node-1": [
                    instancing.ResourceInfo(
                        unittest.mock.sentinel.metadata1,
                        unittest.mock.sentinel.pod1,
                    ),
                    instancing.ResourceInfo(
                        unittest.mock.sentinel.metadata2,
                        unittest.mock.sentinel.pod2,
                    ),
                ],
                "node-2": [
                    instancing.ResourceInfo(
                        unittest.mock.sentinel.metadata3,
                        unittest.mock.sentinel.pod3,
                    ),
                    instancing.ResourceInfo(
                        unittest.mock.sentinel.metadata5,
                        unittest.mock.sentinel.pod5,
                    ),
                ],
                "node-3": [
                    instancing.ResourceInfo(
                        unittest.mock.sentinel.metadata4,
                        unittest.mock.sentinel.pod4,
                    ),
                ],
            },
        )

        self.assertCountEqual(
            leftovers,
            [
                instancing.ResourceInfo(
                    unittest.mock.sentinel.metadata6,
                    unittest.mock.sentinel.pod6,
                ),
            ]
        )

    def test__get_spec_state_returns_stale_if_annotation_is_missing(self):
        self.assertEqual(
            self.cdsp._get_spec_state(
                unittest.mock.sentinel.ctx,
                unittest.mock.sentinel.intent,
                kclient.V1Pod()
            ),
            instancing.ResourceSpecState.STALE,
        )
        self.assertEqual(
            self.cdsp._get_spec_state(
                unittest.mock.sentinel.ctx,
                unittest.mock.sentinel.intent,
                kclient.V1Pod(
                    metadata=kclient.V1ObjectMeta()
                ),
            ),
            instancing.ResourceSpecState.STALE,
        )
        self.assertEqual(
            self.cdsp._get_spec_state(
                unittest.mock.sentinel.ctx,
                unittest.mock.sentinel.intent,
                kclient.V1Pod(
                    metadata=kclient.V1ObjectMeta(
                        annotations={},
                    )
                ),
            ),
            instancing.ResourceSpecState.STALE,
        )
        self.assertEqual(
            self.cdsp._get_spec_state(
                unittest.mock.sentinel.ctx,
                unittest.mock.sentinel.intent,
                kclient.V1Pod(
                    metadata=kclient.V1ObjectMeta(
                        annotations={
                            "foo": "bar",
                        },
                    )
                ),
            ),
            instancing.ResourceSpecState.STALE,
        )

    def test__get_spec_state_returns_stale_if_annotation_does_not_match_hash(self):  # NOQA
        intent = {
            "metadata": {
                "annotations": {
                    cds_resources.HASH_ANNOTATION: unittest.mock.sentinel.foo
                }
            }
        }

        with contextlib.ExitStack() as stack:
            hash_pod = stack.enter_context(unittest.mock.patch(
                "yaook.op.cds.resources.hash_pod",
            ))

            result = self.cdsp._get_spec_state(
                self.ctx,
                intent,
                kclient.V1Pod(
                    metadata=kclient.V1ObjectMeta(
                        annotations={
                            cds_resources.HASH_ANNOTATION:
                            unittest.mock.sentinel.bar
                        },
                    )
                ),
            )

        hash_pod.assert_not_called()

        self.assertEqual(
            result,
            instancing.ResourceSpecState.STALE,
        )

    def test__get_spec_state_returns_up_to_date_if_annotation_matches_hash(self):  # NOQA
        intent = {
            "metadata": {
                "annotations": {
                    cds_resources.HASH_ANNOTATION: unittest.mock.sentinel.foo
                }
            }
        }

        with contextlib.ExitStack() as stack:
            hash_pod = stack.enter_context(unittest.mock.patch(
                "yaook.op.cds.resources.hash_pod",
            ))

            result = self.cdsp._get_spec_state(
                self.ctx,
                intent,
                kclient.V1Pod(
                    metadata=kclient.V1ObjectMeta(
                        annotations={
                            cds_resources.HASH_ANNOTATION:
                            unittest.mock.sentinel.foo
                        },
                    )
                ),
            )

        hash_pod.assert_not_called()

        self.assertEqual(
            result,
            instancing.ResourceSpecState.UP_TO_DATE,
        )

    async def test_get_target_instances_extracts_node_list_from_spec(self):
        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {
            "targetNodes": [
                unittest.mock.sentinel.node1,
                unittest.mock.sentinel.node2
            ],
        }

        self.assertEqual(
            await self.cdsp.get_target_instances(ctx),
            {
                unittest.mock.sentinel.node1: None,
                unittest.mock.sentinel.node2: None
            },
        )

    def test__get_run_state_returns_starting_if_pod_is_not_assigned_to_node(self):  # NOQA
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name=None,
                affinity=kclient.V1Affinity(
                    node_affinity=self._make_node_affinity("node-1"),
                ),
            )
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.STARTING,
        )

    def test__get_run_state_returns_deletion_required_if_pod_is_missing_target_node(self):  # NOQA
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name="some-node",
            )
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.DELETION_REQUIRED,
        )

        node_affinity = kclient.V1NodeAffinity(
            required_during_scheduling_ignored_during_execution=kclient.
            V1NodeSelector(
                node_selector_terms=[
                    kclient.V1NodeSelectorTerm(
                        match_fields=[
                            kclient.V1NodeSelectorRequirement(
                                key="foo",
                                operator="In",
                                values=["bar"],
                            ),
                        ]
                    ),
                ]
            ),
        )
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name="some-node",
                affinity=kclient.V1Affinity(
                    node_affinity=node_affinity,
                ),
            )
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.DELETION_REQUIRED,
        )

    def test__get_run_state_returns_deletion_required_if_pod_is_missing_target_node_and_current_node(self):  # NOQA
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name=None,
            )
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.DELETION_REQUIRED,
        )

    def test__get_run_state_returns_deletion_required_if_target_node_mismatches_current_node(self):  # NOQA
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name="node-2",
                affinity=kclient.V1Affinity(
                    node_affinity=self._make_node_affinity("node-1"),
                ),
            )
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.DELETION_REQUIRED,
        )

    def test__get_run_state_returns_deletion_required_if_target_node_mismatches_current_node_and_deleting(self):  # NOQA
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(
                deletion_timestamp=unittest.mock.sentinel.foo,
            ),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name="node-2",
                affinity=kclient.V1Affinity(
                    node_affinity=self._make_node_affinity("node-1"),
                ),
            )
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.DELETION_REQUIRED,
        )

    def test__get_run_state_returns_deletion_required_if_phase_is_failed(self):
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name="node-1",
                affinity=kclient.V1Affinity(
                    node_affinity=self._make_node_affinity("node-1"),
                ),
            ),
            status=kclient.V1PodStatus(
                phase="Failed",
            ),
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.DELETION_REQUIRED,
        )

    def test__get_run_state_returns_ready_if_phase_is_succeeded(self):
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name="node-1",
                affinity=kclient.V1Affinity(
                    node_affinity=self._make_node_affinity("node-1"),
                ),
            ),
            status=kclient.V1PodStatus(
                phase="Succeeded",
            ),
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.READY,
        )

    def test__get_run_state_returns_unknown_if_phase_is_unknown(self):
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name="node-1",
                affinity=kclient.V1Affinity(
                    node_affinity=self._make_node_affinity("node-1"),
                ),
            ),
            status=kclient.V1PodStatus(
                phase="Unknown",
            ),
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.UNKNOWN,
        )

    def test__get_run_state_returns_shutting_down_if_deletion_timestamp_is_not_none_in_pending_phase(self):  # NOQA
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(
                deletion_timestamp=unittest.mock.sentinel.goner,
            ),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name="node-1",
                affinity=kclient.V1Affinity(
                    node_affinity=self._make_node_affinity("node-1"),
                ),
            ),
            status=kclient.V1PodStatus(
                phase="Pending",
            ),
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.SHUTTING_DOWN,
        )

    def test__get_run_state_returns_shutting_down_if_deletion_timestamp_is_not_none_in_running_phase(self):  # NOQA
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(
                deletion_timestamp=unittest.mock.sentinel.goner,
            ),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name="node-1",
                affinity=kclient.V1Affinity(
                    node_affinity=self._make_node_affinity("node-1"),
                ),
            ),
            status=kclient.V1PodStatus(
                phase="Running",
            ),
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.SHUTTING_DOWN,
        )

    def test__get_run_state_returns_shutting_down_if_deletion_timestamp_is_not_none_in_succeeded_phase(self):  # NOQA
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(
                deletion_timestamp=unittest.mock.sentinel.goner,
            ),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name="node-1",
                affinity=kclient.V1Affinity(
                    node_affinity=self._make_node_affinity("node-1"),
                ),
            ),
            status=kclient.V1PodStatus(
                phase="Succeeded",
            ),
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.SHUTTING_DOWN,
        )

    def test__get_run_state_returns_shutting_down_if_deletion_timestamp_is_not_none_in_unknown_phase(self):  # NOQA
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(
                deletion_timestamp=unittest.mock.sentinel.goner,
            ),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name="node-1",
                affinity=kclient.V1Affinity(
                    node_affinity=self._make_node_affinity("node-1"),
                ),
            ),
            status=kclient.V1PodStatus(
                phase="Unknown",
            ),
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.SHUTTING_DOWN,
        )

    def test__get_run_state_returns_shutting_down_if_deletion_timestamp_is_not_none_in_failed_phase(self):  # NOQA
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(
                deletion_timestamp=unittest.mock.sentinel.goner,
            ),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name="node-1",
                affinity=kclient.V1Affinity(
                    node_affinity=self._make_node_affinity("node-1"),
                ),
            ),
            status=kclient.V1PodStatus(
                phase="Failed",
            ),
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.SHUTTING_DOWN,
        )

    def test__get_run_state_returns_starting_if_running_but_not_ready_yet(self):  # NOQA
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name="node-1",
                affinity=kclient.V1Affinity(
                    node_affinity=self._make_node_affinity("node-1"),
                ),
            ),
            status=kclient.V1PodStatus(
                phase="Running",
                conditions=[
                    kclient.V1PodCondition(
                        type="Ready",
                        status="False",
                    ),
                ],
            ),
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.STARTING,
        )

    def test__get_run_state_returns_starting_if_running_but_ready_is_unknown(self):  # NOQA
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name="node-1",
                affinity=kclient.V1Affinity(
                    node_affinity=self._make_node_affinity("node-1"),
                ),
            ),
            status=kclient.V1PodStatus(
                phase="Running",
                conditions=[
                    kclient.V1PodCondition(
                        type="Ready",
                        status="Unknown",
                    ),
                ],
            ),
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.STARTING,
        )

    def test__get_run_state_returns_ready_if_running_and_ready(self):  # NOQA
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name="node-1",
                affinity=kclient.V1Affinity(
                    node_affinity=self._make_node_affinity("node-1"),
                ),
            ),
            status=kclient.V1PodStatus(
                phase="Running",
                conditions=[
                    kclient.V1PodCondition(
                        type="Ready",
                        status="True",
                    ),
                ],
            ),
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.READY,
        )

    def test__get_run_state_returns_starting_if_pending_and_ready(self):
        pod = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(),
            spec=kclient.V1PodSpec(
                containers=[],
                node_name="node-1",
                affinity=kclient.V1Affinity(
                    node_affinity=self._make_node_affinity("node-1"),
                ),
            ),
            status=kclient.V1PodStatus(
                phase="Pending",
                conditions=[
                    kclient.V1PodCondition(
                        type="Ready",
                        status="True",
                    ),
                ],
            ),
        )

        self.assertEqual(
            self.cdsp._get_run_state(self.ctx, pod),
            instancing.ResourceRunState.STARTING,
        )

    def test_get_listeners_subscribes_to_pod_events_as_broadcast(self):
        self.assertIn(
            context.KubernetesListener(
                api_group="", version="v1", plural="pods",
                listener=self.cdsp._handle_pod_event,
                broadcast=True),
            self.cdsp.get_listeners(),
        )

    def test__handle_pod_event_returns_true_if_added_with_label(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.ADDED
        ev.object_ = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(
                labels={context.LABEL_CDS_CONTROLLER_UID: "the-uid"},
            )
        )
        ctx = unittest.mock.Mock([])
        ctx.parent_uid = "the-uid"

        self.assertEqual(
            self.cdsp._handle_pod_event(ctx, ev),
            True,
        )

    def test__handle_pod_event_returns_true_if_modified_with_label(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.object_ = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(
                labels={context.LABEL_CDS_CONTROLLER_UID: "the-uid"},
            )
        )
        ctx = unittest.mock.Mock([])
        ctx.parent_uid = "the-uid"

        self.assertEqual(
            self.cdsp._handle_pod_event(ctx, ev),
            True,
        )

    def test__handle_pod_event_returns_true_if_deleted_with_label(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.DELETED
        ev.object_ = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(
                labels={context.LABEL_CDS_CONTROLLER_UID: "the-uid"},
            )
        )
        ctx = unittest.mock.Mock([])
        ctx.parent_uid = "the-uid"

        self.assertEqual(
            self.cdsp._handle_pod_event(ctx, ev),
            True,
        )

    def test__handle_pod_event_returns_false_if_deleted_without_label(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.DELETED
        ev.object_ = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(
                labels={},
            )
        )
        ctx = unittest.mock.Mock([])
        ctx.parent_uid = "the-uid"

        self.assertEqual(
            self.cdsp._handle_pod_event(ctx, ev),
            False,
        )

    def test__handle_pod_event_returns_false_if_modified_without_label(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.object_ = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(
                labels={},
            )
        )
        ctx = unittest.mock.Mock([])
        ctx.parent_uid = "the-uid"

        self.assertEqual(
            self.cdsp._handle_pod_event(ctx, ev),
            False,
        )

    def test__handle_pod_event_returns_false_if_added_without_label(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.ADDED
        ev.object_ = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(
                labels={},
            )
        )
        ctx = unittest.mock.Mock([])
        ctx.parent_uid = "the-uid"

        self.assertEqual(
            self.cdsp._handle_pod_event(ctx, ev),
            False,
        )

    def test__handle_pod_event_returns_false_if_added_with_wrong_label(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.ADDED
        ev.object_ = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(
                labels={context.LABEL_CDS_CONTROLLER_UID: "wrong-uid"},
            )
        )
        ctx = unittest.mock.Mock([])
        ctx.parent_uid = "the-uid"

        self.assertEqual(
            self.cdsp._handle_pod_event(ctx, ev),
            False,
        )

    def test__handle_pod_event_returns_false_if_modified_with_wrong_label(self):  # NOQA
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.object_ = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(
                labels={context.LABEL_CDS_CONTROLLER_UID: "wrong-uid"},
            )
        )
        ctx = unittest.mock.Mock([])
        ctx.parent_uid = "the-uid"

        self.assertEqual(
            self.cdsp._handle_pod_event(ctx, ev),
            False,
        )

    def test__handle_pod_event_returns_false_if_deleted_with_wrong_label(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.DELETED
        ev.object_ = kclient.V1Pod(
            metadata=kclient.V1ObjectMeta(
                labels={context.LABEL_CDS_CONTROLLER_UID: "wrong-uid"},
            )
        )
        ctx = unittest.mock.Mock([])
        ctx.parent_uid = "the-uid"

        self.assertEqual(
            self.cdsp._handle_pod_event(ctx, ev),
            False,
        )

    def test__get_max_unavailable_defaults_to_1(self):
        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {}

        self.assertEqual(
            self.cdsp._get_max_unavailable(ctx),
            1,
        )

        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {
            "updateStrategy": {},
        }

        self.assertEqual(
            self.cdsp._get_max_unavailable(ctx),
            1,
        )

        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {
            "updateStrategy": {
                "type": "rollingUpdate",
            },
        }

        self.assertEqual(
            self.cdsp._get_max_unavailable(ctx),
            1,
        )

        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {
            "updateStrategy": {
                "type": "rollingUpdate",
                "rollingUpdate": {},
            },
        }

        self.assertEqual(
            self.cdsp._get_max_unavailable(ctx),
            1,
        )

    def test__get_max_unavailable_extracts_max_unavailable_from_rolling_upgrade(self):  # NOQA
        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {
            "updateStrategy": {
                "type": "rollingUpdate",
                "rollingUpdate": {
                    "maxUnavailable": 10,
                },
            },
        }

        self.assertEqual(
            self.cdsp._get_max_unavailable(ctx),
            10,
        )
