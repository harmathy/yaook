#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest
import unittest.mock

import yaook.op.common as common


class Testextract_labeled_configs(unittest.TestCase):
    def test_returns_configs_which_match_given_labelset(self):
        item1 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l1": "v1",
                },
            }],
            "instance": 1,
        }
        item2 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l1": "v2",
                },
            }],
        }
        item3 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l2": "v1",
                },
            }],
        }
        item4 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l1": "v1",
                    "l2": "v1",
                },
            }],
        }
        item5 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l1": "v1",
                },
            }],
            "instance": 2,
        }

        input_ = [item1, item2, item3, item4, item5]

        result = common.extract_labeled_configs(
            input_,
            {
                "l1": "v1",
            }
        )

        self.assertCountEqual(
            result,
            [item1, item5],
        )

    def test_returns_configs_with_union_of_selectors(self):
        item1 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l1": "v1",
                },
            }],
            "instance": 1,
        }
        item2 = {
            "nodeSelectors": [
                {
                    "matchLabels": {
                        "l1": "v2",
                    },
                },
                {
                    "matchLabels": {
                        "l1": "v1",
                    },
                }
            ],
        }
        item3 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l2": "v1",
                },
            }],
        }
        item4 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l1": "v1",
                    "l2": "v1",
                },
            }],
        }
        item5 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l1": "v1",
                },
            }],
            "instance": 2,
        }

        input_ = [item1, item2, item3, item4, item5]

        result = common.extract_labeled_configs(
            input_,
            {
                "l1": "v1",
            }
        )

        self.assertCountEqual(
            result,
            [item1, item2, item5],
        )


class Testtranspose_config(unittest.TestCase):
    def test_collects_subkeys_in_lists(self):
        input_ = [
            {
                "key1": unittest.mock.sentinel.key1_1,
            },
            {
                "key2": unittest.mock.sentinel.key2_1,
            },
            {
                "key1": unittest.mock.sentinel.key1_2,
                "key2": unittest.mock.sentinel.key2_2,
                "key3": unittest.mock.sentinel.ignored,
            },
            {
                "key3": unittest.mock.sentinel.ignored,
            },
        ]

        result = common.transpose_config(
            input_,
            ["key1", "key2"],
        )

        self.assertEqual(
            result,
            {
                "key1": [
                    unittest.mock.sentinel.key1_1,
                    unittest.mock.sentinel.key1_2,
                ],
                "key2": [
                    unittest.mock.sentinel.key2_1,
                    unittest.mock.sentinel.key2_2,
                ],
            }
        )

    def test_contains_empty_list_for_nonexistent_key(self):
        input_ = [
            {
                "key1": unittest.mock.sentinel.key1_1,
            },
        ]

        result = common.transpose_config(
            input_,
            ["key4"],
        )

        self.assertEqual(
            result,
            {
                "key4": [],
            }
        )


class Testget_node_labels_from_instance(unittest.IsolatedAsyncioTestCase):
    @unittest.mock.patch("kubernetes_asyncio.client.CoreV1Api")
    async def test_extracts_node_labels_from_api(
            self,
            CoreV1Api):
        node = unittest.mock.Mock(["metadata"])
        node.metadata = unittest.mock.Mock(["labels"])
        ctx = unittest.mock.Mock(["api_client", "instance"])
        v1 = unittest.mock.Mock(["read_node"])
        v1.read_node = unittest.mock.AsyncMock()
        v1.read_node.return_value = node
        CoreV1Api.return_value = v1

        result = await common.get_node_labels_from_instance(ctx)

        self.assertEqual(result, node.metadata.labels)

    @unittest.mock.patch("kubernetes_asyncio.client.CoreV1Api")
    async def test_uses_api_client_from_context(
            self,
            CoreV1Api):
        node = unittest.mock.Mock(["metadata"])
        node.metadata = unittest.mock.Mock(["labels"])
        ctx = unittest.mock.Mock(["api_client", "instance"])
        v1 = unittest.mock.Mock(["read_node"])
        v1.read_node = unittest.mock.AsyncMock()
        v1.read_node.return_value = node
        CoreV1Api.return_value = v1

        await common.get_node_labels_from_instance(ctx)

        CoreV1Api.assert_called_once_with(ctx.api_client)

    @unittest.mock.patch("kubernetes_asyncio.client.CoreV1Api")
    async def test_uses_instance_as_node_name(
            self,
            CoreV1Api):
        node = unittest.mock.Mock(["metadata"])
        node.metadata = unittest.mock.Mock(["labels"])
        ctx = unittest.mock.Mock(["api_client", "instance"])
        v1 = unittest.mock.Mock(["read_node"])
        v1.read_node = unittest.mock.AsyncMock()
        v1.read_node.return_value = node
        CoreV1Api.return_value = v1

        await common.get_node_labels_from_instance(ctx)

        v1.read_node.assert_called_once_with(ctx.instance)


class Testpublish_endpoint(unittest.TestCase):
    async def test_publish_endpoint_defaults_to_true(
            self):
        self.assertTrue(common.publish_endpoint())

    async def test_publish_endpoint_returns_false(
            self):
        ctx = {
            "parent_spec": {
                "api": {
                    "publishEndpoint": False
                }
            }
        }

        return_value = common.publish_endpoint(ctx)
        self.assertFalse(return_value)


class Testcreate_ingress(unittest.TestCase):
    async def test_publish_endpoint_defaults_to_true(
            self):
        self.assertTrue(common.create_ingress())

    async def test_create_ingress_returns_false(
            self):
        ctx = {
            "parent_spec": {
                "api": {
                    "ingress": {
                        "createIngress": False
                    }
                }
            }
        }

        return_value = common.publish_endpoint(ctx)
        self.assertFalse(return_value)


class TestOVSDBSchemaParams(unittest.TestCase):
    _members = dict(
        port=int,
        stanza=str,
        access_service=str,
        db_name=str,
        db_global=str
    )

    def test_member_access(self):
        test_data = [
            [20, "i2", 'as', "DB", "G"],
            [-20, "i5", 'ab', "DB", "G"],
            ["-20", "i5", '', "DB", {'': 1}],
            [None, None, None, None, None],
        ]

        for data_set in test_data:
            keys = list(self._members.keys())
            params = {
                key: value for key, value in
                zip(keys, data_set)
            }

            subject = common.OVSDBSchemaParams(**params)

            for member in keys:
                with self.subTest(data_set=data_set, member=member):
                    self.assertEqual(
                        params[member],
                        getattr(subject, member),
                        f"the member {member} gives access to the "
                        f"provided parameter"
                    )


class TestOVSDBSchema(unittest.TestCase):
    defined_schemas = 2

    def test_cardinality(self):
        self.assertEqual(
            self.defined_schemas, len(common.OVSDBSchema),
            f"{common.OVSDBSchema.__name__} has {self.defined_schemas} members"
        )

    def test_access_by_label(self):
        labels = [
            ('northbound', common.OVSDBSchema.northbound),
            ('southbound', common.OVSDBSchema.southbound),
        ]

        for label, item in labels:
            with self.subTest(
                    label=label,
                    method=common.OVSDBSchema.schema_by_label):
                self.assertEqual(
                    item, common.OVSDBSchema.schema_by_label(label),
                    f'label {label} yields correct schema'
                )
            with self.subTest(
                    label=label,
                    method=common.OVSDBSchema.schema_params_by_label):
                self.assertEqual(
                    item.value,
                    common.OVSDBSchema.schema_params_by_label(label),
                    f'label {label} yields correct schema parameters'
                )

    def test_invalid_label(self):

        invalid_label = "This is not a valid label!"

        for method in [common.OVSDBSchema.schema_by_label,
                       common.OVSDBSchema.schema_params_by_label]:
            with self.subTest(
                    method=method,
                    msg=f'method "{method}" raises '
                        f'{common.InvalidOVSDBSchema.__name__} exception'):
                self.assertRaises(
                    common.InvalidOVSDBSchema,
                    method,
                    invalid_label,
                )
