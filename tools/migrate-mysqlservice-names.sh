#!/bin/bash

# This script lets you migrate mysqlservices between different names.
# The migration is running completely online without any disruption.
# It will also rename all affected statefulsets, deployments, certificates, etc.
# However it requires multiple restarts of all databases and the respective services.
# Use at your own risk.
#
# The script needs to be run in multiple different steps.
# You need to ensure that the order of the steps is followed as well as that you wait for all necessary prerequisites.
# Not following these steps can lead to service outages and if you mess up completely to data loss.
#
# The script is invoked using ./migrate-mysqlservice-names.sh <old-name> <new-name> <step-id>.
# You can get the old name via `kubectl get mysqlservices`.
# The new name can be choosen by you, but most probably you replace a random suffix.
# Like old `nova-cell1-abc123` to new `mynovad-nova-cell1`
#
# We suggest you do each openstack service one-by-one.
#
# For nova you should do all databases in parallel.
# So first step 1 on all of them, once that is ready step 2 and so forth.
#
##############################################################################
#
# BEFORE STEP 1
#
# Ensure the operator is updated to at least the version including this script.
# Do not use an older version and copy just this script out, this will cause errors in the migration.
# Ensure that each and every yaook resource in your cluster has been reconciled completely after the update.
#
##############################################################################
#
# RUN STEP 1
# ./migrate-mysqlservice-names.sh <old-name> <new-name> 1
#
##############################################################################
#
# AFTER STEP 1 BUT BEFORE STEP 2
#
# Ensure that all client connections are migrated to the newly spawend haproxies.
# This can generally be assumed after connection_recycle_time which is 5 min.
#
# Also ensure the old database statefulset has completed a rolling restart of all of its pods.
#
##############################################################################
#
# RUN STEP 2
# ./migrate-mysqlservice-names.sh <old-name> <new-name> 2
#
##############################################################################
#
# AFTER STEP 2 BUT BEFORE STEP 3
#
# Ensure that the new database is up and running with all replicas.
# Also ensure that it is recognized in all haproxy instances.
#
##############################################################################
#
# RUN STEP 3
#
# IF you are migrating nova and do all databases at the same time you will need to adjust this script.
# Scroll down to step 3 and adjust it as described in the comment there.
#
# ./migrate-mysqlservice-names.sh <old-name> <new-name> 3
#
##############################################################################
#
# AFTER STEP 3 BUT BEFORE STEP 4
#
# Ensure that the parent resource has completely reconciled (so if you do the keystone db, check the keystonedeployment resource).
# Also ensure that any reference to the old mysqlservice is gone.
# This should generally happen automatically in yaook, if you use mysqlservices from somewhere else as well, you need to check that on your own
#
##############################################################################
#
# RUN STEP 4
#
# ./migrate-mysqlservice-names.sh <old-name> <new-name> 4
#
##############################################################################
#
# AFTER STEP 4
#
# You can now delete the old pvcs with the old names.
# You are also done now.
#


set -euo pipefail

if [ "$#" -ne 3 ]; then
  echo "You seem to have no idea how to use this script. Please read the comments in this file"
  exit 1
fi

set -x

old_name="${1}"
new_name="${2}"
step="${3}"
new_uid=""

echo "migrating from ${old_name} to ${new_name} at step ${step}"

function map_name {
  echo "${1}" | sed -e "s|${old_name}|${new_name}|g"
}

function k {
  kubectl -n yaook $@
}

function k_get {
  k get "${1}" ${2} -o json
}

function copy {
  if [[ "${1}" == "custompath" ]]; then
    cp -f "${2}" /tmp/copyfile
  else
    k_get "${1}" "${2}" > /tmp/copyfile
  fi
  cat /tmp/copyfile | jq -r 'del(.metadata.uid, .metadata.resourceVersion, .metadata.generation)' > /tmp/copyfile2
  cat /tmp/copyfile2 | jq -r ".metadata.name = \"${3}\"" > /tmp/copyfile3
  if [[ $# -ge 4 ]]; then
    cat /tmp/copyfile3 | jq -r "${4}" > /tmp/copyfile4
  else
    cp -f /tmp/copyfile3 /tmp/copyfile4
  fi
  k apply -f /tmp/copyfile4
}

function copy_map_name {
  if [[ "${1}" == "custompath" ]]; then
    current_name=$(cat "${2}" | jq -r '.metadata.name')
  else
    current_name="${2}"
  fi
  new_res_name=$(map_name "${current_name}")
  filter=".metadata.labels.\"state.yaook.cloud/parent-name\" = \"${new_name}\" | .metadata.ownerReferences[0].name = \"${new_name}\" | .metadata.ownerReferences[0].uid = \"${new_uid}\""
  if [[ $# -ge 3 ]]; then
    filter=$(echo "${filter} | ${3}")
  fi

  copy "${1}" "${2}" "${new_res_name}" "${filter}" > /dev/null

  echo "${new_res_name}"
}

function get_cert_secret {
  getfield Certificate "${1}" '.spec.secretName'
}

function copy_cert_and_secret {
  old_secret=$(get_cert_secret "${1}");
  new_secret=$(copy_map_name secret "${old_secret}" 'del(.metadata.ownerReferences[1])')
  new_cn=$(map_name $(getfield Certificate "${1}" '.spec.commonName'))
  filter=".spec.commonName = \"${new_cn}\" | .spec.secretName = \"${new_secret}\""
  if [[ $# -ge 2 ]]; then
    filter=$(echo "${filter} | ${2}")
  fi
  copy_map_name Certificate "${1}" "${filter}"
}

function copy_cert_and_secret_with_dns_and_issuer {
  new_dns0=$(map_name $(getfield Certificate "${1}" '.spec.dnsNames[0]'))
  new_dns1=$(map_name $(getfield Certificate "${1}" '.spec.dnsNames[1]'))
  new_dns2=$(map_name $(getfield Certificate "${1}" '.spec.dnsNames[2]'))
  new_dns3=$(map_name $(getfield Certificate "${1}" '.spec.dnsNames[3]'))
  new_issuer=$(map_name $(getfield Certificate "${1}" '.spec.issuerRef.name'))
  copy_cert_and_secret "${1}" ".spec.dnsNames[0] = \"${new_dns0}\" | .spec.dnsNames[1] = \"${new_dns1}\" | .spec.dnsNames[2] = \"${new_dns2}\" | .spec.dnsNames[3] = \"${new_dns3}\" | .spec.issuerRef.name = \"${new_issuer}\""
}

function copy_cert_and_secret_with_merged_dns {
  new_dns0=$(map_name $(getfield Certificate "${1}" '.spec.dnsNames[0]'))
  new_dns1=$(map_name $(getfield Certificate "${1}" '.spec.dnsNames[1]'))
  new_dns2=$(map_name $(getfield Certificate "${1}" '.spec.dnsNames[2]'))
  new_dns3=$(map_name $(getfield Certificate "${1}" '.spec.dnsNames[3]'))
  copy_cert_and_secret "${1}" ".spec.dnsNames[4] = \"${new_dns0}\" | .spec.dnsNames[5] = \"${new_dns1}\" | .spec.dnsNames[6] = \"${new_dns2}\" | .spec.dnsNames[7] = \"${new_dns3}\""
}

function copy_issuer {
  new_secret=$(map_name $(getfield Issuer "${1}" '.spec.ca.secretName'))
  copy_map_name Issuer "${1}" ".spec.ca.secretName = \"${new_secret}\""
}

function copy_service {
  cluster_ip=$(getfield service "${1}" '.spec.clusterIP')
  filter=''
  if [[ $# -ge 2 ]]; then
    filter=$(echo "${2} |")
  fi
  if [[ "${cluster_ip}" != "None" ]]; then
    filter="${filter} del(.spec.clusterIP, .spec.clusterIPs) |"
  fi
  copy_map_name service "${1}" "${filter} .spec.selector.\"state.yaook.cloud/parent-name\" = \"${new_name}\""
}

function copy_service_with_pod_name {
  pod_name=$(map_name $(getfield service "${1}" '.spec.selector."statefulset.kubernetes.io/pod-name"'))
  copy_service "${1}" ".spec.selector.\"statefulset.kubernetes.io/pod-name\" = \"${pod_name}\""
}

function copy_deployment {
  copy_map_name deployment "${1}" "(.spec.template.spec.volumes[] | .configMap.name | select(. != null)) |= sub(\"${old_name}\"; \"${new_name}\") | (.spec.template.spec.volumes[] | .secret.secretName | select (. != null)) |= sub(\"${old_name}\"; \"${new_name}\") | .spec.selector.matchLabels.\"state.yaook.cloud/parent-name\" = \"${new_name}\" | .spec.template.metadata.labels.\"state.yaook.cloud/parent-name\" = \"${new_name}\" | .spec.paused = false"
}

function getfield {
  k_get "${1}" "${2}" | jq -r "${3}"
}

function find_resource_name {
  for i in $(k get "${1}" -l "${2}" -o name | cut -d / -f 2); do
    echo $i;
    return;
  done
  exit 1
}

# generally gathering some information
# if the old_name is no longer there we probably already got rid of it
if k get mysqlservice "${old_name}" > /dev/null; then
  parent_name=$(getfield mysqlservice "${old_name}" ".metadata.labels.\"state.yaook.cloud/parent-name\"")
  parent_plural=$(getfield mysqlservice "${old_name}" ".metadata.labels.\"state.yaook.cloud/parent-plural\"")
else
  parent_name=$(getfield mysqlservice "${new_name}" ".metadata.labels.\"state.yaook.cloud/parent-name\"")
  parent_plural=$(getfield mysqlservice "${new_name}" ".metadata.labels.\"state.yaook.cloud/parent-plural\"")
fi

# preparation until the new proxies are available.
if [[ "${step}" == "1" ]]; then
  # lets first pause everything that could create chaos
  k annotate "${parent_plural}" "${parent_name}" "state.yaook.cloud/pause=my-db-will-get-a-new-name"
  k annotate mysqlservice "${old_name}" "state.yaook.cloud/pause=i-will-get-a-new-name"
  # and the db proxy
  k patch deployment "${old_name}-prx" --patch '{"spec":{"paused":true}}'

  # also we add a finalizer to the old mysqlservice to ensure that the parent
  # can later not delete it
  k patch mysqlservice "${old_name}" --type=merge --patch '{"metadata":{"finalizers":["this.db.will.be.renamed"]}}'

  # now we create the new mysqlservice in paused state, so that we can patch owner
  # references already. It is paused already above.
  copy mysqlservice "${old_name}" "${new_name}"
  new_uid=$(getfield mysqlservice "${new_name}" '.metadata.uid')

  # copy the current replication ca
  copy_cert_and_secret "${old_name}-db-galeraca"
  copy_issuer "${old_name}-db-galeraca"

  # copy the replicate cert and the monitoring certs
  copy_cert_and_secret_with_dns_and_issuer "${old_name}-db-galera"
  copy_cert_and_secret_with_dns_and_issuer "${old_name}-db-prx-metrics"
  copy_cert_and_secret_with_dns_and_issuer "${old_name}-db-galera-exporter"

  # build a new temporary frontend certificate that contains the old and new dns names
  copy_cert_and_secret_with_merged_dns "${old_name}-db-frontend"

  # waiting to let cert-manager do its magic
  sleep 3

  # create a new replication ca containing the old and the new replication ca
  old_ca=$(getfield secret $(get_cert_secret "${old_name}-db-galeraca") '.data."ca.crt"' | base64 -d)
  new_ca=$(getfield secret $(get_cert_secret "${new_name}-db-galeraca") '.data."ca.crt"' | base64 -d)
  echo "${old_ca}" > /tmp/ca.crt
  echo "${new_ca}" >> /tmp/ca.crt
  k delete configmap "${new_name}-temp-replication-ca-combined" || true
  k create configmap "${new_name}-temp-replication-ca-combined" --from-file=ca.crt=/tmp/ca.crt

  # copy the db admin credentials
  copy_map_name secret "${old_name}-db-creds"

  # copy the services over
  copy_service "${old_name}"
  copy_service "${old_name}-db-bare"
  copy_service "${old_name}-db-rdy"
  copy_service "${old_name}-db-frontend"
  for i in $(k get service -l "state.yaook.cloud/component=database_pod_services,state.yaook.cloud/parent-name=${old_name}" -o name | cut -d / -f 2); do
    copy_service_with_pod_name "${i}"
  done

  # create a new proxy config pointing to the old and new services
  haproxy_configmap=$(find_resource_name configmap "state.yaook.cloud/component=haproxy_config,state.yaook.cloud/parent-name=${old_name}")
  k_get configmap "${haproxy_configmap}" > /tmp/haproxyconfigmap
  current_config=$(getfield configmap "${haproxy_configmap}" '.data."haproxy.cfg"')
  new_config=$(map_name "$(echo "${current_config}" | grep "${old_name}")")
  current_config="${current_config}"$'\n'"${new_config}"
  echo "${current_config}" > /tmp/haproxyconfig
  cat /tmp/haproxyconfigmap | jq -r --rawfile configcontent /tmp/haproxyconfig '.data."haproxy.cfg" = $configcontent' > /tmp/haproxyconfigmap2
  copy_map_name "custompath" "/tmp/haproxyconfigmap2"

  # create new haproxies
  copy_deployment "${old_name}-prx"

  # remap old frontend port to new haproxies
  k patch service "${old_name}" --patch "{\"spec\":{\"selector\":{\"state.yaook.cloud/parent-name\":\"${new_name}\"}}}"
  k patch service "${old_name}-db-frontend" --patch "{\"spec\":{\"selector\":{\"state.yaook.cloud/parent-name\":\"${new_name}\"}}}"

  # update the old databases with the new replication ca
  frontend_cert_secret=$(find_resource_name secret "state.yaook.cloud/component=frontend_certificate_secret,state.yaook.cloud/parent-name=${new_name}")
  k patch statefulset "${old_name}-db" --type=json -p="[{\"op\":\"remove\",\"path\":\"/spec/template/spec/volumes/9/projected/sources/1/secret/items/2\"},{\"op\":\"add\",\"path\":\"/spec/template/spec/volumes/9/projected/sources/3\",\"value\":{\"configMap\":{\"name\":\"${new_name}-temp-replication-ca-combined\",\"items\":[{\"key\":\"ca.crt\",\"path\":\"replication/ca.crt\"}]}}},{\"op\":\"replace\",\"path\":\"/spec/template/spec/volumes/9/projected/sources/0/secret/name\",\"value\":\"${frontend_cert_secret}\"}]"

  rm -f "/tmp/oldsfs-${new_name}"
fi

# after this step the new databases are running
# BEFORE RUNNING THIS STEP: you need to ensure
# that all client connections are migrated to the new haproxies.
# Also the old database statefulset must have completed a rolling restart.
if [[ "${step}" == "2" ]]; then
  new_uid=$(getfield mysqlservice "${new_name}" '.metadata.uid')

  # lets pause the old mysql statefulset before something bad can start to happen
  k patch statefulset "${old_name}-db" --patch '{"spec":{"updateStrategy":{"type":"OnDelete","rollingUpdate":null}}}'

  # stop the old proxies
  k patch deployment "${old_name}-prx" --patch '{"spec":{"paused":false,"replicas":0}}'

  # now get rid of the old db statefulset without deleting the pods
  # we also need to store the statefulset to afterwards apply it again with
  # the new names
  if [ ! -f /tmp/oldsfs ]; then
    k_get statefulset "${old_name}-db" > "/tmp/oldsfs-${new_name}"
  fi
  k delete statefulset "${old_name}-db" --cascade=orphan || true

  # patch the existing pods to match the future statefulset
  # this causes the proxy to no longer find the services, but it only checks every 30 seconds
  # and existing connections will continue to work. We just hope therefor that
  # we are fast enough then with the next step
  for i in $(k get pod -l "state.yaook.cloud/component=database,state.yaook.cloud/parent-name=${old_name}" -o name | cut -d / -f 2); do
    k patch pod "${i}" --patch "{\"metadata\":{\"labels\":{\"state.yaook.cloud/parent-name\":\"${new_name}\"}}}"
  done

  # patch the existing services
  for i in $(k get service -l "state.yaook.cloud/component=database_pod_services,state.yaook.cloud/parent-name=${old_name}" -o name | cut -d / -f 2); do
    k patch service "${i}" --patch "{\"spec\":{\"selector\":{\"state.yaook.cloud/parent-name\":\"${new_name}\"}}}"
  done
  k patch service "${old_name}-db-bare" --patch "{\"spec\":{\"selector\":{\"state.yaook.cloud/parent-name\":\"${new_name}\"}}}"
  # now the proxy should be happy again

  # we now reenable the new statefulset which should find the db pods based on
  # labels and trigger updates to all of them. Hopefully this does not cause downtimes :)
  #
  # we do not patch the following things here, since they are the same anyway.
  # The first real operator run will fix them then for us:
  # * the db configmap
  # * the ca certificates
  # * the exporter config
  #
  # We explicitly do not patch the galera cluster name since that is basically
  # not modifyable. The operator has logic to not ever change this value
  exporter_cert_secret=$(find_resource_name secret "state.yaook.cloud/component=exporter_certificate_secret,state.yaook.cloud/parent-name=${new_name}")
  frontend_cert_secret=$(find_resource_name secret "state.yaook.cloud/component=frontend_certificate_secret,state.yaook.cloud/parent-name=${new_name}")
  replication_cert_secret=$(find_resource_name secret "state.yaook.cloud/component=replication_certificate_secret,state.yaook.cloud/parent-name=${new_name}")
  cat "/tmp/oldsfs-${new_name}" | jq -r ".spec.selector.matchLabels.\"state.yaook.cloud/parent-name\" = \"${new_name}\" | .spec.serviceName = \"${new_name}-db-bare\" | .spec.template.metadata.labels.\"state.yaook.cloud/parent-name\" = \"${new_name}\" | .spec.template.spec.containers[0].env[5].valueFrom.secretKeyRef.name = \"${new_name}-db-creds\" | .spec.template.spec.containers[0].env[8].valueFrom.secretKeyRef.name = \"${new_name}-db-creds\" | .spec.template.spec.containers[1].env[2].valueFrom.secretKeyRef.name = \"${new_name}-db-creds\" | .spec.template.spec.topologySpreadConstraints[0].labelSelector.matchLabels.\"state.yaook.cloud/parent-name\" = \"${new_name}\" | .spec.template.spec.volumes[4].secret.secretName = \"${exporter_cert_secret}\" | .spec.template.spec.volumes[9].projected.sources[0].secret.name = \"${frontend_cert_secret}\" | .spec.template.spec.volumes[9].projected.sources[1].secret.name = \"${replication_cert_secret}\" | .spec.template.spec.volumes[9].projected.sources[2].secret.name = \"${exporter_cert_secret}\" | .spec.volumeClaimTemplates[0].metadata.labels.\"state.yaook.cloud/parent-name\" = \"${new_name}\"" > "/tmp/newsfs-${new_name}"
  copy_map_name "custompath" "/tmp/newsfs-${new_name}"

  # now lets pray that this will all work
  k patch statefulset "${new_name}-db" --patch '{"spec":{"updateStrategy":{"type":"RollingUpdate"}}}'

fi

# after this step all old databases are cleaned up
# BEFORE RUNNING THIS STEP: you need to ensure
# that the new database is up and running and recognized in haproxy
#
# IF you do nova and do all databases at the same time, comment out the last
# annotation removal until the final db.
if [[ "${step}" == "3" ]]; then
  # cleanup the old db pods
  for i in $(k get pod -l "state.yaook.cloud/component=database,state.yaook.cloud/parent-name=${new_name}" -o name | cut -d / -f 2); do
    if [[ "${i}" != "${new_name}-db-"* ]]; then
      k delete pod "${i}"
    fi
  done

  # now lets declare this mysqlservice as healthy
  generation=$(getfield mysqlservice "${new_name}" '.metadata.generation')
  k patch mysqlservice "${new_name}" --subresource=status --type=merge --patch "{\"status\":{\"observedGeneration\":${generation},\"updatedGeneration\":${generation},\"phase\":\"Updated\",\"conditions\":[]}}"

  # and the other mysqlservice is now orphaned, so that there is no chaos
  k label mysqlservice "${old_name}" state.yaook.cloud/orphaned=""

  # and nooooow we can enable the parent resource again.
  # it should find the new mysqlservice and reconcile all the things to now point to it
  # except for the nova cell db connection strings, since they are in some strange db
  k annotate "${parent_plural}" "${parent_name}" "state.yaook.cloud/pause-"
fi

# after this step all the hacky stuff is removed and we are completely normal again
# you need to afterwards manually remove the old PVCs
# BEFORE RUNNING THIS STEP: ensure that the parent resource has completely reconciled
# and any reference to the old mysqlservice is gone (e.g. also the nova cell db creds in the api db)
if [[ "${step}" == "4" ]]; then
  k patch mysqlservice "${old_name}" --type=json -p='[{"op":"remove","path":"/metadata/finalizers"}]' || true

  k patch mysqlservice "${new_name}" --type=json -p='[{"op":"remove","path":"/metadata/finalizers"}]'
  k annotate mysqlservice "${new_name}" "state.yaook.cloud/pause-"

  k delete configmap "${new_name}-temp-replication-ca-combined" || true
fi

