Designate operation
===================

Creating DNS zones
------------------

To create a new zone, the parent zone first needs to be present inside an external authoritative DNS nameserver.
The parent zone file must contain an NS entry for the new subdomain which needs to point to the Designate nameserver address. It also requires an A entry (for IPv4) or an AAAA entry (for IPv6) which delegates the nameserver address to an IP address of the PowerDNS DNS service. 

For example, if you want Designate to be authoritative of the zone designate.yaook.cloud with ``designate-ns.yaook.cloud`` being the nameserver address and ``10.101.250.83`` being the PowerDNS DNS service IP address, the zone file of ``yaook.cloud`` could look like this:

.. code-block::

  yaook.cloud  3600      IN      SOA     designate.yaook.cloud mail.yaook.cloud 1 10800 3600 604800 3600
  designate-ns.yaook.cloud       3600    IN      A       10.101.250.83
  designate.yaook.cloud  3600    IN      NS      designate-ns.yaook.cloud.

For debugging, you can setup a separate PowerDNSService inside your cluster that handles the parent zone.

The nameserver address of Designate is set inside ``/etc/designate/pools.yaml`` and needs to be specified as follows:

.. code-block:: yaml

  apiVersion: yaook.cloud/v1
  kind: DesignateDeployment
  spec:
    nsRecords:
      - hostname: designate-ns.yaook.cloud.
        priority: 1
    ...

To create the zone with Designate, use the following command:

.. code-block:: bash

  openstack zone create designate.yaook.cloud. --email email@yaook.cloud

If everything is setup correctly, the zone state should switch to ACTIVE after a few moments.
You then can also create (multilevel) subdomains of the created zone using the same command.
