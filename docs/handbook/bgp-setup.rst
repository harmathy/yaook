BGP Setup with neutron-bgp-dragent
==================================

.. warning::
    This documentation is outdated, but is still partially valid for the ovn-bgp-agent.
    As this does not yet have a dedicated documentation page we keep the one here for now.

OpenStack neutron supports the neutron-bgp-dragent to announce customer networks to an external network.
Yaook is able to create and configure this bgp agents.

Technical Setup
---------------

Yaook assumes, the bgp agent needs to speak to the external network, to announce routes.
Therefore the agent needs to be connected to the external network.

To create this connection, Yaook creates a Pod running the neutron-bgp-dragent,
with it's own pod network namespace. A ovs port is created, attached to the ovs bridge
representing the external network. The port is connected to the bgp pod network namespace
and gets an IP address, so bgp agent can use it to communicate to the external network.

This way, we achieve connection the bgp agent with the external network and still
protect the nodes running the pods from being directly connected to the external network.

Setup and configure a bgp agent
-------------------------------

To run a bgp-dragent using Yaook, some options needs to be configured:

1. add bgp configuration at neutron deployment
2. set IP annotation to nodes running the bgp agents
3. set bgp label to nodes running the agents

add bgp configuration
^^^^^^^^^^^^^^^^^^^^^

see also :ref:`examples.neutron`

Edit your neutron deployment to have at spec the section `bgp`. Next define a
*config_key* with any name you want and define the `configTemplates` as value.
The name will be used for naming the agents.
As only one bgp agent can run one speaker (but a speaker can have many agents,
e.g. at each node agent), you need to configure many *config_keys* to run for
each speaker bgp agents.

The only required config option is the `bgpInterfaceMapping`, where the
bridge name of the ovs bridge needs to be defined, the bgp agent should be
connected to. It is the same name like one of the bridges, defined in `ovn:`
`bridge_mappings` at `l2` section.

As this bridge is mostly setup at nodes with label
`network.yaook.cloud/neutron-l3-agent: "true"`, you can also run the bgp agent
on these nodes. But it can also run on different nodes, make sure the `bridge_mappings`
exists there.

IP annotation
^^^^^^^^^^^^^

Yaook needs to know, which IP to define at the interface connected to the
external network (via ovs bridge) used for bgp agent.

Therefore an annotation needs to be set, containing the *config_key*. Value is
the IP address with subnet.

Annotation prefix: `bgp-interface-ip.network.yaook.cloud/` (At the code the
variable `ANNOTATION_BGP_INTERFACE_IP` is used)

Example: `bgp-interface-ip.network.yaook.cloud/bgpdr-agent: 10.2.4.42/24`

bgp label at nodes
^^^^^^^^^^^^^^^^^^

see also :ref:`concepts.scheduling`

Each component started by Yaook will only be scheduled on nodes with specific labels.
This is used do define, which component should run on which nodes.

For bgp set the label `network.yaook.cloud/neutron-bgp-dragent=true` (code use
the variable `NETWORK_NEUTRON_BGP_DRAGENT`)
