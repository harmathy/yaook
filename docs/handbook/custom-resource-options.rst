Common Custom Resources Options
===============================

A custom resource is an extension of the Kubernetes API that is not necessarily available in a default Kubernetes installation. 
It represents a customization of a particular Kubernetes installation.
Once a custom resource is installed, users can create and access its objects using kubectl, just as they do for built-in resources like `Pods`.

By our Custom Resources(CRs) you can define these options:
  * Database Specifications
  * Message Queue Specifications
  * Secret Injection
  * Issuer

User can find out how these individual options are defined in `deploy/cuelang/*-crd.cue` files. In these files, you can also define your own CRs options.
Examples of deployments with CRs can be found in `docs/examples`.

Database Specifications
-----------------------

With this option, user can define the specifications of the database. There are two required fields, which are `proxy` and `backup`. 
In field `proxy`, you can set the number of replicas.

In the `backup` field you can specify the exact time when the database should be backed up. This `schedule` field is required. 
Except the exact time, you can also define the `targets`, where is your database backed up. Here user must define all required fields, 
which are `endpoint` as a string, name of the `bucket`, and reference to credentials `credentialRef`. 
For now, you can use only `s3` as your target(see the example of a database specs option).
The last parameter you can set in your `backup` field is `mysqldump`, which acquires a boolean value. By default, it is set to false.

Also, user can set the size and class name of the storage by defining these two fields `storageSize` and `storageClassName`.
The last one what user can define is `mysqlConfig`, where CRD expects only the 4 keys: `mysqld`, `galera`, `client-server` or `sst`.


The following is an example of a database specs option:

.. code-block:: yaml

  database:
    replicas: 1
    storageClassName: premium-perf4-stackit
    storageSize: 8Gi
    proxy:
      replicas: 1
    backup:
      schedule: "*/5 * * * *"
      targets:
        s3:
          endpoint: https://cool.s3.endpoint
          bucket: mybucket
          addressingStyle: path
          credentialRef:
            name: backup-s3-password      
    mysqlConfig:
      mysqld:
        max_connections: 1337
        max_heap_table_size: 64M           

Message Queue Specifications
----------------------------

User can define options like the number of `replicas` and class name and size of the storage by customizing these two fields: `storageClassName`, `storageSize`.

The following is an example of a Message Queue specs option:

.. code-block:: yaml

    messageQueue:
      replicas: 1
      storageSize: 2Gi
      storageClassName: bar-class

Secret Injection
----------------

To include confidential values in the configuration you can use the field `*Secret`, where this field has the prefix `glance` or `keystone` based on the OpenStack component.
This allows use to reference an external secret that is included at a specific path in the configuration. In this option are required fields `secretName` and `items`.
In field `items` user must define `key`, which is part of the external secret, and specific `path`, where the external secret is included in the configuration.

The following is an example of a secret injection option for glance:

.. code-block:: yaml

    apiVersion: v1
    kind: Secret
    metadata:
      name: mysecret
    data:
      mykey: "SomeValueAsBase64"
    ---
    apiVersion: yaook.cloud/v1
    kind: GlanceDeployment
    metadata:
      name: glance
    spec:
      glanceSecrets:
        - secretName: mysecret
          items:
            - key: mykey
              path: /DEFAULT/transport_url

Issuer
------

Issuer is Kubernetes resource that represents certificate authorities (CAs) that are able to generate signed certificates by honoring certificate signing requests. 
All cert-manager certificates require a referenced issuer that is in a ready condition to attempt to honor the request. 

By this option, users can easily by our CRs define `issuerRef` for all certificate templates.

The following is an example of a issuer option:

.. code-block:: yaml

  issuerRef:
    name: "ca-issuer"


Ingress Specifications
----------------------

Many Yaook custom resources configure a Kubernetes `Ingress <https://kubernetes.io/docs/concepts/services-networking/ingress/>`_ to accept incoming HTTP(S) requests.

The options for these ingresses are generally available under a resource-specifically located `ingress` key, which supports the following items:

- `fqdn`: The FQDN on which the ingress should listen.
- `port`: The external port number of the ingress. This is an information which is not declarative for the Ingress, but only used to form a URL for use with other services.
- `ingressClassName`: Maps to the `ingressClassName` field on the Ingress. Please see the `Ingress Class section in the Ingress documentation <https://kubernetes.io/docs/concepts/services-networking/ingress/#ingress-class>`_ for more details.


Container Resource Management
-----------------------------

Whenever Yaook custom resources create workloads, the `containers' resources <https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/>`_ can be managed via a `resources` attribute.
These attributes have the following structure:

.. code-block:: yaml

  resources:
    container-name-1:
      limits: {}
      requests: {}
    container-name-2:
      limits: {}
      requests: {}

Please note:

- Both `limits` and `requests` are optional. Remember that only specifying `limits` will set `requests` to the same values.
- Container names are specific to each custom resource definition. Please refer to the definition or the installed resources for details.
- In general, `resources` is a sibling key to `replicas`; in most places where you can set `replicas` (for instance, within `.spec.api` in a KeystoneDeployment), you can also set `resources`.


Job Resource Management
-----------------------

The resources for containers in jobs and cronjobs can also be specified via a top-level `jobResources` attribute. Here the job name is used as the key.

.. code-block:: yaml

  jobResources:
    job-name-1-job:
      limits: {}
      requests: {}
    job-name-2-job:
      limits: {}
      requests: {}

Again, please refer to the custom resource definition to find out the names of the jobs that can be managed.
