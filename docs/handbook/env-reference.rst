.. _uref-operator-env-vars:

Environment Variable Reference
==============================

.. envvar:: YAOOK_OP_NAMESPACE

    The Kubernetes Namespace the Operator watches for custom resources.

    Affects: all operators.

.. envvar:: YAOOK_OP_MULTIPLE_NAMESPACES

    This option should be set to true if the same operator is to run in several namespaces. In addition, the compute and gateway nodes should be labelled with the respective namespace (see ``LABEL_NAMESPACE``) so that the correct operator can deploy the Nova and Neutron resources there.

    Default: ``false``

    Affects: all operators.

.. envvar:: YAOOK_OP_VERBOSITY

    The Log-Level of the Operator. Can be set to 0 (ERROR), 1 (WARNING), 2 (INFO), 3 (DEBUG)

    Affects: all operators.

.. envvar:: YAOOK_OP_DOCKER_CONFIG

    The Path to a Docker config file containg the credentials for an authenticated Docker registry. Only required when automatically updating (or during development) and using some registry besides the official Gitlab.

    Affects: all operators.

.. envvar:: YAOOK_OP_DOCKER_REGISTRY_BASE_PATH

    Allows overriding the base image path for the Yaook images.

    Default: ``registry.yaook.cloud/yaook``

    Affects: all operators.

.. envvar:: YAOOK_OP_DOCKER_REGISTRY_OVERRIDE

    Allows overriding the host names of container image registries there the container images are pulled from. If set, this must be a valid yaml dictionary.

    The key must be the registry to be overwritten. The special key ``DEFAULT_REGISTRY`` overrides the default registry which matches all image paths which are not prefixed with a certain registry.

    The value must be the new registry host name.

    For example to replace the registry for all images pulled from the default registry, e.g. the Bitnami images, and images from registry.yaook.cloud, :envvar:`YAOOK_OP_DOCKER_REGISTRY_OVERRIDE` must be set to:

    .. code-block:: json

        {"DEFAULT_REGISTRY": "my-registry.domain.tld", "registry.yaook.cloud": "my-registry.domain.tld"}

    Note that if used in conjunction with :envvar:`YAOOK_OP_VERSIONS_OVERRIDE` the registry name is overwritten first. Can cause problems if used together with :envvar:`YAOOK_OP_DOCKER_REGISTRY_BASE_PATH` if the Yaook image registry has been overwritten.

    Affects: all operators.

.. envvar:: YAOOK_OP_DOCKER_INSECURE

    Allows disabling HTTPS when talking to docker registries. Don't set this.

    Affects: all operators.

.. envvar:: YAOOK_OP_DOCKER_TLS_SKIP_VERIFY

    Allows disabling TLS verification when talking to docker registries. Don't set this.

    Affects: all operators.

.. envvar:: YAOOK_OP_CLUSTER_DOMAIN

    The cluster domain of the Kubernetes cluster the Operator is connecting to. If the Operator is running inside the cluster this is automatically discovered and does not need to be set.

    Affects: all operators.

.. envvar:: YAOOK_OP_VERSIONS_USE_ALPHA

    If True allow the Operator to use Docker images that are flagged as alpha. This is only used for images which are not currently pinned. Nowadays, this is generally only the case while re-generating the pin file.

    Affects: all operators.

.. envvar:: YAOOK_OP_VERSIONS_USE_ROLLING

    If True allow the Operator to use Docker images that are flagged as rolling. This is only used for images which are not currently pinned. Nowadays, this is generally only the case while re-generating the pin file.

    Affects: all operators.

.. envvar:: YAOOK_OP_VERSIONS_OVERRIDE

    Allows overriding the url of a external dependency (e.g. a Docker image). Must be set to a valid yaml dictionary. The key must be the full original url as in `pinned_version.yml` while the value must be the full URL (including version) of the dependency to use. Note that if used in conjunction with :envvar:`YAOOK_OP_DOCKER_REGISTRY_OVERRIDE` the registry name will be overwritten first so the key for the version overwrite must match the new registry name to apply.

    Affects: all operators.

.. envvar:: YAOOK_OP_RUNNER_COUNT

    The amount of task queue runners that reconcile the different custom resources

    Default: ``3``

    Affects: all operators.

.. envvar:: YAOOK_OP_QUEUE_BACKOFF_BASE

    The Task will be requeued after a failed reconcile. This defines the factor by which the back-off value is multiplied on each iteration.

    Default: ``2``

    Affects: all operators.

.. envvar:: YAOOK_OP_QUEUE_BACKOFF_START

    The Task will be requeued after a failed reconcile. This defines the start value of the exponential back-off for failed tasks.

    Default: ``0.5``

    Affects: all operators.

.. envvar:: YAOOK_OP_QUEUE_BACKOFF_MAX

    The Task will be requeued after a failed reconcile. This defines the cutoff value for the exponential back-off for failed tasks.

    Default: ``120``

    Affects: all operators.

.. envvar:: YAOOK_OP_QUEUE_BACKOFF_JITTER

    The Task will be requeued after a failed reconcile. This defines the jitter ratio to use for rescheduling failed tasks.

    Default: ``0.1``

    Affects: all operators.

.. envvar:: YAOOK_OP_PERIODIC_RECONCILE_DELAY

    The amount of time in seconds after that each resource is reconciled again.

    Default: ``3600``

    Affects: all operators.

.. envvar:: YAOOK_OP_METRICS_PORT

    The default port for the prometheus metrics endpoint.

    Default: ``8000``

    Affects: all operators.

.. envvar:: YAOOK_OP_TRACING_ENABLED

    Enables the support for sending traces to jaeger via opentelemetry.

    Affects: all operators.

.. envvar:: YAOOK_OP_JEAGER_HOSTNAME

    Defines the Hostname of the Jaeger Endpoint.

    Default: ``localhost``

    Affects: all operators.

.. envvar:: YAOOK_OP_JEAGER_PORT

    Defines the Port of the Jaeger Endpoint.

    Default: ``6831``

    Affects: all operators.

.. envvar:: YAOOK_OP_INTERFACE_INCONSISTENT_READ

    Allows the Kubernetes client to do list requests without strong consistency.
    This should significantly improve performance, but may cause duplicate resources.
    This envvar is set as a feature gate since it might cause issues inside the operators.
    However this envvar is not set for all actions as it may create duplicate resources.
    The "strongly_consistent" parameter ensures that API results are up-to-date and reflect current resource states,
    helping to avoid conflicts when creating resources with existing names or working with other controllers.
    In some cases, it might be necessary to use strongly_consistent=True in order to avoid race conditions
    or ensure that you are working with up-to-date information about resources. This can be seen in the "create()" function which is written in the "interfaces.py" file.

    Default: ``False``

    Affects: all operators.

.. envvar:: YAOOK_OP_OVN_EVICTION_COMMIT_SLEEP_TIME

    Defines the time, in seconds, between each Logical Router Port (LRP) commit to the OVN Northbound when evicting a gateway node. By default it is 10 for commiting priority 5 LRP, for lower priorities it is <SLEEP_TIME>/(6-<PRIORITY>)

    Default: ``10``

    Affects: Neutron OVN operator.

.. envvar:: YAOOK_OP_OVN_EVICTION_DB_CONNECTION_TIMEOUT

    Defines connection timeout duration, in seconds, when connecting to the OVN Northbound/Southbound, for eviction job of a gateway node.

    Default: ``150``

    Affects: Neutron OVN operator.

.. envvar:: YAOOK_OP_OVN_EVICTION_PROBE_INTERVAL

    Defines the time, in milliseconds, to probe OVN Northbound/Southbound when the database connection is established for evicting job a gateway node.

    Default: ``0`` - Probing disabled

    Affects: Neutron OVN operator.

.. envvar:: YAOOK_KEYSTONE_OP_AUTH_URL

    .. deprecated:: (something ancient)

        This environment variable has no effect anymore and can safely be removed.

    Affects: keystone-resources-operator.

.. envvar:: YAOOK_KEYSTONE_OP_INTERFACE

    The OpenStack interface the Keystone Operator should use to connect to Keystone.

    Default: ``internal``

    Affects: keystone-resources-operator.

.. envvar:: YAOOK_NEUTRON_OVN_AGENT_OP_JOB_IMAGE

    A reference to the Docker image that should be used to evacuate OVN agents.

    Affects: neutron-ovn-operator.

.. envvar:: YAOOK_NOVA_COMPUTE_OP_INTERFACE

    The OpenStack interface the Nova Compute Operator should use to connect to Keystone.

    Affects: nova-compute-operator.

.. envvar:: YAOOK_NOVA_COMPUTE_OP_JOB_IMAGE

    A reference to the Docker image that should be used to evacuate compute nodes.

    Affects: nova-compute-operator.

.. envvar:: YAOOK_INFRA_OP_AMQP_INTER_NODE_TLS_VERIFICATION

    Enable inter-node TLS verification for RabbitMQ servers.

    Default: ``true``.

    Affects: infra-operator.

.. envvar:: REQUESTS_CA_BUNDLE

    Path to a file to override the CA certificates trusted by HTTPS client operations done by the operators.
