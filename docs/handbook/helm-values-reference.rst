.. _uref-operator-helm-values:

Helm Chart Values Reference
###########################

.. note::

    The ``crds`` Helm chart does not accept any values.

Operator Helm Charts
====================

The YAOOK Operator Helm charts share common values which you can set via the usual means. They are documented here.

.. object:: operator.image.repository

    (string) The docker image repository to pull the operator image from.

    Default: ``"registry.yaook.cloud/yaook/operator"``

.. object:: operator.image.tag

    (string) The docker image tag to use for the operator image. This can be used to deploy a newer operator image without waiting for a new Helm chart version.

    Default: Helm chart version (``appVersion`` as shown by ``helm show chart yaook/OPERATORNAME``)

.. object:: operator.image.pullPolicy

    (string) The ``imagePullPolicy`` to set on the pod.

    Default: ``"IfNotPresent"``

.. object:: operator.resources

    (object) The ``spec.container[].resources`` field of the Operator container. See `ResourceRequirements <https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#resources>`_ for syntax.

    Default: ``{}``

.. object:: operator.schedulingKeys

    (list of strings) Scheduling keys to set on the Operator pod itself.

    Default: ``["operator.yaook.cloud/any", "operator.yaook.cloud/OPERATORNAME"]``, where ``OPERATORNAME`` is the name of the operator (without the ``-operator`` suffix), e.g. ``keystone-resources``.

.. object:: operator.verbosity

    (string) Set the verbosity level. This goes from 0 (ERRORs only) to 3 (DEBUG messages).

    Default: ``"3"``

.. object:: operator.extraEnv

    (list of `EnvVar <https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#environment-variables>`_) Add extra environment variables to the Operator container.

    Default: ``[]``

.. object:: operator.imagePullSecrets

    (list of `LocalObjectReference <https://kubernetes.io/docs/reference/kubernetes-api/common-definitions/local-object-reference/#LocalObjectReference>`_) Optionally set image pull secrets necessary to pull the operator image itself.

    .. note::

        This does not affect the pods spawned by the Operator; you need to set image pull secrets on the custom resources for that.

    Default: ``[]``

.. object:: operator.pythonOptimize

    (boolean) Enable optimizations in Python. There is seldom a reason to turn this off.

    Default: ``true``

.. object:: operator.dockerConfigSecret

    (string) Name of a Kubernetes Secret containing a docker config json (``kubernetes.io/dockerconfigjson`` type) to mount into the container. This will automatically set the :envvar:`YAOOK_OP_DOCKER_CONFIG`. Should generally not be needed thanks to pinned versions.

    Default: ``""`` (disabled)

.. object:: operator.caConfigMap

    (string) Optional specify a name of a ConfigMap which is mounted within the operator container under /etc/pki/tls/certs. If set the environment variable :envvar:`REQUESTS_CA_BUNDLE` is set to point to /etc/pki/tls/certs/ca-bundle.crt. The operator then uses that file as the CA bundle. This can be useful for example if a private image registry is used which is secured by a certificate not signed by a official CA.

    Default: ``""`` (disabled)


