Infrastructure Ironic
=======================

Infrastructure Ironic is meant to be a standalone ironic for deployment of the nodes. Its not build to have connection to other services besides keystone.
In a perfect scenario ironic would run in a separate deployment cluster using its dedicated keystone.

To use the infrastructure ironic you need to build an ipa (ironic-python-agent) kernel and ramdisk first.
This can be done by using the ironic-python-agent-builder. This uses the regular diskimage-builder to build ipa kernel and ramdisk.
(https://docs.openstack.org/ironic-python-agent-builder/latest/)

After building ipa.kernel and ipa.initramfs those to be placed within the imgserver.

The path which is used as default-path for the imgservers webserver is
``/usr/share/nginx/html/``


Example Config
----------------------------------------
.. literalinclude:: infra_ironic.yaml