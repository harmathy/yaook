MySqlService
============

The MySqlService is used by many operators to get a database for their service.
This resource is normally not used by the users directly but we provide here an example for better understanding.

.. literalinclude:: mysqlservice.yaml
