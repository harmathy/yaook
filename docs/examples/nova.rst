Nova
======

.. literalinclude:: nova.yaml

Ceph Config
-----------

.. note::
    To use ceph you need to specify the same uuid and the same secret name as
    it is in use for cinder. The usage of different keys/users is not
    supported by openstack.

Add vTPM to instance
--------------------

To add a vTPM, the following prequisites are to be fullfilled.

1. Enable the swtpm settings in the nova-compute config for the nodes which are to provide the vTPM to the instance

.. literalinclude:: nova-compute-vtpm.yaml

As we specify the same user/group within the qemu config for swtpm we highly recommend to use nova:libvirt within the novacompute config.

2. Set the image or flavor specs to provide a vTPM if an instance is created with either of those.
``openstack flavor set $FLAVOR -property hw:tpm_version=2.0 --property hw:tpm_model=tpm-crb``
or
``openstack image set $IMAGE -property hw_tpm_version=2.0 --property hw_tpm_model=tpm-crb``
(reference https://docs.openstack.org/nova/latest/admin/emulated-tpm.html)

3. Have barbican as secret manager present
4. Nova (and compute) of atleast Victoria Release
5. Make sure the novacompute keystone user has permissions to create(store) barbican secrets (and read them).

.. note::
    Yaook allows shelve and evacuate actions for vTPM enabled servers.
    To archive this the vTPM becomes ephemeral.
    This is comparable with the baremetal behavior where the TPM content is gone after moving a disk to a new hardware.

Add provider config to nova compute
-----------------------------------

Nova allows you to manage resource providers using config files for each
compute node. (https://docs.openstack.org/nova/latest/admin/managing-resource-providers.html)
This can be used for example to configure traits for isolated host aggregate filtering.
(https://docs.openstack.org/nova/latest/reference/isolate-aggregates.html)

Yaook supports to set this config, like shown in this example:

.. literalinclude:: nova-compute-provider-config.yaml

Eviction
=================
the eviction part is mandatory. it contains two parameter.
 - ironicNodeShutdown (optional): It is used to power off the node via ironic when it is not available. It contains a secret with ironic openrc credentials.
 - volumeLockDurationSeconds (optional): wait for releasing the volume lock during the eviction. If NodeShutdown is not set, it is not needed to wait for the volume release. For this reason you can set it to 0. If ironicNodeShutdown is enabled, please set this parameter to the volume release time of your storage backend. This is especially needed for NetApp. If Ceph is used, the exclusive locks feature will handle the volume release, so please leave this setting at the default value.

.. literalinclude:: novaeviction.yaml


NovaHostAggregate
=================

Availability-Zone and Properties are both optional.
A computenode can only reside in a single Availability-Zone.
If there is a mapping for aggregates with conflicting Availability-Zone values, no mapping will take place.
(there will be an Exception for that)

Example of a NovaHostAggregate
------------------------------

.. literalinclude:: novahostaggregate.yaml
