Cinder
======

.. literalinclude:: cinder.yaml

Cinder using Netapp
-------------------

.. literalinclude:: cinder_netapp.yaml

The configmap that contains the netapp copyoffload tool can be created using this command: 
``kubectl create configmap netapp-copyoffload --from-file na_copyoffload_64``