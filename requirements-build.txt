keystone==25.0.0
barbican==18.0.0
nova==29.2.0
glance==28.1.0
neutron==24.0.1
neutron-dynamic-routing==24.0.0
cinder==24.1.0
ironic==26.1.1
ironic-inspector==12.2.0
mypy==1.11.1
pyroute2>=0.6.2
types-PyMySQL==1.1.0.20240524
eventlet>=0.33.3
sqlalchemy<2.0.0
oslo.db<16.0.1
oslo.policy<4.5.0
neutron-lib<3.15.1
pynetbox==7.2.0
